import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext

class NewTestListener {
		
	
	@AfterTestCase
	def sampleAfterTestCase(TestCaseContext testCaseContext) {
	//satu 
	if(testCaseContext.getTestCaseId() == 'Test Cases/04_Create Project/04_001_Create Project'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status = 'FAILED'
	}
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status = 'ERROR'
	}
	}
	
	//dua
	if(testCaseContext.getTestCaseId() == 'Test Cases/05_Submit Qoute/05_001__Submit Qoute'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status = 'FAILED'
	}
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status = 'ERROR'
	}	
	}
	
	//tiga
	if(testCaseContext.getTestCaseId() == 'Test Cases/06_Start Project/06_001__start project'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status = 'FAILED'
	}
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status = 'ERROR'
	}	
	}
		
	//empat
	if(testCaseContext.getTestCaseId() == 'Test Cases/07_Pay/07_001_Pay'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status = 'FAILED'
	}
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status = 'ERROR'
	}
	}
	
	//lima
	if(testCaseContext.getTestCaseId() == 'Test Cases/08_Complate Project/08_001_Complete Project'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status = 'FAILED'
	}
		
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status = 'ERROR'
	}
	
	else{
		GlobalVariable.TC_Status = 'OK'
	}
	
	//enam
	if(testCaseContext.getTestCaseId() == 'Test Cases/09_Payment/09_001_payment'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status = 'FAILED'
	}
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status = 'ERROR'
	}
	
	//tujuh
	if(testCaseContext.getTestCaseId() == 'Test Cases/10_Approve payment/10_001_approve payment'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status = 'FAILED'
	}
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status = 'ERROR'
	}
	
}
	//invoke member
	if(testCaseContext.getTestCaseId() == 'Test Cases/11_Workspace Setting/11_001_invite member'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status2 = 'FAILED'
	}
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status2 = 'ERROR'
	}
	
}
	//re invite member
	if(testCaseContext.getTestCaseId() == 'Test Cases/11_Workspace Setting/11_002_invoke member'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status2 = 'FAILED'
	}
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status2 = 'ERROR'
	}
	
	
}
	
}
}
	}
	}	


