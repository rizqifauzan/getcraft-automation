import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext

class NewTestListener_1 {
	
	@AfterTestCase
	def sampleAfterTestCase2(TestCaseContext testCaseContext) {
	//satu
	if(testCaseContext.getTestCaseId() == 'Test Cases/11_Workspace Setting/11_001_invite member'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status2 = 'FAILED'
	}
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status2 = 'ERROR'
	}
	}
	
	//dua
	if(testCaseContext.getTestCaseId() == 'Test Cases/11_Workspace Setting/11_002_invoke member'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status2 = 'FAILED'
	}
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status2 = 'ERROR'
	}
	}
	
	//tiga
	if(testCaseContext.getTestCaseId() == 'Test Cases/09_Payment/09_001_payment'){
		if(testCaseContext.getTestCaseStatus()=='FAILED'){
		GlobalVariable.TC_Status = 'FAILED'
	}
	if(testCaseContext.getTestCaseStatus()=='ERROR'){
		GlobalVariable.TC_Status = 'ERROR'
	}
	

	}}}