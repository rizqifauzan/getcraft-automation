<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Rahmat Suites</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>83bd4115-4a88-4fa0-a7e6-1137b3decad5</testSuiteGuid>
   <testCaseLink>
      <guid>88ca5e5b-da10-4b3b-b0ba-7f4d6bf14ef2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01_Login and Register/01_002__ Login As Creator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84dee0a9-3852-4f12-bab3-7d837a053141</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03_Verify Element/03_004_Change Password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d3f7cd8-d1c7-445b-8a97-f2af359657ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03_Verify Element/03_005_Change Password fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da3f2273-731b-4e6a-a0d9-cbe029bd46ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01_Login and Register/01_007__change email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>943c834e-7e49-4d96-a234-ce53631a7da2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01_Login and Register/01_013__change email (has been taken)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4616248-bc71-4126-aa09-eb165ce19d25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01_Login and Register/01_014__change email (different email)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43504f9b-09e5-445d-994d-5ee5b22c75c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04_Project/04_012_Create project multiple services</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ff9786de-0db2-4a65-a93f-01f1775e13e4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3723cfe1-6b97-422d-aabc-3638b3ece2fb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>113d0b3e-8522-4806-9cbe-122589bd2664</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>10534b4b-5ef3-4a32-82e2-8c977d04fde9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c481f298-8175-420d-a07a-c8adf0bc1c4c</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
