<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Refine Rizky-PROD</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>rizubuntu@gmail.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9c3e7385-d8fd-4317-b6ad-5ffa0d7ccd3f</testSuiteGuid>
   <testCaseLink>
      <guid>47dac3ad-7693-4494-85cf-ffef939c5487</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/17_Marketplace/17_002_Search URL sharing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8cb27c3d-9f69-424b-8985-05bef51be3b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/17_Marketplace/17_003_Delete Param URL sharing</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
