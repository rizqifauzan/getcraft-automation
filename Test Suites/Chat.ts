<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Chat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f8d72b39-fa25-4591-99b9-9d2fc21aed19</testSuiteGuid>
   <testCaseLink>
      <guid>13c01efa-426e-41fd-a72b-a3a1ead4d0e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_001__Direct Command and Qoute Feedback for old project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80894efc-35a7-4bc7-825a-1ba1e1f337e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_002__Direct Command and Qoute Feedback for new project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c0121c2-c3b9-4e34-90e1-503d79cc6fef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_003__mute and unmute chat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5dd25b7d-9421-443c-8f33-42bdcc10e5e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_004__send chat</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>215e12f3-7cde-439a-9a33-c852a87cbad4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2a2811c5-eb01-4b95-b680-3343fa0f5c61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_007__verify notofication</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2813700d-975a-4f90-871c-2b1bb49c0439</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3ab04e54-2b8a-4505-9599-445c224055a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_005__read chat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>edd319b3-4514-436f-bbd8-2975de4960e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_004__send chat</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>215e12f3-7cde-439a-9a33-c852a87cbad4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f625492d-b75c-4b26-be46-9089522d3ff1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_008__upload file</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0fefe708-6d41-4ca8-b2e9-f50467a14829</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_009__mute and unmute email digest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8f2ae01-6881-430b-b120-214c30c95064</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_010__prioritize recent chat</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>edf5947d-e066-459e-8691-017568da2837</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
