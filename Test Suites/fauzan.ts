<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>fauzan</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>54fb3086-145e-4560-96c0-22ac1d7c1af3</testSuiteGuid>
   <testCaseLink>
      <guid>ae45bca2-1715-4913-9bba-ba26b2897e35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_001__Direct Command and Qoute Feedback for old project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e1e7e24-390b-4cfa-bb09-0e3def858620</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_003__mute and unmute chat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20e3b633-b460-4c8a-bde1-221c3a12dbb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_002__Direct Command and Qoute Feedback for new project</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e50bd50-dba4-4294-9b11-61d9dc197b3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_004__send chat</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>215e12f3-7cde-439a-9a33-c852a87cbad4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5a1ae5eb-3857-4674-ab72-30a1f4864399</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_006__read chat 2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>572782ab-4fd9-44b6-8530-a5085c5abfff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_005__read chat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cda132b3-66d3-40a1-b9ff-e55c218781d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_007__verify notofication</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2813700d-975a-4f90-871c-2b1bb49c0439</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b12967db-f8c1-47e4-bee9-70457f73de02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_008__upload file</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3ea1a7b-2478-45a7-bed4-201324c9def6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_009__mute and unmute email digest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>effbbe1c-2204-4e42-a637-4c893893fc2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_010__prioritize recent chat</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>edf5947d-e066-459e-8691-017568da2837</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>be9f63f8-ab3d-465d-87a3-d449076afdb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_011__Scraper</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb1d4a68-0f47-40ad-9b2c-4586406fe1ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_012_temp Chat</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d4c2c347-5d2d-4c89-b8ec-6ee4b41ddb55</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5e0e37b4-fcd4-45e8-a628-0cfc21a7cee8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_013__remember selected chat</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>45944924-84a5-483c-ad95-fd1d5d8dca2b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>29955b50-62c3-479b-9cd8-76b74dba1b63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_014__esc for close pop up window</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7de73ba9-d938-4f5a-8f3f-9d7b31104fa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_015__reply chat</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>27b82a71-5c38-416d-a343-42518f377a29</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f597fc78-521d-456e-92b7-d2298ef8208b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_019__Media Share - navigation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>887ff09f-2f42-456e-af26-cf573f13e781</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14_Chat/14_020__send chat - foreign text</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4ace1e66-a5b3-43cc-8c11-b7068165c319</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
