<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Getcraft-API</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c5119a6b-8f69-4104-b1fa-29f489cbc331</testSuiteGuid>
   <testCaseLink>
      <guid>7844e76b-d265-445c-9875-5484f54fd57e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/99_API/99_01_Loki/99_01_01_Chat/99_01_01_01_SaveChat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74f41ad2-8c85-452d-8d50-788436d1a7c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/99_API/99_01_Loki/99_01_01_Chat/99_01_01_03_Mute Email Notification</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a11727a-ea57-4116-b38e-0afd3ff89004</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/99_API/99_01_Loki/99_01_01_Chat/99_01_01_04_Mute Push Notification</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0a7737c1-e7d1-403f-aebe-f820a5702f96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/99_API/99_01_Login and Register/99_01_01__login refreshToken logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
