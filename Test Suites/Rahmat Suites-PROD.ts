<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Rahmat Suites-PROD</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8509aa9e-052f-49be-8f16-42cf99105987</testSuiteGuid>
   <testCaseLink>
      <guid>e364552e-33a5-4e9e-ad2b-b3cb8054b427</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/11_Workspace Setting/11_001_invite member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>717463c1-47c3-4f4d-8bf7-fbe86fd46633</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/11_Workspace Setting/11_002_invoke member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bce5ce93-6a87-4869-abb4-d7219c26507f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/11_Workspace Setting/11_003_ReInvite member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20f43095-3ad9-4e6b-a7d1-9170e48bc2a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/11_Workspace Setting/11_002_invoke member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>067e1e0d-5e95-43c3-a591-da37a12cfbff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/01_Login and Register/01_013__change email (has been taken)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35592dcb-7bb1-4536-bcca-4eb8128b03e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/01_Login and Register/01_014__change email (different email)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33213df9-a1a5-4d56-9e50-8d42e2d6c458</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/01_Login and Register/01_015_Register_validation email</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f82e2300-cbd6-4859-a5ef-b525b9780f6e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>babc4146-a3c1-4cb8-8f71-91b110591885</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/02_Profile/02_025_See More after update</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7c6f67fd-0030-4fc4-a8d3-d059a0c0ed9e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d6374e11-e8de-4e8e-969c-88f95208b95f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d087cab1-7e2b-4718-9283-f0c69c35f013</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/02_Profile/02_027_Edit Occupation</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7ac6f394-de38-4647-b6f6-8edf2a7cf894</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>02314748-684f-467e-a1f6-29043dcb048f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e618c190-3b4d-4da3-8d31-7c26a46e2368</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/03_Verify Element/03_001_Change workspace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9dda4848-0f2d-4877-ab5a-df25bd852189</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/03_Verify Element/03_006_Check URL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80a08ab9-f654-4217-a890-0d1135172b68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/03_Verify Element/03_007_Check direct URL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>414c449b-0dbe-41e1-bd3a-eb5d08e3a9f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Production/17_Marketplace/17_007_See Details</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b414879c-9004-4fdd-8009-2b2cebf715c6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b9f52e34-a7b9-4227-971c-8a4c8beb17ca</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
