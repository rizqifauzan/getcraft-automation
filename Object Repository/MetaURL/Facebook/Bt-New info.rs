<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Bt-New info</name>
   <tag></tag>
   <elementGuidId>a6ffd992-40f9-413c-a444-43e401903758</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[(text() = 'Log In' or . = 'Log In')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[5]/div[3]/div/div[2]/div/div/div/div/span/form/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Log In</value>
   </webElementProperties>
</WebElementEntity>
