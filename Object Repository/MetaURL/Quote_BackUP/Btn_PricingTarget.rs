<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_PricingTarget</name>
   <tag></tag>
   <elementGuidId>99ed5c06-0e62-4d71-a7fd-1e64b1004372</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='index__warpBox__2wyQr clearfix']/a[1]//div[@class='MuiCardContent-root index__boxContent__1the0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='index__warpBox__2wyQr clearfix']/a[1]//div[@class='MuiCardContent-root index__boxContent__1the0']</value>
   </webElementProperties>
</WebElementEntity>
