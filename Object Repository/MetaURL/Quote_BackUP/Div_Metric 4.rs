<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Div_Metric 4</name>
   <tag></tag>
   <elementGuidId>9350763b-4abc-4765-806a-3954e5cac475</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//option[(text() = 'Leads' or . = 'Leads') and contains(@id, 'mtrSec2Line1')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>option</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Leads</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mtrSec2Line1</value>
   </webElementProperties>
</WebElementEntity>
