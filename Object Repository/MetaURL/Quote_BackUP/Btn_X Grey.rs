<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_X Grey</name>
   <tag></tag>
   <elementGuidId>58b71862-6526-4ba0-8717-b5c382469e4a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//svg[@class = 'MuiSvgIcon-root index__closeItem__XF8wZ']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiSvgIcon-root index__closeItem__XF8wZ</value>
   </webElementProperties>
</WebElementEntity>
