<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Desc_empty</name>
   <tag></tag>
   <elementGuidId>ea7660e6-356d-4e89-bc5d-2dc97bc2f9b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'pull-right index__errorText__Iahgp hidden-xs hidden-sm']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pull-right index__errorText__Iahgp hidden-xs hidden-sm</value>
   </webElementProperties>
</WebElementEntity>
