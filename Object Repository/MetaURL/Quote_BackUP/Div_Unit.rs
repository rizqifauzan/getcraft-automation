<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Div_Unit</name>
   <tag></tag>
   <elementGuidId>11933843-4423-4baa-ae18-8c42a8c0e8cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, ' css-1wy0on6')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value> css-1wy0on6</value>
   </webElementProperties>
</WebElementEntity>
