<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_Qty</name>
   <tag></tag>
   <elementGuidId>106487b4-c0ba-4a17-a7e3-f71e22094d07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tbody[1]/tr[1]/td[2]//input[@class='MuiInputBase-input MuiOutlinedInput-input index__inputText__1gUxs ']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tbody[1]/tr[1]/td[2]//input[@class='MuiInputBase-input MuiOutlinedInput-input index__inputText__1gUxs ']</value>
   </webElementProperties>
</WebElementEntity>
