<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>sigout-linkedin</name>
   <tag></tag>
   <elementGuidId>dd3b8fb6-5511-4c31-bffb-71ef227c25ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-control-name = 'nav.settings_signout']</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-control-name</name>
      <type>Main</type>
      <value>nav.settings_signout</value>
   </webElementProperties>
</WebElementEntity>
