<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label-Info</name>
   <tag></tag>
   <elementGuidId>78e81fbf-9952-4995-9b82-84aa2873804d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h2[(text() = 'URL information' or . = 'URL information')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>URL information</value>
   </webElementProperties>
</WebElementEntity>
