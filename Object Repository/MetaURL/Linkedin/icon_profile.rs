<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icon_profile</name>
   <tag></tag>
   <elementGuidId>08f0dffb-3eeb-4ec0-ba2e-0505287ed2f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'nav-item__profile-member-photo nav-item__icon ghost-person ember-view']</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-item__profile-member-photo nav-item__icon ghost-person ember-view</value>
   </webElementProperties>
</WebElementEntity>
