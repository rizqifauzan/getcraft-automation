<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>start_project2</name>
   <tag></tag>
   <elementGuidId>052d0700-642a-4e19-ba76-eaa60b1ef9b6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[14]/div/div[1]/div/div/div[2]/button[2]/div/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(contains(text(), 'Start Project') or contains(., 'Start Project'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Start Project</value>
   </webElementProperties>
</WebElementEntity>
