<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_service</name>
   <tag></tag>
   <elementGuidId>bf9608b8-aaeb-4a23-b18e-7b1d4772307a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[4]/div/main/section/div/div/div/form/div/div[2]/div[3]/div[3]/div[1]/div[1]/div/div/div[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'mdi mdi-arrow-up')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mdi mdi-arrow-up</value>
   </webElementProperties>
</WebElementEntity>
