<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_phoneNumber</name>
   <tag></tag>
   <elementGuidId>bde268b5-486f-4506-a8a7-dab88e3c8bd1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@id, 'undefined-Typeyouractivephonenumber-Phonenumber') and @type = 'phoneNumber']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>undefined-Typeyouractivephonenumber-Phonenumber</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>phoneNumber</value>
   </webElementProperties>
</WebElementEntity>
