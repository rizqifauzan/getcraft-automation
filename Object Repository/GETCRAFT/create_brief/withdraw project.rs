<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>withdraw project</name>
   <tag></tag>
   <elementGuidId>3a4d0e48-cad8-4918-89b4-2d6f27c876f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[4]/div/div/main/section/div/div[2]/div/a[2]/div/div/div[2]/div[2]/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'col-md-12') and (text() = 'Withdraw from project' or . = 'Withdraw from project')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Withdraw from project</value>
   </webElementProperties>
</WebElementEntity>
