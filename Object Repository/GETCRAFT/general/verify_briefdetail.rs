<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_briefdetail</name>
   <tag></tag>
   <elementGuidId>afca26c0-0940-4a5a-8bc5-21759d42d2ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(@class, 'index__activeTabs')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[4]/div/main/section/div/div/article/form/div[1]/div[3]/div/div[1]/div/div[1]/div[1]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__activeTabs</value>
   </webElementProperties>
</WebElementEntity>
