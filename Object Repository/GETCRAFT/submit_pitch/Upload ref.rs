<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Upload ref</name>
   <tag></tag>
   <elementGuidId>35cfa5db-450d-4eea-a35b-a9cb523a2648</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'button button-xlarge button-rounded index__browse-files__')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[4]/div/main/section/div/div/article/form/div[1]/div[3]/div/div[1]/div/div[1]/div[1]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button button-xlarge button-rounded index__browse-files__</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
</WebElementEntity>
