<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>viewer</name>
   <tag></tag>
   <elementGuidId>6b0ba25a-cbd0-4ad7-baa7-fc97990af3ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'index__title__2QYH2') and (contains(text(), 'Price') or contains(., 'Price'))]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[4]/div/main/section/div/div/div/form/div/div[2]/div[3]/div[4]/div[1]/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__title__2QYH2</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Price</value>
   </webElementProperties>
</WebElementEntity>
