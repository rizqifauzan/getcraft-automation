<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_submit_pitch</name>
   <tag></tag>
   <elementGuidId>e85dab5b-d73b-4f66-9dcf-b1028b6c7812</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[(contains(text(), 'Submit Pitch') or contains(., 'Submit Pitch'))]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[4]/div/main/section/div/div/article/form/div[1]/div[3]/div/div[1]/div/div[1]/div[1]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit Pitch</value>
   </webElementProperties>
</WebElementEntity>
