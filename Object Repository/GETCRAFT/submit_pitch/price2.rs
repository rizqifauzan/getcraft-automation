<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>price2</name>
   <tag></tag>
   <elementGuidId>a78befe5-8692-4d62-8990-92acf0afffec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[4]/div/main/section/div/div/div/form/div/div[2]/div[3]/div[3]/div[1]/div[1]/div/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@id, 'price-objectObject-undefined') and contains(@name, 'price')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>price-objectObject-undefined</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>price</value>
   </webElementProperties>
</WebElementEntity>
