<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_recover_account_PROD</name>
   <tag></tag>
   <elementGuidId>a8ba833a-6fce-4ef1-ad6f-1f025499aafb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/table/tbody/tr[2]/td/div/table/tbody/tr[5]/td</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[(contains(text(), 'Recover account') or contains(., 'Recover account'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Recover account</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
</WebElementEntity>
