<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>open_email</name>
   <tag></tag>
   <elementGuidId>ae4b7597-3a69-4bf6-8c55-cf661944bb01</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;Table1&quot;]/tbody/tr[2]/td[4]/b/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[(contains(text(), '
                                                GetCRAFT - Recover Account
                                                  ') or contains(., '
                                                GetCRAFT - Recover Account
                                                  '))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>even pointer ng-scope</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                GetCRAFT - Recover Account
                                                  </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;row_testing_creator445-1533704445-2560377&quot;)/div[@class=&quot;all_message-min&quot;]</value>
   </webElementProperties>
</WebElementEntity>
