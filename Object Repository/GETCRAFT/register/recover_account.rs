<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>recover_account</name>
   <tag></tag>
   <elementGuidId>b0f15371-828b-49f2-a267-517f8f16364c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'even pointer ng-scope' and (contains(text(), '
                GetCraft Team
            ') or contains(., '
                GetCraft Team
            '))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>even pointer ng-scope</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                GetCraft Team
            </value>
   </webElementProperties>
</WebElementEntity>
