<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Join team_PROD</name>
   <tag></tag>
   <elementGuidId>c3e94745-4045-4a96-9198-0987f4362e04</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/table/tbody/tr[2]/td/div/table/tbody/tr[6]/td/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[(contains(text(), 'Join the team') or contains(., 'Join the team'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Join the team</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
</WebElementEntity>
