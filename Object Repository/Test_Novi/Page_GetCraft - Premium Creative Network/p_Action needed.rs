<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Action needed</name>
   <tag></tag>
   <elementGuidId>deaadfad-5dc4-4df6-84de-0877e5307da5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[4]/div/div/div[3]/div/div/div/div/div[4]/div/div/div/div/div[2]/div[2]/div/div/div/div[2]/div/div[3]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Action needed</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;react-main-container&quot;]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/div[@class=&quot;index__detailBody__1hIqn&quot;]/div[@class=&quot;container index__paddingTop40__2yAeR index__bgGrey__2025-&quot;]/div[@class=&quot;row false&quot;]/div[@class=&quot;false col-md-9 col-xs-12&quot;]/div[1]/div[@class=&quot;react-swipeable-view-container&quot;]/div[4]/div[@class=&quot;index__tabBody__3l3q3&quot;]/div[@class=&quot;col-md-12 no-space&quot;]/div[@class=&quot;index__menu__34BQp col-md-12&quot;]/div[1]/div[@class=&quot;payment-section row&quot;]/div[@class=&quot;col-md-11 col-sm-12 index__mobileSection__QPC39&quot;]/div[@class=&quot;index__card__jGCW5&quot;]/div[1]/div[1]/div[@class=&quot;body-expanded&quot;]/div[@class=&quot;index__box-product-selected__2O4Hp clearfix&quot;]/div[@class=&quot;col-md-3 index__additional-action__3hzWc&quot;]/p[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[4]/div/div/div[3]/div/div/div/div/div[4]/div/div/div/div/div[2]/div[2]/div/div/div/div[2]/div/div[3]/p</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(0)'])[2]/following::p[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Seller added to invitation list'])[1]/preceding::p[11]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='VIEW'])[1]/preceding::p[11]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Action needed']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div[2]/div/div[3]/p</value>
   </webElementXpaths>
</WebElementEntity>
