<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input</name>
   <tag></tag>
   <elementGuidId>9b83a535-98cd-439b-8b0f-11e2be2f12c5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value=''])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;react-main-container&quot;]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/div[@class=&quot;index__detailBody__1hIqn&quot;]/div[@class=&quot;container index__paddingTop40__2yAeR index__bgGrey__2025-&quot;]/div[@class=&quot;row false&quot;]/div[@class=&quot;false col-md-9 col-xs-12&quot;]/div[1]/div[@class=&quot;react-swipeable-view-container&quot;]/div[4]/div[@class=&quot;index__tabBody__3l3q3&quot;]/div[@class=&quot;col-md-12 no-space&quot;]/div[@class=&quot;index__menu__34BQp col-md-12&quot;]/div[1]/div[@class=&quot;payment-section row&quot;]/div[@class=&quot;col-md-11 col-sm-12 index__mobileSection__QPC39&quot;]/div[@class=&quot;index__card__jGCW5 index__active__3azTH&quot;]/div[1]/div[3]/div[1]/input[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value=''])[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[4]/div/div/div[3]/div/div/div/div/div[4]/div/div/div/div/div[2]/div[2]/div/div/div[3]/div/input</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
