<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Approve quotation</name>
   <tag></tag>
   <elementGuidId>e28f57f9-6628-4fe8-9040-9a8eda4344a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[4]/div/div/div[3]/div/div/div/div/div[4]/div/div/div/div/div[2]/div[2]/div/div/div[3]/div[2]/div/div/div/button/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Approve quotation</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;react-main-container&quot;]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/div[@class=&quot;index__detailBody__1hIqn&quot;]/div[@class=&quot;container index__paddingTop40__2yAeR index__bgGrey__2025-&quot;]/div[@class=&quot;row false&quot;]/div[@class=&quot;false col-md-9 col-xs-12&quot;]/div[1]/div[@class=&quot;react-swipeable-view-container&quot;]/div[4]/div[@class=&quot;index__tabBody__3l3q3&quot;]/div[@class=&quot;col-md-12 no-space&quot;]/div[@class=&quot;index__menu__34BQp col-md-12&quot;]/div[1]/div[@class=&quot;payment-section row&quot;]/div[@class=&quot;col-md-11 col-sm-12 index__mobileSection__QPC39&quot;]/div[@class=&quot;index__card__jGCW5 index__active__3azTH&quot;]/div[1]/div[3]/div[@class=&quot;col-md-12 no-space&quot;]/div[@class=&quot;clearfix&quot;]/div[@class=&quot;text-center&quot;]/div[1]/button[1]/div[1]/div[1]/span[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[4]/div/div/div[3]/div/div/div/div/div[4]/div/div/div/div/div[2]/div[2]/div/div/div[3]/div[2]/div/div/div/button/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quotation'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Seller added to invitation list'])[1]/preceding::span[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='VIEW'])[1]/preceding::span[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Approve quotation']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/button/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
