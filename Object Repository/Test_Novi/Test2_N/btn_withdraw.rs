<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_withdraw</name>
   <tag></tag>
   <elementGuidId>000d0e45-f235-4086-9ac1-f44dc62f98bb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[.='Withdraw from project']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[.='Withdraw from project']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
</WebElementEntity>
