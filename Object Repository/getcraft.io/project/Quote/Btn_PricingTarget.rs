<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_PricingTarget</name>
   <tag></tag>
   <elementGuidId>e8eeb831-75b7-409c-a8f8-24b1c5dc5f06</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='index__warpBox__2wyQr clearfix']/a[1]//div[@class='MuiCardContent-root index__boxContent__1the0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='index__warpBox__2wyQr clearfix']/a[1]//div[@class='MuiCardContent-root index__boxContent__1the0']</value>
   </webElementProperties>
</WebElementEntity>
