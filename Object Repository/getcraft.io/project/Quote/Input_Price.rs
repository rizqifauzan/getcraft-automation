<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_Price</name>
   <tag></tag>
   <elementGuidId>437866c8-1a64-4c35-b14b-210a23b1ddfc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'MuiInputBase-input MuiOutlinedInput-input index__inputText__1gUxs') and @value = '0']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tbody[1]/tr[1]/td[4]//input[@class='MuiInputBase-input MuiOutlinedInput-input index__inputText__1gUxs ']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiInputBase-input MuiOutlinedInput-input index__inputText__1gUxs</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
</WebElementEntity>
