<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Div_Metric 4</name>
   <tag></tag>
   <elementGuidId>8c7d0a4e-acbb-4ebc-9e41-79cbb7847b12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//option[(text() = 'Leads' or . = 'Leads') and contains(@id, 'mtrSec2Line1')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>option</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Leads</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mtrSec2Line1</value>
   </webElementProperties>
</WebElementEntity>
