<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_SendtoClient</name>
   <tag></tag>
   <elementGuidId>b23fb670-f3cc-4b45-b557-ed6086dacb6d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[.='Send to Client']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[.='Send to Client']</value>
   </webElementProperties>
</WebElementEntity>
