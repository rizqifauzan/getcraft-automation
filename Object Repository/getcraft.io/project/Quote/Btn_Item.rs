<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Item</name>
   <tag></tag>
   <elementGuidId>3e425038-743a-4bbe-ba0e-cd3647b09f17</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(@class, 'MuiButtonBase-root MuiButton-root MuiButton-contained')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiButtonBase-root MuiButton-root MuiButton-contained</value>
   </webElementProperties>
</WebElementEntity>
