<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_Qty</name>
   <tag></tag>
   <elementGuidId>33195f25-d998-4524-a422-3429f5cb9306</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tbody[1]/tr[1]/td[2]//input[@class='MuiInputBase-input MuiOutlinedInput-input index__inputText__1gUxs ']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tbody[1]/tr[1]/td[2]//input[@class='MuiInputBase-input MuiOutlinedInput-input index__inputText__1gUxs ']</value>
   </webElementProperties>
</WebElementEntity>
