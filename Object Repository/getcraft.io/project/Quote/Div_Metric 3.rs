<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Div_Metric 3</name>
   <tag></tag>
   <elementGuidId>e75ac9c7-8b84-4ca6-a05a-a7382d2ba85f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class = 'MuiInputBase-root MuiOutlinedInput-root index__metricField__yqUJF metricSec1Line1 ']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiInputBase-root MuiOutlinedInput-root index__metricField__yqUJF metricSec1Line1 </value>
   </webElementProperties>
</WebElementEntity>
