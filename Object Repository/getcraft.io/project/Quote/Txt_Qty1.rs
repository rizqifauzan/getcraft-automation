<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_Qty1</name>
   <tag></tag>
   <elementGuidId>8d50548b-0ff7-40b3-b32c-6ef24df10acf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tr[2]/td[2]//input[@class='MuiInputBase-input MuiOutlinedInput-input index__inputText__1gUxs ']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tr[2]//input[@value='1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tr[2]/td[2]//input[@class='MuiInputBase-input MuiOutlinedInput-input index__inputText__1gUxs ']</value>
   </webElementProperties>
</WebElementEntity>
