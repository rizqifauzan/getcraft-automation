<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Pricing</name>
   <tag></tag>
   <elementGuidId>8dac9d86-3134-454e-a8f7-a51da5ea6e14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'buttonChoosesc__TitleContent-hzixcx-2 eVhKkI') and (text() = 'Pricing' or . = 'Pricing')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='buttonChoosesc__WarpBox-hzixcx-0 hqbRoV clearfix']//div[@class='MuiPaper-root MuiCard-root buttonChoosesc__MainBox-hzixcx-4 kScACC MuiPaper-elevation1 MuiPaper-rounded']/div[.='Pricing(Good for those i.e, Designers, Writers, Photographers etc)']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>buttonChoosesc__TitleContent-hzixcx-2 eVhKkI</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pricing</value>
   </webElementProperties>
</WebElementEntity>
