<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Desc_empty</name>
   <tag></tag>
   <elementGuidId>eca7242d-9a9f-4efc-9018-3f532b023df9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'pull-right index__errorText__Iahgp hidden-xs hidden-sm']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pull-right index__errorText__Iahgp hidden-xs hidden-sm</value>
   </webElementProperties>
</WebElementEntity>
