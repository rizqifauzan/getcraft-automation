<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>delete_line</name>
   <tag></tag>
   <elementGuidId>079c05bc-1d02-4c64-bc85-d7cab5d16df3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'MuiSvgIcon-root index__closeItem__XF8wZ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiSvgIcon-root index__closeItem__XF8wZ</value>
   </webElementProperties>
</WebElementEntity>
