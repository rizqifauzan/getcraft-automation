<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_withdraw</name>
   <tag></tag>
   <elementGuidId>0e808f95-c6ab-4c3e-ac13-b1c1e462f88f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Withdraw from project' or . = 'Withdraw from project')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;index__idSwipables__2aISR&quot;]/div/div/div[3]/div/div[2]/div[1]/div[1]/div/div/div/div/div/div/div[3]/div/div/div/div/div[1]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Withdraw from project</value>
   </webElementProperties>
</WebElementEntity>
