<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Review quote</name>
   <tag></tag>
   <elementGuidId>d256d5fe-cbde-43e6-acb3-b1e137001126</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='index__idSwipables__2aISR']/div/div/div[3]/div/div[2]/div[2]/div/div/a/div/div/div/div/button/div/div/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;index__idSwipables__2aISR&quot;)/div[1]/div[@class=&quot;react-swipeable-view-container&quot;]/div[3]/div[1]/div[@class=&quot;container index__paddingTop40__2yAeR&quot;]/div[@class=&quot;col-md-3&quot;]/div[1]/div[@class=&quot;client-summary-tab&quot;]/a[@class=&quot;index__link__1uenW&quot;]/div[@class=&quot;index__table-view__3vffw&quot;]/div[@class=&quot;index__table-cell-view__2Zaz3&quot;]/div[1]/div[@class=&quot;form-group no-margin-bottom-md&quot;]/button[1]/div[1]/div[1]/span[1][count(. | //span[(text() = 'Review quote' or . = 'Review quote')]) = count(//span[(text() = 'Review quote' or . = 'Review quote')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Review quote</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;index__idSwipables__2aISR&quot;)/div[1]/div[@class=&quot;react-swipeable-view-container&quot;]/div[3]/div[1]/div[@class=&quot;container index__paddingTop40__2yAeR&quot;]/div[@class=&quot;col-md-3&quot;]/div[1]/div[@class=&quot;client-summary-tab&quot;]/a[@class=&quot;index__link__1uenW&quot;]/div[@class=&quot;index__table-view__3vffw&quot;]/div[@class=&quot;index__table-cell-view__2Zaz3&quot;]/div[1]/div[@class=&quot;form-group no-margin-bottom-md&quot;]/button[1]/div[1]/div[1]/span[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='index__idSwipables__2aISR']/div/div/div[3]/div/div[2]/div[2]/div/div/a/div/div/div/div/button/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(0)'])[2]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Archived'])[1]/following::span[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel Project'])[2]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Seller added to invitation list'])[2]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div[2]/div/div/a/div/div/div/div/button/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
