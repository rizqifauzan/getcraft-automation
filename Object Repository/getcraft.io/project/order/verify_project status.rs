<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_project status</name>
   <tag></tag>
   <elementGuidId>f55afffe-7846-4438-8f10-cc0bbea6ea6d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>.cKZOtq</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'indexsc__BriefLabel-sc')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[.='Quote created']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>indexsc__BriefLabel-sc</value>
   </webElementProperties>
</WebElementEntity>
