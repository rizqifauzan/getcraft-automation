<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_status running</name>
   <tag></tag>
   <elementGuidId>b52093ad-022e-4e10-8d27-8a78cf52420a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>.kSOkzh</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[.='Quote created']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'indexsc__BriefLabel-sc')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>indexsc__BriefLabel-sc</value>
   </webElementProperties>
</WebElementEntity>
