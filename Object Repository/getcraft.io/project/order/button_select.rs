<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_select</name>
   <tag></tag>
   <elementGuidId>92fc89fc-7363-4079-92e7-59b47f0bcbec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>.//*[normalize-space(text()) and normalize-space(.)='Archive'])[1]/following::span[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>string</name>
      <type>Main</type>
      <value>//*[@id=&quot;index__idSwipables__2aISR&quot;]/div/div/div[3]/div/div[2]/div[1]/div[1]/div/div[3]/div/div[2]/div/div/div[3]/div/div/div/div[1]/div/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>.//*[normalize-space(text()) and normalize-space(.)='Archive'])[1]/following::span[3]</value>
   </webElementProperties>
</WebElementEntity>
