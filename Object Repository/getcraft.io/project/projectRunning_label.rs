<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>projectRunning_label</name>
   <tag></tag>
   <elementGuidId>fb46aeee-59fc-4349-80b6-4b4c4af59dbd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;app&quot;]/div/div[4]/div/main/section/div[4]/div/div[2]/div[1]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Project running' or . = 'Project running')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Project running</value>
   </webElementProperties>
</WebElementEntity>
