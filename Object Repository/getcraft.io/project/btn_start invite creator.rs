<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_start invite creator</name>
   <tag></tag>
   <elementGuidId>b308daf6-f633-4aca-a8cc-ee8a75bcbd79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Start inviting creators' or . = 'Start inviting creators')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div[1]/span/div[2]/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div > div.widget-app-container.Application__WidgetAppContainer-cMhpjo.lcWceR > span > div.p-top-8.p-bottom-4.InitialMessageBubble__StyleWrapper-eRtbPx.DnsJf > div > button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Start inviting creators</value>
   </webElementProperties>
</WebElementEntity>
