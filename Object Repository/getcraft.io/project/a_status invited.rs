<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_status invited</name>
   <tag></tag>
   <elementGuidId>7b243a41-67c4-4642-8d31-6755623758e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;app&quot;]/div/div[5]/div/main/section/div[3]/div/div[2]/div[2]/a/article[count(. | //*[(text() = 'Invited' or . = 'Invited')]) = count(//*[(text() = 'Invited' or . = 'Invited')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[5]/div/main/section/div[3]/div/div[2]/div[2]/a/article</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Invited</value>
   </webElementProperties>
</WebElementEntity>
