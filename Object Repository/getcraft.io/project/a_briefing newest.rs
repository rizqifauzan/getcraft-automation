<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_briefing newest</name>
   <tag></tag>
   <elementGuidId>b3cdb16a-f436-4afc-bf5b-3cbfdb6b347a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'indexsc__ContentContainer-sc-1tonu19-1 eSHQEK index__projectDesc__3DYk5')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>indexsc__ContentContainer-sc-1tonu19-1 eSHQEK index__projectDesc__3DYk5</value>
   </webElementProperties>
</WebElementEntity>
