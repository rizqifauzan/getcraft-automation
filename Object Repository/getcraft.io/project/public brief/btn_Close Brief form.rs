<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Close Brief form</name>
   <tag></tag>
   <elementGuidId>24340293-4f7b-46da-be02-35d17601bebf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@data-testid, 'microbrief_close-btn')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/div[2]/div/div[1]/h6/svg</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>microbrief_close-btn</value>
   </webElementProperties>
</WebElementEntity>
