<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Creator</name>
   <tag></tag>
   <elementGuidId>0e7198bb-cf93-424c-b7ad-a9f2fec2ab3a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'BriefCardBasicsc__ProfileWrapper-sc-1ddsv7y-1 MLFwN')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>BriefCardBasicsc__ProfileWrapper-sc-1ddsv7y-1 MLFwN</value>
   </webElementProperties>
</WebElementEntity>
