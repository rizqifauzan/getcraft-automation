<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Project Detail</name>
   <tag></tag>
   <elementGuidId>1a0a869c-50d1-4a97-a1c4-5936367318f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'InformationSectionsc__DetailWrapper-sc-7yf14x-0 jCOdtp')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>InformationSectionsc__DetailWrapper-sc-7yf14x-0 jCOdtp</value>
   </webElementProperties>
</WebElementEntity>
