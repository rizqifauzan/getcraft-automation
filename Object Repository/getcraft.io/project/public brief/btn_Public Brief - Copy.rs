<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Public Brief - Copy</name>
   <tag></tag>
   <elementGuidId>8fe5ed87-c259-4d41-a865-cefa698a250e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = concat('Post a Job (It' , &quot;'&quot; , 's free!)') or . = concat('Post a Job (It' , &quot;'&quot; , 's free!)'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Post a Job (It's free!)</value>
   </webElementProperties>
</WebElementEntity>
