<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_projectDetail</name>
   <tag></tag>
   <elementGuidId>4bb876b5-9daf-4c08-940b-b41ab3dec621</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#rdw-wrapper-1377 > div.editorStyle___3pg8w.editorArea___B9wge.rdw-editor-main > div > div > div > div > div > div > span > span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'public-DraftEditor-content')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;rdw-wrapper-1377&quot;]/div[2]/div/div/div/div/div/div/span/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>public-DraftEditor-content</value>
   </webElementProperties>
</WebElementEntity>
