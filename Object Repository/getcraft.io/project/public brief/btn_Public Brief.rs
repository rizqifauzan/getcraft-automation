<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Public Brief</name>
   <tag></tag>
   <elementGuidId>8ce3367d-6fa9-44e8-a5fb-90e012a44f42</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = concat('Post a Job (It' , &quot;'&quot; , 's free!)') or . = concat('Post a Job (It' , &quot;'&quot; , 's free!)'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Post a Job (It's free!)</value>
   </webElementProperties>
</WebElementEntity>
