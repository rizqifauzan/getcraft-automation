<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_logIn_savedSearch</name>
   <tag></tag>
   <elementGuidId>71f8351e-652a-4474-8c0d-f1dd684a8672</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>.MuiButton-outlined</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='MuiButtonBase-root MuiButton-root MuiButton-contained Buttonsc-sc-1fu0k89-0 kztHDC SavedSearchDialogsc__StyledButton-b3vses-4 cFgjtz MuiButton-containedPrimary']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
