<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_newest project</name>
   <tag></tag>
   <elementGuidId>1b06bb1a-80d2-437f-9d47-ef14aaf8123f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'indexsc__ContentContainer-sc-13sxo57-1')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>indexsc__ContentContainer-sc-13sxo57-1 bvXTpL projectDesc</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>indexsc__ContentContainer-sc-13sxo57-1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>indexsc__ContentContainer-sc-13sxo57-1</value>
   </webElementProperties>
</WebElementEntity>
