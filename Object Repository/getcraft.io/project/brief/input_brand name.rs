<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_brand name</name>
   <tag></tag>
   <elementGuidId>d3ec5438-fb01-45db-8c97-49489708567f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@id, 'brand-Thisprojectfor-Brandname')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>brand-Thisprojectfor-Brandname</value>
   </webElementProperties>
</WebElementEntity>
