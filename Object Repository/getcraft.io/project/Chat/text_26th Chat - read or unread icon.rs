<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_26th Chat - read or unread icon</name>
   <tag></tag>
   <elementGuidId>92a255cf-d209-46b8-bc62-aaa5843b7d3d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;index__idSwipables__2aISR&quot;]/div/div/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div[26]/div/div/p[3]/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;index__idSwipables__2aISR&quot;]/div/div/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div[26]/div/div/p[3]/img</value>
   </webElementProperties>
</WebElementEntity>
