<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_send upload</name>
   <tag></tag>
   <elementGuidId>226bb817-3841-4d77-9d1d-f277a429541d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'MuiButtonBase-root MuiIconButton-root indexsc__SubmitButton-sc')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiButtonBase-root MuiIconButton-root indexsc__SubmitButton-sc</value>
   </webElementProperties>
</WebElementEntity>
