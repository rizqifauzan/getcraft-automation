<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CharRoom-Jingpin</name>
   <tag></tag>
   <elementGuidId>ad04a922-142e-43a9-a140-8af2c9940094</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'ChatItem__roomName__') and (text() = 'Jingpin Uniq' or . = 'Jingpin Uniq')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ChatItem__roomName__</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jingpin Uniq</value>
   </webElementProperties>
</WebElementEntity>
