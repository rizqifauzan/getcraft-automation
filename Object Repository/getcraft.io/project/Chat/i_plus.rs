<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_plus</name>
   <tag></tag>
   <elementGuidId>f232bbec-bb04-4713-a257-7b43a102fb9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//i[(text() = 'add' or . = 'add')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-icons Icon__icon__3jvux Icon__clickable__3Mb-u </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>add</value>
   </webElementProperties>
</WebElementEntity>
