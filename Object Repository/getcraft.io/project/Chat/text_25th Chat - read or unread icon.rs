<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_25th Chat - read or unread icon</name>
   <tag></tag>
   <elementGuidId>017b749b-ce2f-4ee6-906e-69ef42a25729</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;index__idSwipables__2aISR&quot;]/div/div/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div[25]/div/div/p[3]/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;index__idSwipables__2aISR&quot;]/div/div/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div[25]/div/div/p[3]/img</value>
   </webElementProperties>
</WebElementEntity>
