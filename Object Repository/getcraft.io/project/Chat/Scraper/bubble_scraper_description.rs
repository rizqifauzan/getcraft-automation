<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>bubble_scraper_description</name>
   <tag></tag>
   <elementGuidId>0472f373-70a4-4dcd-9841-dcd542e1135f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'MetaDataUrlInfo__metaurlDescription')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MetaDataUrlInfo__metaurlDescription</value>
   </webElementProperties>
</WebElementEntity>
