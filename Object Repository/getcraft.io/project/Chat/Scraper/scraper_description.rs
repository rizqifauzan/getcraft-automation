<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>scraper_description</name>
   <tag></tag>
   <elementGuidId>6463065d-495f-487e-b545-41043988e113</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'MetaDataUrlInfo__metaurlDescription')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MetaDataUrlInfo__metaurlDescription</value>
   </webElementProperties>
</WebElementEntity>
