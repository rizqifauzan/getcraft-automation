<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>scraper_title</name>
   <tag></tag>
   <elementGuidId>da99dbff-cc2a-424f-a7f4-1f9cf92465c5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'MetaDataUrlInfo__metaurlTitle')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MetaDataUrlInfo__metaurlTitle</value>
   </webElementProperties>
</WebElementEntity>
