<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_send</name>
   <tag></tag>
   <elementGuidId>b7f38759-8e5a-49cd-ab14-abd519a126b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'ChatInputBox__senButtonWrapper')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ChatInputBox__senButtonWrapper</value>
   </webElementProperties>
</WebElementEntity>
