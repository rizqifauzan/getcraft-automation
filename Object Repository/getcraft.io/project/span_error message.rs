<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_error message</name>
   <tag></tag>
   <elementGuidId>c1b73c64-68aa-45f1-87b3-2ea2abd80355</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;no-access-snackbar&quot;]/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;no-access-snackbar&quot;]/div/div/span</value>
   </webElementProperties>
</WebElementEntity>
