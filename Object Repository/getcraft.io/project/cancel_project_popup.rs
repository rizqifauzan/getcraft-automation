<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>cancel_project_popup</name>
   <tag></tag>
   <elementGuidId>207528d0-62d2-4e0d-8f87-d8d137db7214</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[22]/div/div[1]/div/div/div[2]/button[2]/div/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Cancel Project' or . = 'Cancel Project')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cancel Project</value>
   </webElementProperties>
</WebElementEntity>
