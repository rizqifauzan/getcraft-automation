<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_SubmitQuote</name>
   <tag></tag>
   <elementGuidId>ba4b54f7-f997-4171-8c6e-f3872e76fc9f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Send to Client' or . = 'Send to Client')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Send to Client</value>
   </webElementProperties>
</WebElementEntity>
