<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Yes we are3</name>
   <tag></tag>
   <elementGuidId>e815ab3a-75cd-43ec-b97b-19cab9c33a1a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;app&quot;)/div[@class=&quot;react-main-container&quot;]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/main[@class=&quot;page-content no-space padding-page-content no-padding-all bgColor-page-content min-height&quot;]/section[@class=&quot;container undefined index__MarginTop__11LJn undefined&quot;]/div[3]/div[1]/div[2]/div[@class=&quot;col-xs-6 col-sm-4 col-md-3 col-lg-5ths&quot;]/a[1]/article[@class=&quot;portlet no-shadow light no-space&quot;]/div[@class=&quot;portlet-body no-space&quot;]/div[@class=&quot;index__projectItem__3rxtJ&quot;]/div[@class=&quot;index__projectDesc__3DYk5&quot;]/div[@class=&quot;margin-bottom-s index__projectName__1iXIA&quot;][count(. | //*[@class = 'margin-bottom-s index__projectName__']) = count(//*[@class = 'margin-bottom-s index__projectName__'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[4]/div/main/section/div[3]/div/div[2]/div[2]/a/article/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>margin-bottom-s index__projectName__</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Yes we are</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;react-main-container&quot;]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/main[@class=&quot;page-content no-space padding-page-content no-padding-all bgColor-page-content min-height&quot;]/section[@class=&quot;container undefined index__MarginTop__11LJn undefined&quot;]/div[3]/div[1]/div[2]/div[@class=&quot;col-xs-6 col-sm-4 col-md-3 col-lg-5ths&quot;]/a[1]/article[@class=&quot;portlet no-shadow light no-space&quot;]/div[@class=&quot;portlet-body no-space&quot;]/div[@class=&quot;index__projectItem__3rxtJ&quot;]/div[@class=&quot;index__projectDesc__3DYk5&quot;]/div[@class=&quot;margin-bottom-s index__projectName__1iXIA&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[4]/div/main/section/div[3]/div/div[2]/div[2]/a/article/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create new project'])[1]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Briefing'])[1]/following::div[12]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not for Sale'])[1]/preceding::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Videographers'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//article/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
