<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_additional requirement</name>
   <tag></tag>
   <elementGuidId>bad9bd4f-6e52-4f50-a3f3-6e791d1ed5c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-placeholder = 'Explain your additional requirements if you have any']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/main/section/div/div/div[1]/form/div[8]/div/div/div/div[1]/div[2]/div[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-placeholder</name>
      <type>Main</type>
      <value>Explain your additional requirements if you have any</value>
   </webElementProperties>
</WebElementEntity>
