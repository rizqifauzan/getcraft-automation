<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>amanda_chat popup</name>
   <tag></tag>
   <elementGuidId>8cc34c20-f7b4-4212-946d-127d81c2b39e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class = 'UIIcon__IconContent-kJgGvp ccFFBM']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div/div[1]/span/div[2]/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>UIIcon__IconContent-kJgGvp ccFFBM</value>
   </webElementProperties>
</WebElementEntity>
