<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_upload file -- submit</name>
   <tag></tag>
   <elementGuidId>0b35861a-2d3b-4799-86b2-37c2274251e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div/span[(text() = 'Save' or . = 'Save')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Save</value>
   </webElementProperties>
</WebElementEntity>
