<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_target audiance</name>
   <tag></tag>
   <elementGuidId>8612fdc1-c034-43f9-abf0-34fe7baf59ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-placeholder = 'Who do you want to reach in this campaign?']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/main/section/div/div/div[1]/form/div[7]/div/div/div/div[1]/div[2]/div[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-placeholder</name>
      <type>Main</type>
      <value>Who do you want to reach in this campaign?</value>
   </webElementProperties>
</WebElementEntity>
