<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Setlement period</name>
   <tag></tag>
   <elementGuidId>e6bc1ef0-7694-4fc7-a4fb-1a85d39c9e64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@id, 'dayspayable-undefined-Settlementperiod')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dayspayable-undefined-Settlementperiod-33482&quot;)/div[1]/div[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dayspayable-undefined-Settlementperiod</value>
   </webElementProperties>
</WebElementEntity>
