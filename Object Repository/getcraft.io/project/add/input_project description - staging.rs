<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_project description - staging</name>
   <tag></tag>
   <elementGuidId>a1d1b145-66dc-42ca-b6a2-c1712af1ea12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[@data-placeholder = concat('What' , &quot;'&quot; , 's your big idea?')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;app&quot;]/div/div[5]/div/div/main/section/div/div/div[1]/form/div[6]/div/div/div/div[1]/div[2]/div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/main/section/div/div/div[1]/form/div[6]/div/div/div/div[1]/div[2]/div[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-placeholder</name>
      <type>Main</type>
      <value>What's your big idea?</value>
   </webElementProperties>
</WebElementEntity>
