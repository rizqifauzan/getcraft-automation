<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_upload reference</name>
   <tag></tag>
   <elementGuidId>655a1e67-55c4-431d-842f-0f8ae127e4d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div/input[@type = 'file']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div/input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>file</value>
   </webElementProperties>
</WebElementEntity>
