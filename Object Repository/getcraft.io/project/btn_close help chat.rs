<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_close help chat</name>
   <tag></tag>
   <elementGuidId>f9e9adf2-ea07-4811-9854-f7f9869fe912</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div[1]/span/div[2]/div/button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div[1]/span/div[2]/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div > div.widget-app-container.Application__WidgetAppContainer-cMhpjo.lcWceR > span > div.p-top-8.p-bottom-4.InitialMessageBubble__StyleWrapper-eRtbPx.DnsJf > div > button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div[1]/span/div[2]/div/button</value>
   </webElementProperties>
</WebElementEntity>
