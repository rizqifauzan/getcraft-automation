<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_congratulations</name>
   <tag></tag>
   <elementGuidId>383693d0-1211-4813-bbb2-616b870267f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//p[.='Your payment &amp; project are complete']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//p[.='Your payment &amp; project are complete']</value>
   </webElementProperties>
</WebElementEntity>
