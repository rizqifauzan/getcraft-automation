<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>box_quotation approval</name>
   <tag></tag>
   <elementGuidId>82d53a0d-8a6d-4b70-86fd-933ebaa9ee30</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'index__box-product-selected__2O4Hp clearfix')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='index__box-product-selected__MyXtz clearfix']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>string</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/main/section/div/div[1]/div/div[3]/div[3]/div/div/div/div/div[2]/div[2]/div/div/div/div[2]/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/div[3]/div/div[1]/div/div/div[4]/div/div/div/div/div[2]/div[2]/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__box-product-selected__2O4Hp clearfix</value>
   </webElementProperties>
</WebElementEntity>
