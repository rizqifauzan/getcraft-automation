<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_Month Year</name>
   <tag></tag>
   <elementGuidId>c5336fbb-929a-4c4d-a884-fa2a591496f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'index__cardExpiry__3aqXU  StripeElement StripeElement--empty']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__cardExpiry__3aqXU  StripeElement StripeElement--empty</value>
   </webElementProperties>
</WebElementEntity>
