<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_ I have paid</name>
   <tag></tag>
   <elementGuidId>3a0d1397-9e37-4a42-9984-eaa257e0db28</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[.='Yes, i have paid']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'I have paid' or . = 'I have paid')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>I have paid</value>
   </webElementProperties>
</WebElementEntity>
