<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Pay now</name>
   <tag></tag>
   <elementGuidId>97986092-fabc-4b05-bd8b-dd030f622304</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//btn[@data-testid = 'btn-pay-now']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[.='Pay Now']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>btn</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>btn-pay-now</value>
   </webElementProperties>
</WebElementEntity>
