<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_invoice status</name>
   <tag></tag>
   <elementGuidId>245ac65a-d435-4747-8705-31bd04ee6ab0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'mdi mdi-check']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//i[@class='mdi mdi-timer-sand-empty']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mdi mdi-check</value>
   </webElementProperties>
</WebElementEntity>
