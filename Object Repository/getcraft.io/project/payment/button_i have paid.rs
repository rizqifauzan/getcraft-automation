<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_i have paid</name>
   <tag></tag>
   <elementGuidId>40c9e457-b732-4309-9914-05828c598298</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[.='I have paid']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Pay Now' or . = 'Pay Now')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pay Now</value>
   </webElementProperties>
</WebElementEntity>
