<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_Pay</name>
   <tag></tag>
   <elementGuidId>af51f345-3086-4f28-9f6f-fde4f505f6a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'CreditCardMethodsc__BtnPay-sc-1q8dubc-12 dECCAn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>CreditCardMethodsc__BtnPay-sc-1q8dubc-12 dECCAn</value>
   </webElementProperties>
</WebElementEntity>
