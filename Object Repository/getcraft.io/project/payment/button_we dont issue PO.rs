<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_we dont issue PO</name>
   <tag></tag>
   <elementGuidId>6a6a5754-2f3f-4ca2-9eac-c006154466a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = concat('We don' , &quot;'&quot; , 't issue PO') or . = concat('We don' , &quot;'&quot; , 't issue PO'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>string</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/main/section/div/div[1]/div/div[3]/div[3]/div/div/div/div/div[3]/div[2]/div/div/div[3]/div/div[2]/div/div[1]/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/div[3]/div/div[1]/div/div/div[4]/div/div/div/div/div[3]/div[2]/div/div/div[3]/div/div[2]/div/div[1]/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>We don't issue PO</value>
   </webElementProperties>
</WebElementEntity>
