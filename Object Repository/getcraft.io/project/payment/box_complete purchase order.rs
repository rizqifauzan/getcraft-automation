<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>box_complete purchase order</name>
   <tag></tag>
   <elementGuidId>1e8c0ea3-6793-4832-a61a-926b085c7c15</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'index__box-product-selected__22tt4 clearfix')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='index__box-product-selected__3fEck clearfix']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>string</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/main/section/div/div[1]/div/div[3]/div[3]/div/div/div/div/div[3]/div[2]/div/div/div/div[2]/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/div[3]/div/div[1]/div/div/div[4]/div/div/div/div/div[3]/div[2]/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__box-product-selected__22tt4 clearfix</value>
   </webElementProperties>
</WebElementEntity>
