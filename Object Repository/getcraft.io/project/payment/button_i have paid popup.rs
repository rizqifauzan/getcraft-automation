<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_i have paid popup</name>
   <tag></tag>
   <elementGuidId>3a28b226-e780-4ad9-8083-a8b78c99822a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[.='Yes, i have paid']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'I have paid' or . = 'I have paid')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>I have paid</value>
   </webElementProperties>
</WebElementEntity>
