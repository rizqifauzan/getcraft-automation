<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>box_complete payment</name>
   <tag></tag>
   <elementGuidId>4681f2da-1de2-4c5d-8be9-a1a688d03088</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/main/section/div/div[1]/div/div[3]/div[3]/div/div/div/div/div[4]/div[2]/div/div/div/div[2]/div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='index__box-product-selected__1qBs0 clearfix']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/main/section/div/div[1]/div/div[3]/div[3]/div/div/div/div/div[4]/div[2]/div/div/div/div[2]/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Complete Project</value>
   </webElementProperties>
</WebElementEntity>
