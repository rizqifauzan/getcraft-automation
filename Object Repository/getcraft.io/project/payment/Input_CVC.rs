<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_CVC</name>
   <tag></tag>
   <elementGuidId>eb49b1ce-117d-4ee3-a638-c953be513a11</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'index__cardCvv__2LmZu  StripeElement StripeElement--empty']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__cardCvv__2LmZu  StripeElement StripeElement--empty</value>
   </webElementProperties>
</WebElementEntity>
