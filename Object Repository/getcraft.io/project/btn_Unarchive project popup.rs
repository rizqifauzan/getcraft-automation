<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Unarchive project popup</name>
   <tag></tag>
   <elementGuidId>b1ac9b9a-19c6-4bea-9db5-6054b6823062</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Unarchive' or . = 'Unarchive')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[22]/div/div[1]/div/div/div[2]/button[2]/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Unarchive</value>
   </webElementProperties>
</WebElementEntity>
