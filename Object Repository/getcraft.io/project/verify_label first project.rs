<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_label first project</name>
   <tag></tag>
   <elementGuidId>ac81d898-2dc9-49c8-a854-818c967c7694</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'margin-bottom-xs index__projectName__1iXIA']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/main/section/div[3]/div/div[2]/div[2]/a/article/div/div/div/div[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>margin-bottom-xs index__projectName__1iXIA</value>
   </webElementProperties>
</WebElementEntity>
