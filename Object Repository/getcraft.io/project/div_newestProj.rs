<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_newestProj</name>
   <tag></tag>
   <elementGuidId>29646dc1-48f9-4d16-a692-ab047c2bdee8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'margin-bottom-xs index__projectName') and (text() = 'GlobalVariable.project_name' or . = 'GlobalVariable.project_name')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>margin-bottom-xs index__projectName</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>GlobalVariable.project_name</value>
   </webElementProperties>
</WebElementEntity>
