<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>add_creator</name>
   <tag></tag>
   <elementGuidId>b044754c-aa4f-48a4-9118-db239f3a22df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/div[2]/div/div[1]/div/div/div/div/div[1]/div/div[1]/div[2]/i</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'UtilitySection__iconHolder__')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>UtilitySection__iconHolder__</value>
   </webElementProperties>
</WebElementEntity>
