<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_30 Days Settlement</name>
   <tag></tag>
   <elementGuidId>92345297-0cc3-49af-9594-218ffffbdc9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3[(text() = '30 Days Settlement' or . = '30 Days Settlement')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>30 Days Settlement</value>
   </webElementProperties>
</WebElementEntity>
