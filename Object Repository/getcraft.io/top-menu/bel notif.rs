<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>bel notif</name>
   <tag></tag>
   <elementGuidId>b3a487f8-d7b2-407c-abee-c81fd05f09b6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'MuiSvgIcon-root indexsc__StyledNotificationIcon')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiSvgIcon-root indexsc__StyledNotificationIcon</value>
   </webElementProperties>
</WebElementEntity>
