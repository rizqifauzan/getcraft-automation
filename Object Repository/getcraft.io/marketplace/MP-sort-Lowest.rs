<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MP-sort-Lowest</name>
   <tag></tag>
   <elementGuidId>18c51146-1b87-49a1-bb4d-b4ce99cec612</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[(text() = 'Cost - Lowest' or . = 'Cost - Lowest')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cost - Lowest</value>
   </webElementProperties>
</WebElementEntity>
