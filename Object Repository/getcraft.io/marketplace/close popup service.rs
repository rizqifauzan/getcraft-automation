<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>close popup service</name>
   <tag></tag>
   <elementGuidId>51c34153-85b1-4075-8796-b152d7e811bf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'MuiSvgIcon-root ProductDetailPopup__StyledCloseIcon')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/div[3]/div/div[1]/h2/div[2]/div[2]/svg</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiSvgIcon-root ProductDetailPopup__StyledCloseIcon</value>
   </webElementProperties>
</WebElementEntity>
