<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Brief Case</name>
   <tag></tag>
   <elementGuidId>fc65a69e-677e-4466-8cc8-5da3d5eb052c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(@class, 'ProfileDetailsc__ServiceButton-idnzxt-11 karDd btn-gc-border___3OQlN')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ProfileDetailsc__ServiceButton-idnzxt-11 karDd btn-gc-border___3OQlN</value>
   </webElementProperties>
</WebElementEntity>
