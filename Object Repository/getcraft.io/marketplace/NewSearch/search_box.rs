<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>search_box</name>
   <tag></tag>
   <elementGuidId>b52912fe-079b-4a1b-aa38-ffd68025f4c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-id = 'searchInputWithAutoSuggest' and @placeholder = 'ie. videographers, designers, influencers etc.']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-id</name>
      <type>Main</type>
      <value>searchInputWithAutoSuggest</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>ie. videographers, designers, influencers etc.</value>
   </webElementProperties>
</WebElementEntity>
