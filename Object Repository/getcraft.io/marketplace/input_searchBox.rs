<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_searchBox</name>
   <tag></tag>
   <elementGuidId>f27768de-ba70-4b68-93c9-34372051b4e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-id = 'searchInputWithAutoSuggest']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-id</name>
      <type>Main</type>
      <value>searchInputWithAutoSuggest</value>
   </webElementProperties>
</WebElementEntity>
