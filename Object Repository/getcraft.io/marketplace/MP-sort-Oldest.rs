<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MP-sort-Oldest</name>
   <tag></tag>
   <elementGuidId>8ed50bc8-72ca-454d-a8b4-a4953daf1d05</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[(text() = 'Created date - Oldest' or . = 'Created date - Oldest')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Created date - Oldest</value>
   </webElementProperties>
</WebElementEntity>
