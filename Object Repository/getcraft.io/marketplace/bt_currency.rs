<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>bt_currency</name>
   <tag></tag>
   <elementGuidId>585b7609-c30f-4d38-b0e9-3e70f0ccb465</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'CurrencyDropDownsc__CurrencyWrapper')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>CurrencyDropDownsc__CurrencyWrapper</value>
   </webElementProperties>
</WebElementEntity>
