<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>service_next</name>
   <tag></tag>
   <elementGuidId>69d6e814-3991-46f2-95e7-730a97ab48d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//header/div/div[1]/div[1]/div[2]/ul/li[1]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'ServiceDetailDialogsc__RightIconStyled')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ServiceDetailDialogsc__RightIconStyled</value>
   </webElementProperties>
</WebElementEntity>
