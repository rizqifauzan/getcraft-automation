<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>result_firstProfile</name>
   <tag></tag>
   <elementGuidId>988e9169-22e1-4f08-a662-e92f3290a79a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'ProfileCardsc__ProfileName-jzxdjt-11 hptKSK') and (text() = 'storyfirst pictures' or . = 'storyfirst pictures')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='profileContainer___3pTPd']/div[1]//div[@class='ProfileCardsc__ProfileName-jzxdjt-11 hptKSK']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ProfileCardsc__ProfileName-jzxdjt-11 hptKSK</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>storyfirst pictures</value>
   </webElementProperties>
</WebElementEntity>
