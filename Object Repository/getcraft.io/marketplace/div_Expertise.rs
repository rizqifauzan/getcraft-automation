<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Expertise</name>
   <tag></tag>
   <elementGuidId>33a6921e-1957-4e46-bbc4-45e14b972dd9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'jss59 ProfileDetailsc__ChipExpertise-idnzxt-20')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss59 ProfileDetailsc__ChipExpertise-idnzxt-20</value>
   </webElementProperties>
</WebElementEntity>
