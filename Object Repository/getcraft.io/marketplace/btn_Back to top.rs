<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Back to top</name>
   <tag></tag>
   <elementGuidId>3320bf58-51f2-4d1e-85da-b626caddfd26</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'BackToTopButtonsc__StyledDiv-sc-1cntjtf-1 fMOgnD']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//header/div/div[1]/div[1]/div[2]/ul/li[1]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>BackToTopButtonsc__StyledDiv-sc-1cntjtf-1 fMOgnD</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
</WebElementEntity>
