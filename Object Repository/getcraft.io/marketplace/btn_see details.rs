<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_see details</name>
   <tag></tag>
   <elementGuidId>e4480a25-91aa-41e6-b95f-c726685763b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'See Details' or . = 'See Details')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;open-service-detail&quot;]/span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>See Details</value>
   </webElementProperties>
</WebElementEntity>
