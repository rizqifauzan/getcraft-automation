<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>service_previous</name>
   <tag></tag>
   <elementGuidId>c0dc6d14-55b4-4cdb-8fec-60f31a8bf5d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//header/div/div[1]/div[1]/div[2]/ul/li[1]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'ServiceDetailDialogsc__LeftIconStyled')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ServiceDetailDialogsc__LeftIconStyled</value>
   </webElementProperties>
</WebElementEntity>
