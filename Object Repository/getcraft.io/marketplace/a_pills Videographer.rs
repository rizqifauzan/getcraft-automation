<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_pills Videographer</name>
   <tag></tag>
   <elementGuidId>5f09b435-9631-4929-aed6-74b70ca61a25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Videographers' or . = 'Videographers')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Videographers</value>
   </webElementProperties>
</WebElementEntity>
