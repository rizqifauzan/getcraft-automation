<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>creator_next</name>
   <tag></tag>
   <elementGuidId>3dd8b12a-a1ec-46d3-9191-ff1969603f92</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//header/div/div[1]/div[1]/div[2]/ul/li[1]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'ServiceDetailDialogsc__LeftIconStyled')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ServiceDetailDialogsc__LeftIconStyled</value>
   </webElementProperties>
</WebElementEntity>
