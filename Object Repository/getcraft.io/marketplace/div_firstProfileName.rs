<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_firstProfileName</name>
   <tag></tag>
   <elementGuidId>44752f83-e563-4127-9e12-d0eac8986e0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'ProfileCardsc__ProfileName-jzxdjt-11 hptKSK')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='ProfileCardsc__ProfileCardWrapper-jzxdjt-2 ccFIwp']//div[@class='ProfileCardsc__ProfileName-jzxdjt-11 hptKSK']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ProfileCardsc__ProfileName-jzxdjt-11 hptKSK</value>
   </webElementProperties>
</WebElementEntity>
