<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>close_mini profile</name>
   <tag></tag>
   <elementGuidId>a5a72a66-0321-4ef8-b7f9-cf280109f38b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id = 'profile-detail-close']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//header/div/div[1]/div[1]/div[2]/ul/li[1]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>profile-detail-close</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
</WebElementEntity>
