<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_addMoreCreator</name>
   <tag></tag>
   <elementGuidId>1b95929d-3a35-4e94-a876-3715f52fe0ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>button[title='Add to existing project']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>button[title='Add to existing project']</value>
   </webElementProperties>
</WebElementEntity>
