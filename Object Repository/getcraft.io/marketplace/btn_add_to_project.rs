<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_add_to_project</name>
   <tag></tag>
   <elementGuidId>9d727a49-168b-43e3-801e-ae0e89ee8b98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = 'Add to project' or . = 'Add to project')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add to project</value>
   </webElementProperties>
</WebElementEntity>
