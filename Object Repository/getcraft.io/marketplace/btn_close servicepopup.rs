<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_close servicepopup</name>
   <tag></tag>
   <elementGuidId>5bdfee5f-076b-4970-b0c4-bd5117c15dc3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'ProductDetailPopup__StyledCloseIcon')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ProductDetailPopup__StyledCloseIcon</value>
   </webElementProperties>
</WebElementEntity>
