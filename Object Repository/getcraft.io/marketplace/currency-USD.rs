<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>currency-USD</name>
   <tag></tag>
   <elementGuidId>7a08a4d1-5b3c-42e8-815e-d28e92905a9b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[(text() = 'USD ' or . = 'USD ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>USD </value>
   </webElementProperties>
</WebElementEntity>
