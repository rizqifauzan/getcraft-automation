<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_celebrities</name>
   <tag></tag>
   <elementGuidId>0020ba81-ebe4-4dbd-beb0-5317835579d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//p[(text() = 'Celebrities &amp; influencers' or . = 'Celebrities &amp; influencers')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/div/section/div/div[1]/div/div[3]/div[1]/a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Celebrities &amp; influencers</value>
   </webElementProperties>
</WebElementEntity>
