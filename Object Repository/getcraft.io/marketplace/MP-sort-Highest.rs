<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MP-sort-Highest</name>
   <tag></tag>
   <elementGuidId>6a6fe16d-7db1-4715-aec5-47fa4dc73824</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[(text() = 'Cost - Highest' or . = 'Cost - Highest')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cost - Highest</value>
   </webElementProperties>
</WebElementEntity>
