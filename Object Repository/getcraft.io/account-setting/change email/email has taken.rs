<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>email has taken</name>
   <tag></tag>
   <elementGuidId>a29cd6ac-1377-4f75-9ac5-ec095ab03f4e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//p[(text() = 'Email has been taken' or . = 'Email has been taken')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/main/section/div/div/div[2]/div/form/div[5]/div/div[2]/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Email has been taken</value>
   </webElementProperties>
</WebElementEntity>
