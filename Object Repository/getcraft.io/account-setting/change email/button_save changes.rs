<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_save changes</name>
   <tag></tag>
   <elementGuidId>f717d419-002f-4d81-9551-9007e432978a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Save Changes' or . = 'Save Changes')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/main/section/div/div/div[2]/div/form/div[5]/div/div[2]/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Save Changes</value>
   </webElementProperties>
</WebElementEntity>
