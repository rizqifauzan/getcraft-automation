<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>email different</name>
   <tag></tag>
   <elementGuidId>39d11c1b-06b7-42d4-852e-188a971904fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//p[(text() = 'Please confirm that this email and your New email are the same' or . = 'Please confirm that this email and your New email are the same')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;app&quot;]/div/div[4]/div/main/section/div/div/div[2]/div/form/div[5]/div/div[2]/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Please confirm that this email and your New email are the same</value>
   </webElementProperties>
</WebElementEntity>
