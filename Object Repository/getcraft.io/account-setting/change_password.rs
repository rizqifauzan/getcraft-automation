<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>change_password</name>
   <tag></tag>
   <elementGuidId>e69fb86d-9aac-4319-a7fa-d3fa01c4e26e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Change password' or . = 'Change password')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Change password</value>
   </webElementProperties>
</WebElementEntity>
