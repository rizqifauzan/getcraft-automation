<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_passw_changes_fail</name>
   <tag></tag>
   <elementGuidId>f60f24c9-928a-4f87-970c-449c8c639f44</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//p[(text() = 'Your current password is wrong' or . = 'Your current password is wrong')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Your current password is wrong</value>
   </webElementProperties>
</WebElementEntity>
