<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_error message</name>
   <tag></tag>
   <elementGuidId>98b1c7be-ca65-4598-b11f-009a22ace7ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>.MuiAlert-root</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[3]/div/div[1]/div/div/div[2]/div/div/form/div[1]/h1</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//h1[(text() = 'User not found' or . = 'User not found')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>User not found</value>
   </webElementProperties>
</WebElementEntity>
