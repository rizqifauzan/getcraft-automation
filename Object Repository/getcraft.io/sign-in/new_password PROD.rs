<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>new_password PROD</name>
   <tag></tag>
   <elementGuidId>fe874a05-78c0-4c38-8801-7f49b3a8942a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@id, 'undefined-Typeyourpassword-undefined') and @type = 'password']</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>undefined-Typeyourpassword-undefined</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
   </webElementProperties>
</WebElementEntity>
