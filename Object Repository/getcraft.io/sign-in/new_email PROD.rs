<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>new_email PROD</name>
   <tag></tag>
   <elementGuidId>b42f9fbf-69ad-4df8-996a-3729208cfaa9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@id, 'undefined-nameemailcom-undefined') and @type = 'email']</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>undefined-nameemailcom-undefined</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
   </webElementProperties>
</WebElementEntity>
