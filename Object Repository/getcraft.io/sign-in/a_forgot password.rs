<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_forgot password</name>
   <tag></tag>
   <elementGuidId>e097f9ca-afe6-4601-979e-8d15a7624848</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[(text() = 'Forgot password' or . = 'Forgot password')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//body[@class='stretched no-transition page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid']/div[5]//a[.='Forgot password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>font-theme index__forgetPassword</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Forgot password</value>
   </webElementProperties>
</WebElementEntity>
