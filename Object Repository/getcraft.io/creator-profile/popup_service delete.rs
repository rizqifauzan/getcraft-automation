<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>popup_service delete</name>
   <tag></tag>
   <elementGuidId>c9efd2fd-b3d0-41d8-a25d-b9634297043d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button/span[(text() = 'Delete' or . = 'Delete')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delete</value>
   </webElementProperties>
</WebElementEntity>
