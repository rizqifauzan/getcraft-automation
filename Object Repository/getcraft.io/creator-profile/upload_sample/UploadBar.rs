<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>UploadBar</name>
   <tag></tag>
   <elementGuidId>bc734b87-3071-4b93-bcec-92f69d9b807c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;uploadBar_QA&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'uploadBar_QA')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>uploadBar_QA</value>
   </webElementProperties>
</WebElementEntity>
