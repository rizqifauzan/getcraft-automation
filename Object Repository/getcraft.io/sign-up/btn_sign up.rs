<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_sign up</name>
   <tag></tag>
   <elementGuidId>4e031b84-d7d1-4070-93d7-ee35aacc5889</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[.='Sign Up']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Sign Up' or . = 'Sign Up')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div/div[3]/header/div/div[4]/div/ul[2]/li[4]/a/div/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign Up</value>
   </webElementProperties>
</WebElementEntity>
