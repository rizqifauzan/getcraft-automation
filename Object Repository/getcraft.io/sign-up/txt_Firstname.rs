<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_Firstname</name>
   <tag></tag>
   <elementGuidId>1fa39ae5-bd6d-4ac8-8b7f-00bcf6ac168b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@id, 'Typeyourfirstname')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Typeyourfirstname</value>
   </webElementProperties>
</WebElementEntity>
