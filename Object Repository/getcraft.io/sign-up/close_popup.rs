<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>close_popup</name>
   <tag></tag>
   <elementGuidId>5c1c1e62-338a-41fd-80f1-1d3f435a337b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'mdi mdi-close font-24 index__closeButton__')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mdi mdi-close font-24 index__closeButton__</value>
   </webElementProperties>
</WebElementEntity>
