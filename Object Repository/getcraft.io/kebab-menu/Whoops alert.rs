<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Whoops alert</name>
   <tag></tag>
   <elementGuidId>29eaf0eb-2cd5-475d-811e-9b91750ec0ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;app&quot;]/div/div[5]/div/div/main/section/div/div/div[2]/div[3]/div[1]/div/article/div[1]/text()</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'alert alert-danger' and (text() = 'This user already invited' or . = 'This user already invited')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>This user already invited</value>
   </webElementProperties>
</WebElementEntity>
