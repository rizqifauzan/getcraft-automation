<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Whoops alert2</name>
   <tag></tag>
   <elementGuidId>28bd1f3e-8e0b-41a7-a5b8-5c094db6adb7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;app&quot;]/div/div[5]/div/div/main/section/div/div/div[2]/div[3]/div[1]/div/article/div[1]/text()</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'alert alert-danger' and (text() = 'This user is already member of this workspace.' or . = 'This user is already member of this workspace.')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>This user is already member of this workspace.</value>
   </webElementProperties>
</WebElementEntity>
