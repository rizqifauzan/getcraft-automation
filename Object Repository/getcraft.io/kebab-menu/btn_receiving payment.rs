<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_receiving payment</name>
   <tag></tag>
   <elementGuidId>eda52edc-27c5-4b9d-8557-8041029bb188</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a/span[(text() = 'Receiving payments' or . = 'Receiving payments')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Receiving payments</value>
   </webElementProperties>
</WebElementEntity>
