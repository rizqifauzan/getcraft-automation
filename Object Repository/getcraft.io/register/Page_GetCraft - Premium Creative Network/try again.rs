<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>try again</name>
   <tag></tag>
   <elementGuidId>fc3922e7-9dab-4146-bcd5-e28aa28c7e45</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//p[contains(@class, 'index__wrongText__')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__wrongText__</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Try again</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;react-main-container&quot;]/div[3]/header[@class=&quot;page-header guest navbar navbar-fixed-top index__header__26T6B index__homepageBg__3Iqd-&quot;]/div[@class=&quot;page-header-inner container page-top index__topFixed__2IL1o index__homepageBg__3Iqd-&quot;]/div[@class=&quot;page-top nav hidden-xs hidden-sm index__homepageBg__3Iqd-&quot;]/div[@class=&quot;top-menu&quot;]/ul[@class=&quot;nav navbar-nav pull-right index__nav__WOgeF&quot;]/li[3]/button[@class=&quot;index__homeSignIn__2CyDg color-gc-blue&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
</WebElementEntity>
