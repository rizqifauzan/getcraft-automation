<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Continue button</name>
   <tag></tag>
   <elementGuidId>bb4e11fa-f73b-43cd-9851-f9b3816300b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[(text() = 'Continue' or . = 'Continue')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//body[@class='stretched no-transition page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid']/div[6]//button[@class='index__buttonSignIn__Vw9PZ']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Continue</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid&quot;]/div[5]/div[1]/div[1]/div[@class=&quot;index__dialogContent__3qF1e&quot;]/div[@class=&quot;index__dialogPaperShort__17EWV&quot;]/div[@class=&quot;index__dialogBody__1sDhy&quot;]/div[@class=&quot;index__bodyContent__4AkCB&quot;]/div[@class=&quot;index__containerForm__21zoL&quot;]/form[1]/button[@class=&quot;index__buttonSignIn__Vw9PZ&quot;]/div[1]</value>
   </webElementProperties>
</WebElementEntity>
