<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Continue buttonNew</name>
   <tag></tag>
   <elementGuidId>2457a8c7-b091-4ad5-ba11-de8724ada0cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[(text() = 'Continue' or . = 'Continue')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[5]/div/div[1]/div/div/div[2]/div/div/form/button
</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Continue</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid&quot;]/div[5]/div[1]/div[1]/div[@class=&quot;index__dialogContent__3qF1e&quot;]/div[@class=&quot;index__dialogPaperShort__17EWV&quot;]/div[@class=&quot;index__dialogBody__1sDhy&quot;]/div[@class=&quot;index__bodyContent__4AkCB&quot;]/div[@class=&quot;index__containerForm__21zoL&quot;]/form[1]/button[@class=&quot;index__buttonSignIn__Vw9PZ&quot;]/div[1]</value>
   </webElementProperties>
</WebElementEntity>
