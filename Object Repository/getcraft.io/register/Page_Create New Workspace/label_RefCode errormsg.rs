<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_RefCode errormsg</name>
   <tag></tag>
   <elementGuidId>754fb435-21ac-45af-b238-b7fe47e19b61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[.='Referral Code (optional)']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//label[@data-testid = 'error-message']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>error-message</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
</WebElementEntity>
