<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_RefCode field</name>
   <tag></tag>
   <elementGuidId>0c22f610-6267-48c0-aabc-77ddbac1ecfa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[.='Referral Code (optional)']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//label[@data-testid = 'referral-title']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>referral-title</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
</WebElementEntity>
