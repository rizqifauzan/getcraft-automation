<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>get desc</name>
   <tag></tag>
   <elementGuidId>7bbdad84-079d-494f-a56a-c0905c011851</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'descriptionField___')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[2]/div/div[2]/div[3]/div[2]/div[1]/div/div[1]/div[2]/div/div/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>descriptionField___</value>
   </webElementProperties>
</WebElementEntity>
