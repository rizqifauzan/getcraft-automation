<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Service description</name>
   <tag></tag>
   <elementGuidId>b8733254-5456-4e32-b4af-7869cbcc5011</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;rdw-wrapper-9925&quot;]/div/div/div[2]/div/div/div/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'notranslate public-DraftEditor-content')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notranslate public-DraftEditor-content</value>
   </webElementProperties>
</WebElementEntity>
