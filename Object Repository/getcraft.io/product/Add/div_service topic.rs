<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_service topic</name>
   <tag></tag>
   <elementGuidId>27cec3b0-e860-4f0b-b344-29c145b5cb6a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;react-select-2-input&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'jss435 jss443 jss468 placeholder___1bu-A')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;__next&quot;]/div/div[2]/div/div/form/div[3]/div/div/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss435 jss443 jss468 placeholder___1bu-A</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
</WebElementEntity>
