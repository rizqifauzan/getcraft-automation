<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>overlay_banner</name>
   <tag></tag>
   <elementGuidId>31c1a584-6445-4b06-bc0f-35e8cee811bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'stylessc__HasBannerWrapper-sc')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;__next&quot;]/div/div[2]/div/div[2]/div[2]/label[2]/svg/path[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>stylessc__HasBannerWrapper-sc</value>
   </webElementProperties>
</WebElementEntity>
