<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>element_banner</name>
   <tag></tag>
   <elementGuidId>659dbaa8-bfb0-47c3-8b16-25504c342b55</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'stylessc__BannerBackground-sc-41o0hx-0 ixjzNS']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;__next&quot;]/div/div[2]/div/div[2]/div[2]/label[2]/svg/path[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>stylessc__BannerBackground-sc-41o0hx-0 ixjzNS</value>
   </webElementProperties>
</WebElementEntity>
