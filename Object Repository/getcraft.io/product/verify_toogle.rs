<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_toogle</name>
   <tag></tag>
   <elementGuidId>99ea0284-ea52-4bdc-8395-0c3d776e6180</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class = 'MuiButtonBase-root MuiIconButton-root jss1 MuiSwitch-switchBase MuiSwitch-colorPrimary jss2 Mui-checked']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiButtonBase-root MuiIconButton-root jss1 MuiSwitch-switchBase MuiSwitch-colorPrimary jss2 Mui-checked</value>
   </webElementProperties>
</WebElementEntity>
