<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>profile farisha</name>
   <tag></tag>
   <elementGuidId>57590ad4-e613-405e-9be4-6cc1e06d2a5d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[(text() = 'FARISHA JAIS' or . = 'FARISHA JAIS') and contains(@class, 'ProfileCardsc__ProfileName')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>FARISHA JAIS</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ProfileCardsc__ProfileName</value>
   </webElementProperties>
</WebElementEntity>
