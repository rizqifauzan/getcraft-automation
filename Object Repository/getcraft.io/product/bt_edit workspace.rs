<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>bt_edit workspace</name>
   <tag></tag>
   <elementGuidId>0c87a229-f9a7-42b8-9faa-dfeb2adb4319</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'btnEditWorkspace_QA')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btnEditWorkspace_QA</value>
   </webElementProperties>
</WebElementEntity>
