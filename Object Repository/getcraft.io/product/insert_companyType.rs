<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>insert_companyType</name>
   <tag></tag>
   <elementGuidId>95a92c8f-87db-4322-baf8-fa53689e72e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='react-select-6-input']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'react-select-6-input']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>inputClassName___1ulj9</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-select-6-input</value>
   </webElementProperties>
</WebElementEntity>
