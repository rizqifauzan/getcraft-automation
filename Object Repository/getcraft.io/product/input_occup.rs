<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_occup</name>
   <tag></tag>
   <elementGuidId>30a5dc61-9977-438a-83e3-3a40d37a8572</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div/div/div[2]/div/div[2]/div[3]/div[3]/div[1]/div/div[1]/div[2]/div[2]/div/div/div/div/div/div[1]/div/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'jss63 jss71 jss96 placeholder___1bu-A')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss63 jss71 jss96 placeholder___1bu-A</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@class='cardContent___2fVTE undefined']//div[@class='valueContainer___3QOlF']/div[1]//input[1]</value>
   </webElementXpaths>
</WebElementEntity>
