<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Account Setting</name>
   <tag></tag>
   <elementGuidId>5472d8a9-6e97-4677-ab4d-f7b6fba4c3e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>.contentBody___3FChh</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;__next&quot;]/div/div[1]/div/div/div[2]/div[3]/div/div[2]/ul/li[11]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//li[(text() = 'Personal settings' or . = 'Personal settings')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag-old</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Personal settings</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiButtonBase-root MuiListItem-root MuiMenuItem-root menuList___3pgdP MuiMenuItem-gutters MuiListItem-gutters MuiListItem-button</value>
   </webElementProperties>
</WebElementEntity>
