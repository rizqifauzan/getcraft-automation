<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_sign out</name>
   <tag></tag>
   <elementGuidId>6132d561-8815-4436-bd35-64146973d1a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Sign out' or . = 'Sign out')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign out</value>
   </webElementProperties>
</WebElementEntity>
