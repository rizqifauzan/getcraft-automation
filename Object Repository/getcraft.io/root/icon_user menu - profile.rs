<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icon_user menu - profile</name>
   <tag></tag>
   <elementGuidId>c19225b4-ecc5-43f6-a8b3-762a47fbc480</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div/button[@class = 'jss52 jss26 jss28 jss31 profileId___2wiKX']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss52 jss26 jss28 jss31 profileId___2wiKX</value>
   </webElementProperties>
</WebElementEntity>
