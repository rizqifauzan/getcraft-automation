<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>homepage</name>
   <tag></tag>
   <elementGuidId>20026fbe-8e4b-4e55-96ed-f49645be4d20</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'logo-default index__logo__')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>logo-default index__logo__</value>
   </webElementProperties>
</WebElementEntity>
