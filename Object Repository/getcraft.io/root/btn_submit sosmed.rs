<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_submit sosmed</name>
   <tag></tag>
   <elementGuidId>a1df0f01-f27e-46a0-8c89-12d9539280ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button/span[(text() = 'Submit' or . = 'Submit')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/div[2]/div/div[3]/button[2]/span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit</value>
   </webElementProperties>
</WebElementEntity>
