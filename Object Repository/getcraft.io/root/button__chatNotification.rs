<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button__chatNotification</name>
   <tag></tag>
   <elementGuidId>75b9ff47-2dcc-4dcc-af7a-6c9176fe0539</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'Iconsc__StyledIcon-up2a7b-0')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>Iconsc__StyledIcon-up2a7b-0</value>
   </webElementProperties>
</WebElementEntity>
