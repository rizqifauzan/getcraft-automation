<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Searchbox_Homepage Bt</name>
   <tag></tag>
   <elementGuidId>99b9a6f7-4dd1-4ec1-bddf-35fc8fcf9860</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'mdi mdi-magnify index__searchButtonIcon')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mdi mdi-magnify index__searchButtonIcon</value>
   </webElementProperties>
</WebElementEntity>
