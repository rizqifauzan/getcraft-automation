<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign In</name>
   <tag></tag>
   <elementGuidId>5e05991a-651f-44ef-b254-7df84189e6df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;app&quot;]/div/div[3]/header/div/div[4]/div/ul[2]/li[3]/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Log in' or . = 'Log in')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Log in</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__homeSignIn</value>
   </webElementProperties>
</WebElementEntity>
