<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_yt link - profile - Copy</name>
   <tag></tag>
   <elementGuidId>1eb35f97-447f-45f4-b297-ffedf6b2cac6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[contains(@placeholder, 'E.g: https://www.youtube.com/channel/XXXX')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>E.g: https://www.youtube.com/channel/XXXX</value>
   </webElementProperties>
</WebElementEntity>
