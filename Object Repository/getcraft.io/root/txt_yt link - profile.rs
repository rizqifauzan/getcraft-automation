<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_yt link - profile</name>
   <tag></tag>
   <elementGuidId>95e37a82-c48e-4d5b-8022-80d46b496c98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[contains(@placeholder, 'E.g: https://www.youtube.com/channel/XXXX')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>E.g: https://www.youtube.com/channel/XXXX</value>
   </webElementProperties>
</WebElementEntity>
