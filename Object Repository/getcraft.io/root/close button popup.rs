<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>close button popup</name>
   <tag></tag>
   <elementGuidId>aa89e8e1-66a5-4fcc-a593-2c3b5680d265</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'mdi mdi-close font-24 index__closeButton__')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mdi mdi-close font-24 index__closeButton__</value>
   </webElementProperties>
</WebElementEntity>
