<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_sosmed twitter - profile</name>
   <tag></tag>
   <elementGuidId>b5562947-4ef2-433d-83db-f57de481ef6d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//img[@alt = 'twitter_icon']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>twitter_icon</value>
   </webElementProperties>
</WebElementEntity>
