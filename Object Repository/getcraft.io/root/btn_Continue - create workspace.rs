<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Continue - create workspace</name>
   <tag></tag>
   <elementGuidId>35426ab4-7f16-4df9-9735-cb27c9ccd9c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button/span[(text() = 'Continue' or . = 'Continue')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Continue</value>
   </webElementProperties>
</WebElementEntity>
