<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_homepage</name>
   <tag></tag>
   <elementGuidId>3d669ab9-7674-4756-ae26-da5ed650f1b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'ImageBanner__Figure') and @src = 'https://assets.getcraft.com/images/banner_def_desktop.jpg']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>old_class</name>
      <type>Main</type>
      <value>index__rightSide__2HpON</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>old_src</name>
      <type>Main</type>
      <value>https://assets.getcraft.com/images/homepage/banner_celebrities_2020.png</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ImageBanner__Figure</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://assets.getcraft.com/images/banner_def_desktop.jpg</value>
   </webElementProperties>
</WebElementEntity>
