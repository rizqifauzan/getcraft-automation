<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ChatNotificationCard__bottomLeft</name>
   <tag></tag>
   <elementGuidId>29504238-93e8-4d91-afa7-80ef23ffa7bf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'ChatNotificationCard__bottomLeft')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ChatNotificationCard__bottomLeft</value>
   </webElementProperties>
</WebElementEntity>
