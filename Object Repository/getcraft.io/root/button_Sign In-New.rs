<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign In-New</name>
   <tag></tag>
   <elementGuidId>5d833812-31cc-4665-863a-9efb6f855de9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;app&quot;]/div/div[3]/header/div/div[4]/div/ul[2]/li[3]/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[(text() = 'Sign in' or . = 'Sign in')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
   </webElementProperties>
</WebElementEntity>
