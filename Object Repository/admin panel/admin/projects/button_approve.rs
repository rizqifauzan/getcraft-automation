<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_approve</name>
   <tag></tag>
   <elementGuidId>37b4af5d-4a4e-42b5-a0b3-0bb4281cb113</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='index__list-item__Gm8Jy index__active__2JTM4']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = 'Approve' or . = 'Approve')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Approve</value>
   </webElementProperties>
</WebElementEntity>
