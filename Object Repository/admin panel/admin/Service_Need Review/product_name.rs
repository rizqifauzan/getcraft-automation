<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>product_name</name>
   <tag></tag>
   <elementGuidId>83b1b346-a0e9-43d5-9a17-279e5a2229e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h4[contains(@class, 'index__productName__')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__productName__</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
   </webElementProperties>
</WebElementEntity>
