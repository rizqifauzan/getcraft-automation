<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>nav_pending invoice new</name>
   <tag></tag>
   <elementGuidId>9b074d6a-b6a2-4718-8635-9438777dd222</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[.='Pending Finance Profiles']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[.='Pending Finance Profiles']</value>
   </webElementProperties>
</WebElementEntity>
