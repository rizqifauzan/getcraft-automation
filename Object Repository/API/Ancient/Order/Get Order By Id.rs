<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Get Order By Id</name>
   <tag></tag>
   <elementGuidId>2441d6a2-c832-4f39-bb2c-d640f99fa7cb</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clientid</name>
      <type>Main</type>
      <value>artemis</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${token}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>user</name>
      <type>Main</type>
      <value>${userId}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${baseUrl}/order/profile/${profileId}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.baseAPI</defaultValue>
      <description></description>
      <id>38eb75e7-66eb-48dd-bc68-0aebc781539c</id>
      <masked>false</masked>
      <name>baseUrl</name>
   </variables>
   <variables>
      <defaultValue>'eyJ1c2VyIjoiXCI1ZGJmZjgzZThmOWY3NDAwMWU0NTEwNjhcIiIsImlhdCI6MTU4MDExOTExOSwiYXVkIjoiZ2V0Y3JhZnQuY29tIiwiaXNzIjoiZ2V0Y3JhZnQuY29tIn0.4WXVaCDYrthXGdG2hNyWV2A0g5PWr9nHuQppWRDKFq4'</defaultValue>
      <description></description>
      <id>a35c6c28-fe70-49ea-a8b9-3b21dc9948ac</id>
      <masked>false</masked>
      <name>token</name>
   </variables>
   <variables>
      <defaultValue>'5dbff83e8f9f74001e451068'</defaultValue>
      <description></description>
      <id>7b8f7d3c-e683-48ef-9c00-8834d40a4d9d</id>
      <masked>false</masked>
      <name>userId</name>
   </variables>
   <variables>
      <defaultValue>'5dbff83ed33f80001ff4f126'</defaultValue>
      <description></description>
      <id>43d2c11a-e22e-49c5-8073-19e80e30d703</id>
      <masked>false</masked>
      <name>profileId</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
