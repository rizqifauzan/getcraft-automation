<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Send Chat</name>
   <tag></tag>
   <elementGuidId>5f559eeb-35a2-4cf9-a3f8-3f719585d346</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\t\&quot;from\&quot;: \&quot;5dbff83ed33f80001ff4f126\&quot;,\n\t\&quot;to\&quot;: \&quot;5dbff1d5d33f80001ff4f123\&quot;,\n\t\&quot;user\&quot;: \&quot;5dbff83e8f9f74001e451068\&quot;,\n\t\&quot;userFirstName\&quot;: \&quot;Rizqi\&quot;,\n\t\&quot;content\&quot;: \&quot;${message}\&quot;,\n\t\&quot;type\&quot;: \&quot;chat\&quot;,\n\t\&quot;timestamp\&quot;: ${timeStamp},\n\t\&quot;order\&quot; : {\n\t\t\&quot;id\&quot; : \&quot;5dbff945c418d7001b3d4c54\&quot;,\n\t\t\&quot;projectName\&quot; : \&quot;coba 1\&quot;\n\t}\n}\n&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clientid</name>
      <type>Main</type>
      <value>artemis</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${token}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>user</name>
      <type>Main</type>
      <value>5dbff83e8f9f74001e451068</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${baseUrl}/loki/chat/${roomId}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>1567163516</defaultValue>
      <description></description>
      <id>ec722409-f91c-4897-a977-4bd7cc8c94da</id>
      <masked>false</masked>
      <name>timeStamp</name>
   </variables>
   <variables>
      <defaultValue>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiXCI1ZGJmZjgzZThmOWY3NDAwMWU0NTEwNjhcIiIsImlhdCI6MTU3MjkyNTk2NCwiYXVkIjoiZ2V0Y3JhZnQuY29tIiwiaXNzIjoiZ2V0Y3JhZnQuY29tIn0.-d__kfSlQtK_zNXY3PulFCh3Bz6xVGj84rH-T1JGRlQ'</defaultValue>
      <description>dapat dari login</description>
      <id>2377a8dd-a835-4ad6-9a0a-81fea4919583</id>
      <masked>false</masked>
      <name>token</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.baseAPI</defaultValue>
      <description></description>
      <id>f886c226-fadd-4cb7-8285-8a2890a9d1e8</id>
      <masked>false</masked>
      <name>baseUrl</name>
   </variables>
   <variables>
      <defaultValue>'Hai From API Katalon'</defaultValue>
      <description></description>
      <id>800649f9-2c5d-4229-940f-7b329942150b</id>
      <masked>false</masked>
      <name>message</name>
   </variables>
   <variables>
      <defaultValue>'5d4a9c331cde74000122ae59'</defaultValue>
      <description></description>
      <id>5d8a3c4e-0a0c-4a93-a65e-0aaa4c4302c8</id>
      <masked>false</masked>
      <name>roomId</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)

WS.verifyElementPropertyValue(response, 'data[0].userFirstName', &quot;Rizqi&quot;)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
