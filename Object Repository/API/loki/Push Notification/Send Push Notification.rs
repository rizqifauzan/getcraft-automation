<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Send Push Notification</name>
   <tag></tag>
   <elementGuidId>463b6afa-ffec-437a-9721-898585ca3888</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\t\&quot;profile\&quot;:[\&quot;5d36d7f58edb04001b8171bc\&quot;],\n\t\&quot;payload\&quot;: \&quot;{\\\&quot;room\\\&quot;:\\\&quot;5dbff946ce468700015668c3\\\&quot;,\\\&quot;from\\\&quot;:\\\&quot;5dbff83ed33f80001ff4f126\\\&quot;,\\\&quot;to\\\&quot;:\\\&quot;5dbff1d5d33f80001ff4f123\\\&quot;,\\\&quot;user\\\&quot;:\\\&quot;5dbff83e8f9f74001e451068\\\&quot;,\\\&quot;userFirstName\\\&quot;:\\\&quot;Rizqi\\\&quot;,\\\&quot;content\\\&quot;:\\\&quot;fauzan\\\&quot;,\\\&quot;type\\\&quot;:\\\&quot;chat\\\&quot;}\&quot;,\n\t\&quot;roomId\&quot;: \&quot;5dbff946ce468700015668c3\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clientid</name>
      <type>Main</type>
      <value>artemis</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clientsecret</name>
      <type>Main</type>
      <value>afz2Sc5FMQWLmFhJGB99wMRN</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>user</name>
      <type>Main</type>
      <value>5dbff83e8f9f74001e451068</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${baseUrl}/loki/send-notification</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.baseAPI</defaultValue>
      <description></description>
      <id>f886c226-fadd-4cb7-8285-8a2890a9d1e8</id>
      <masked>false</masked>
      <name>baseUrl</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)

WS.verifyElementPropertyValue(response, 'data[0].userFirstName', &quot;Rizqi&quot;)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
