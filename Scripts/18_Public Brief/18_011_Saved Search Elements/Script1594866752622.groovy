import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.delay(3)

WebUI.click(findTestObject('getcraft.io/project/public brief/a_entryPublicBrief'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_Public Brief'), 5)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_Create jobs alert'), 5)

WebUI.verifyElementNotClickable(findTestObject('getcraft.io/project/public brief/btn_Create jobs alert'))

WebUI.delay(3)

WebUI.verifyTextPresent('Saved job alerts', false)

WebUI.verifyTextPresent('Nothing here', false)

WebUI.click(findTestObject('getcraft.io/project/public brief/btn_All Categories'))

WebUI.delay(3)

WebUI.click(findTestObject('getcraft.io/project/public brief/click_Videographer - briefList'))

WebUI.delay(3)

WebUI.verifyElementClickable(findTestObject('getcraft.io/project/public brief/btn_Create jobs alert - enable'))

WebUI.click(findTestObject('getcraft.io/project/public brief/btn_Create jobs alert - enable'))

WebUI.focus(findTestObject('getcraft.io/project/public brief/div_Dialog_savedSearch'))

WebUI.verifyTextPresent('Login to GetCraft to create your first Search Alert', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_signUp_savedSearch'), 5)
	
WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_logIn_savedSearch'), 5)

WebUI.closeBrowser()