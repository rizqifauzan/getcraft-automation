import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.openBrowser('')

WebUI.navigateToUrl('https://beta.getcraft.com/briefs')

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_Public Brief'), 8)

WebUI.click(findTestObject('getcraft.io/project/public brief/btn_Public Brief'))

WebUI.delay(3)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), betaUname)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), betaPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.delay(5)

WebUI.click(findTestObject('getcraft.io/project/public brief/btn_Close Brief form'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_Schedule Consul'), 5)

WebUI.getText(findTestObject('getcraft.io/project/public brief/btn_Schedule Consul'))

WebUI.click(findTestObject('getcraft.io/project/public brief/btn_Schedule Consul'))

'Close Documentation window'
WebUI.closeWindowIndex(1)

'Switch to Main window'
WebUI.switchToWindowIndex(0)

WebUI.click(findTestObject('getcraft.io/project/public brief/btn_All Categories'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('getcraft.io/project/public brief/value_photographers'), 5)

WebUI.click(findTestObject('getcraft.io/project/public brief/value_celebrities'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_All Countries'), 5)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_Apply'), 5)

WebUI.getText(findTestObject('getcraft.io/project/public brief/btn_Apply'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/div_Date Created'), 5)

WebUI.getText(findTestObject('getcraft.io/project/public brief/div_Date Created'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/div_Project Detail'), 5)

String rawDetail = WebUI.getText(findTestObject('getcraft.io/project/public brief/div_Project Detail'))

String projectTitle = rawDetail.split('\\n')[0]

String projectCat = rawDetail.split('\\n')[1]

String projectPrice = rawDetail.split('\\n')[2]

System.out.println(projectTitle)

System.out.println(projectCat)

System.out.println(projectPrice)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/div_Creator'), 5)

WebUI.closeBrowser()

