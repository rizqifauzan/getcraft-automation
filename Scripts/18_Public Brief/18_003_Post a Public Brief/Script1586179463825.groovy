import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
//random string or number
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
import org.openqa.selenium.Keys as Keys
//get project dir
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.delay(3)

WebUI.click(findTestObject('getcraft.io/project/public brief/a_entryPublicBrief'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_Public Brief'), 5)

WebUI.click(findTestObject('getcraft.io/project/public brief/btn_Public Brief'))

WebUI.delay(3)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), betaUname)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), betaPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.delay(5)

verifyProjectTitle = (('PB Automation' + ' ') + RandomStringUtils.randomNumeric(4))

WebUI.setText(findTestObject('getcraft.io/project/public brief/txt_projectTitle'), verifyProjectTitle)

WebUI.setText(findTestObject('getcraft.io/project/public brief/txt_projectDetail'), ' Automation is the key of success!')

AssetsDir = (RunConfiguration.getProjectDir() + '/Assets')

String Random = ((Math.random() * 5) as int)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/input_uploadRef'), 5)

WebUI.click(findTestObject('getcraft.io/project/public brief/div_categoryForm'))

WebUI.click(findTestObject('getcraft.io/project/public brief/click_Videographer'))

WebUI.click(findTestObject('getcraft.io/project/public brief/div_serviceTypeForm'))

WebUI.click(findTestObject('getcraft.io/project/public brief/click_Documentary'))

WebUI.click(findTestObject('getcraft.io/project/public brief/div_locationForm'))

WebUI.click(findTestObject('getcraft.io/project/public brief/click_Indonesia'))

WebUI.setText(findTestObject('getcraft.io/project/public brief/input_projectBudget'), '3000000')

WebUI.click(findTestObject('getcraft.io/project/public brief/div_TOP'))

WebUI.click(findTestObject('getcraft.io/project/public brief/click_60Days'))

WebUI.click(findTestObject('getcraft.io/project/public brief/btn_sendForm'))

WebUI.delay(5)

rawData = WebUI.getText(findTestObject('getcraft.io/project/public brief/div_detailInfoBrief'))

projectTitle = (rawData.split('\\n')[0])

WebUI.verifyMatch(verifyProjectTitle, projectTitle, false)

WebUI.closeBrowser()

