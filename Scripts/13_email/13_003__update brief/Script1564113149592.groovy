import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

not_run: WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

not_run: WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

not_run: WebUI.delay(2)

not_run: WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.emailClient_check)

not_run: WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.pt_password)

not_run: WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

not_run: WebUI.click(findTestObject('getcraft.io/root/a_my project'))

not_run: WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

not_run: WebUI.click(findTestObject('getcraft.io/project/order/a_edit'))

not_run: WebUI.click(findTestObject('getcraft.io/project/edit/button_update brief'))

not_run: WebUI.delay(30)

WebUI.navigateToUrl('https://mailnesia.com/')

WebUI.setText(findTestObject('Mailnesia/input_email'), 'creator22')

WebUI.click(findTestObject('Mailnesia/Go'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Mailnesia/a_first subject'), '📢 Check it out! pt22 updated brief for Test E-mail project ')

WebUI.closeBrowser()

