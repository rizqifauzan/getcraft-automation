import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'rizqi.fauzan@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'p@ssw0rd1!')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.delay(2)

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

not_run: WebUI.click(findTestObject('getcraft.io/root/a_my project'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))

WebUI.click(findTestObject('getcraft.io/project/Chat/i_plus'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('getcraft.io/project/Chat/span_upload file'))

AssetsDir = (RunConfiguration.getProjectDir() + '/Assets')

WebUI.uploadFile(findTestObject('getcraft.io/project/Chat/span_upload file'), AssetsDir + '/0.jpg')

WebUI.setText(findTestObject('getcraft.io/project/Chat/input_chat_box_upload'), 'upload a file from katalon')

WebUI.click(findTestObject('getcraft.io/project/Chat/button_send upload'))

WebUI.delay(6)

CustomKeywords.'IsStaging.verifyAttributeOjectFromList'('//div/div[2]/img', -1, 'src', '.jpg')

CustomKeywords.'IsStaging.verifyObjectFromList'('//div/div/div/p', -1, 'upload a file from katalon')

not_run: WebUI.closeBrowser()

