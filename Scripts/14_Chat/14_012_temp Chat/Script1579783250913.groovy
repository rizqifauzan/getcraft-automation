import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'rizqi.fauzan@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'p@ssw0rd1!')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

not_run: WebUI.click(findTestObject('getcraft.io/root/a_my project'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/Project_fauzan_test'))

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

String chatRoom1 = 'Hai Chat Room 1'

String chatRoom2 = 'Hai Chat Room 2'

CustomKeywords.'IsStaging.clickObjectFromList'('//*[contains(@class, \'ChatItem__chatItem__\')]', 1)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_2nd'))

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), chatRoom2)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), chatRoom1)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.clickObjectFromList'('//*[contains(@class, \'ChatItem__chatItem__\')]', 1)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_2nd'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.verifyTextContain'(findTestObject('getcraft.io/project/Chat/span_chat box'), chatRoom2)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))

CustomKeywords.'IsStaging.verifyTextContain'(findTestObject('getcraft.io/project/Chat/span_chat box'), chatRoom1)

WebUI.closeBrowser()

