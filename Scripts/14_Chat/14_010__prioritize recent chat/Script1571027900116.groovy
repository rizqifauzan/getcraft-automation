import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'rizqi.fauzan@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'p@ssw0rd1!')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('getcraft.io/root/a_my project'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/Project_fauzan_test'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/Chat/Chat_1st'), 15)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_2nd'))

WebUI.delay(2)

creatorName = CustomKeywords.'IsStaging.getTextObjectFromList'('//*[contains(@class, \'ChatItem__chatItem__\')]', 1)

CustomKeywords.'IsStaging.clickObjectFromList'('//*[contains(@class, \'ChatItem__chatItem__\')]', 1)

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), chatText)

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))

CustomKeywords.'IsStaging.verifyObjectFromList'('//*[contains(@class, \'ChatItem__chatItem__\')]', 0, creatorName)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_2nd'))

WebUI.delay(2)

creatorName = WebUI.getText(findTestObject('getcraft.io/project/Chat/label_chatRoomName'))

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), chatText)

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))

WebUI.verifyElementText(findTestObject('getcraft.io/project/Chat/verify_chatRoom'), creatorName)

WebUI.closeBrowser()

