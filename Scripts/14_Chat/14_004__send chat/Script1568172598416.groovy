import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/input_email'), 5)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'rizqi.fauzan@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'p@ssw0rd1!')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/Chat/Chat_1st'), 15)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), chatText)

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), Keys.chord(Keys.ENTER))

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), chatText)

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), Keys.chord(Keys.ENTER))

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), chatText)

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

CustomKeywords.'IsStaging.verifyObjectFromList'('//div/div/p[2]/p', -1, chatText)

'Verify chat is not yet read'
WebUI.verifyElementAttributeValue(findTestObject('getcraft.io/project/Chat/text_26th Chat - read or unread icon'), 'src', 
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAICAYAAADN5B7xAAAAAXNSR0IArs4c6QAAAOFJREFUGBljZsAD/v//z8jOzq61d+/e193d3dxOTk7SjLjUgxQ3NjbOAspLcXNzh339+nUbkH0Cqwao4hlABRpAxaHfvn1bBRS7W19fn8o0f/58DpACkE0zZ87kAtFNTU2djIyMmiDFQJNXwhQDxf4xGxoath48eDBs//79W7Zu3boZ6E4JFhaW9UxMTPN//vy5Aqj/PshkkGKQYYwgU58/f74VKHCXmZm5+e/fv3uB4nOB2BVoMopisAYQgaRpCwcHx8rv37/vBwofQjYZpA4FgDTB/NDW1iYMNJ0JRQGUAwC58nEEhkmPcQAAAABJRU5ErkJggg==', 
    0)

WebUI.closeBrowser()

