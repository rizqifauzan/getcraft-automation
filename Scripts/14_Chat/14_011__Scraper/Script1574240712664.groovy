import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('', FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.Staging_URL, FailureHandling.STOP_ON_FAILURE)

WebUI.maximizeWindow(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'rizqi.fauzan@mailnesia.com', FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'p@ssw0rd1!', FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

not_run: WebUI.click(findTestObject('getcraft.io/root/a_my project'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/Project_fauzan_test'))

def url = WebUI.getUrl(FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/Chat/Chat_1st'), 15)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), 'https://getcraft.com', FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('getcraft.io/project/Chat/Scraper/scraper_title'), 'GetCraft - Premium Creative Network', 
    FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('getcraft.io/project/Chat/Scraper/scraper_description'), 'GetCraft is a premium creative network who offer marketers & creatives solutions that help make their work more efficient. We move the world forward by empowering creators to no longer be held back!', 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/Chat/button_send'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.verifyObjectFromList'('//*[contains(@class, \'Chat__metaurlTitle\')]', -1, 'GetCraft - Premium Creative Network')

CustomKeywords.'IsStaging.verifyObjectFromList'('//*[contains(@class, \'Chat__metaurlDescription\')]', -1, 'GetCraft is a premium creative network who offer marketers & creatives solutions that help make their work more efficient. We move the world forward by empowering creators to no longer be held back!')

CustomKeywords.'IsStaging.verifyAttributeOjectFromList'('//div/a/div/div[1]/img', -1, 'src', 'png')

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), 'https://www.youtube.com/watch?v=sC1e3EMaTfQ', 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('getcraft.io/project/Chat/Scraper/scraper_title'), '[Clash-A-Rama] Keseruan di Hari Natal (Clashmas)', 
    FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('getcraft.io/project/Chat/Scraper/scraper_description'), 'Mari kita lihat bagaimana Clash Universe merayakan Natal kemarin~ Subscribe -- https://goo.gl/C9CFcs CLASH-A-RAMA! adalah animasi seri komedi original dari k...', 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/Chat/button_send'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.verifyObjectFromList'('//*[contains(@class, \'Chat__metaurlTitle\')]', -1, '[Clash-A-Rama] Keseruan di Hari Natal (Clashmas)')

CustomKeywords.'IsStaging.verifyObjectFromList'('//*[contains(@class, \'Chat__metaurlDescription\')]', -1, 'Mari kita lihat bagaimana Clash Universe merayakan Natal kemarin~ Subscribe -- https://goo.gl/C9CFcs CLASH-A-RAMA! adalah animasi seri komedi original dari k...')

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), url, FailureHandling.STOP_ON_FAILURE)

projectName = WebUI.getText(findTestObject('getcraft.io/project/Chat/div_project_name'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('getcraft.io/project/Chat/Scraper/scraper_title'), projectName, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/Chat/button_send'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.verifyObjectFromList'('//*[contains(@class, \'Chat__metaurlTitle\')]', -1, projectName)

'Verify chat is not yet read'
not_run: WebUI.verifyElementAttributeValue(findTestObject('getcraft.io/project/Chat/text_26th Chat - read or unread icon'), 
    'src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAICAYAAADN5B7xAAAAAXNSR0IArs4c6QAAAOFJREFUGBljZsAD/v//z8jOzq61d+/e193d3dxOTk7SjLjUgxQ3NjbOAspLcXNzh339+nUbkH0Cqwao4hlABRpAxaHfvn1bBRS7W19fn8o0f/58DpACkE0zZ87kAtFNTU2djIyMmiDFQJNXwhQDxf4xGxoath48eDBs//79W7Zu3boZ6E4JFhaW9UxMTPN//vy5Aqj/PshkkGKQYYwgU58/f74VKHCXmZm5+e/fv3uB4nOB2BVoMopisAYQgaRpCwcHx8rv37/vBwofQjYZpA4FgDTB/NDW1iYMNJ0JRQGUAwC58nEEhkmPcQAAAABJRU5ErkJggg==', 
    0, FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

