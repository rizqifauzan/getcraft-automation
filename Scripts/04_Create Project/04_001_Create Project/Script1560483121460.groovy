import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
//random string or number
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
//get project dir
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(null)

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

//WebUI.setViewPortSize(1680, 850)
WebUI.waitForPageLoad(GlobalVariable.G_Timeout_Small)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.ClientEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.ClientPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

WebUI.click(findTestObject('getcraft.io/marketplace/a_browse creator'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/marketplace/a_Videographers'))

WebUI.setText(findTestObject('getcraft.io/marketplace/input_search'), GlobalVariable.Keyword_Search)

WebUI.sendKeys(findTestObject('getcraft.io/marketplace/input_search'), Keys.chord(Keys.ENTER))

not_run: WebUI.click(findTestObject('getcraft.io/marketplace/button_submit search'))

WebUI.click(findTestObject('getcraft.io/marketplace/a_first service'))

WebUI.click(findTestObject('getcraft.io/marketplace/btn_Invite to quote'))

WebUI.click(findTestObject('getcraft.io/marketplace/btn_close invitation'))

//WebUI.click(findTestObject('getcraft.io/marketplace/btn_req a quote'))
CustomKeywords.'IsStaging.clickElement'(findTestObject('getcraft.io/marketplace/btn_ask quote'), findTestObject('getcraft.io/marketplace/btn_req a quote'))

WebUI.click(findTestObject('getcraft.io/marketplace/span_Complete project details'))

String newProjectName = projectName +' '+ RandomStringUtils.randomNumeric(3)

WebUI.setText(findTestObject('getcraft.io/project/add/input_project name'), newProjectName)

WebUI.setText(findTestObject('getcraft.io/project/add/input_brand name'), brandName)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementNotPresent(findTestObject('getcraft.io/project/add/input_project description - staging'), 5)

//WebUI.setText(findTestObject('getcraft.io/project/add/input_project description - staging'), 'Deskripsi singktanya adalah lorem ipsum dolor amit', 
//    FailureHandling.STOP_ON_FAILURE)
CustomKeywords.'IsStaging.setTextElement'(findTestObject('getcraft.io/project/add/input_project description'), findTestObject(
        'getcraft.io/project/add/input_project description - staging'), 'Deskripsi singktanya adalah Lorem ipsum dolor amit.... et muy bien.')

WebUI.setText(findTestObject('getcraft.io/project/add/input_target audiance'), 'Men 18 - 24 years old', FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.setText(findTestObject('getcraft.io/project/add/input_target audiance'), 'Men 18 - 24 years old')

WebUI.setText(findTestObject('getcraft.io/project/add/input_additional requirement'), 'additional requirement', FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('getcraft.io/project/add/input_additional requirement'), 'additional requirement')

not_run: AssetsDir = (RunConfiguration.getProjectDir() + '/Assets')

not_run: WebUI.uploadFile(findTestObject('getcraft.io/project/add/input_upload reference'), AssetsDir + '/HappyWorking.jpeg')

not_run: WebUI.waitForElementNotClickable(findTestObject('getcraft.io/project/add/input_upload file -- submit'), 3)

not_run: WebUI.click(findTestObject('getcraft.io/project/add/input_upload file -- submit'))

WebUI.click(findTestObject('getcraft.io/project/add/checkbox_project refernece build brand'))

WebUI.click(findTestObject('getcraft.io/project/add/checkbox_project reference generate action'))

WebUI.click(findTestObject('getcraft.io/project/add/checkbox_project refernece grow brand'))

WebUI.setText(findTestObject('getcraft.io/project/add/input_project budget'), 'IDR 1,000,000')

WebUI.click(findTestObject('getcraft.io/project/add/select_term of payment'))

WebUI.click(findTestObject('getcraft.io/project/add/select_term of payment - 100 after'))

WebUI.delay(1)

WebUI.click(findTestObject('getcraft.io/project/add/select_Setlement period'))

WebUI.click(findTestObject('getcraft.io/project/add/select_Setlement period - 30'))

not_run: WebUI.verifyElementText(findTestObject('getcraft.io/project/add/input_project budget'), 'IDR 1,000,000')

not_run: WebUI.verifyElementText(findTestObject('getcraft.io/project/order/verify_brand name'), brandName)

not_run: WebUI.verifyElementText(findTestObject('getcraft.io/project/order/verify_project name'), projectName)

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/project/add/button_Send quote invitation'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/project/order/button_wait and comeback'), 0)

WebUI.click(findTestObject('getcraft.io/project/order/button_wait and comeback'))

not_run: WebUI.verifyElementText(findTestObject('getcraft.io/project/order/verify_project base'), '100% Settlement, 60 Calendar Days After Project Completed')

not_run: WebUI.verifyElementText(findTestObject('getcraft.io/project/order/verify_objective'), 'Build brand and product awareness; Grow your brand and product reputation; Generate action, clicks and sales\n', 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyElementText(findTestObject('getcraft.io/project/order/verify_target audiance'), targetAudiance, FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyElementText(findTestObject('getcraft.io/project/order/verify_description'), description, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.project_name = newProjectName

WebUI.closeBrowser()

