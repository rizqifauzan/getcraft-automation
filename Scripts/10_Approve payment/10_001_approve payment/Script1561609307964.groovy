import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String status = GlobalVariable.TC_Status

if ((status == 'FAILED') || (status == 'ERROR')) {
    KeywordUtil.markErrorAndStop('---->|')
} else {
    WebUI.openBrowser('')

    WebUI.navigateToUrl(GlobalVariable.Admin_Staging_URL)

    WebUI.maximizeWindow()

    WebUI.click(findTestObject('admin.staging.getcraft.io/Login/button_login google'))

    WebUI.switchToWindowTitle('Masuk - Akun Google')

    WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_email'), 'QAtest@getcraft.com')

    WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))

    WebUI.delay(2)

    WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_password'), 'crafty1234')

    WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))

    WebUI.delay(2)

    WebUI.switchToWindowTitle('Getcraft', FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('admin.staging.getcraft.io/admin/nav_project'))

    WebUI.click(findTestObject('admin.staging.getcraft.io/admin/projects/button_view newest project'))

    WebUI.click(findTestObject('admin.staging.getcraft.io/admin/projects/tab_qoutation'))

    WebUI.click(findTestObject('admin.staging.getcraft.io/admin/projects/button_approve'))

    WebUI.verifyElementText(findTestObject('admin.staging.getcraft.io/admin/projects/verify_invoice status'), 'Invoice Confirmed')
}

