import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'jingpin@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

WebUI.click(findTestObject('getcraft.io/project/order/tab_qoute'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/order/button_add qoute'), 7)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/order/button_add qoute'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/pitch/input_describe your idea'), 10)

WebUI.click(findTestObject('getcraft.io/project/Quote/Btn_SendtoClient'))

WebUI.verifyTextPresent('Please fill in your creative idea before submitting a pitch', false)

WebUI.setText(findTestObject('getcraft.io/project/pitch/input_describe your idea'), 'Test Quote')

WebUI.click(findTestObject('getcraft.io/project/Quote/Btn_SendtoClient'))

WebUI.delay(2)

WebUI.verifyTextPresent('Please fill in your pricing', false)

'Click Pricing'
WebUI.click(findTestObject('getcraft.io/project/Quote/Btn_Pricing'))

WebUI.click(findTestObject('getcraft.io/project/Quote/Btn_SendtoClient'))

WebUI.verifyTextPresent('Please fill in your pricing', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/Input_Desc1'), 3)

'filled Section'
WebUI.setText(findTestObject('getcraft.io/project/Quote/Input_Desc1'), 'Foto')

not_run: WebUI.setText(findTestObject('getcraft.io/project/Quote/Txt_Qty'), '1')

WebUI.click(findTestObject('getcraft.io/project/Quote/Div_Unit'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/Div_Unit2'), 3)

WebUI.click(findTestObject('getcraft.io/project/Quote/Div_Unit2'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/Input_Price'), 3)

WebUI.setText(findTestObject('getcraft.io/project/Quote/Input_Price'), '1000000')

not_run: WebUI.click(findTestObject('getcraft.io/project/Quote/Btn_SubmitQuote'))

WebUI.verifyTextPresent('Please fill in your pricing', false)

'click Textbox Section\n'
not_run: WebUI.setText(findTestObject('getcraft.io/project/Quote/Txt_Section'), 'January')

WebUI.click(findTestObject('getcraft.io/project/Quote/Btn_Item'))

'filled Section'
WebUI.mouseOver(findTestObject('getcraft.io/project/Quote/Input_Desc1'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/delete_line'), 0)

WebUI.click(findTestObject('getcraft.io/project/Quote/delete_line'))

WebUI.verifyTextNotPresent('Piece', false)

WebUI.click(findTestObject('getcraft.io/project/Quote/remove_section'))

WebUI.click(findTestObject('getcraft.io/project/Quote/yes,sure'))

'Click Pricing'
WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/Btn_Pricing'), 4)

WebUI.closeBrowser()

