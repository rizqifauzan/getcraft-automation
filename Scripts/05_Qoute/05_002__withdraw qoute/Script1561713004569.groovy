import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.CreatorEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.ClientPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 30)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

not_run: WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/order/tab_qoute'), 10)

WebUI.click(findTestObject('getcraft.io/project/order/tab_brief'))

WebUI.click(findTestObject('getcraft.io/project/order/button_withdraw'))

WebUI.click(findTestObject('getcraft.io/project/order/button_withdraw popup'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/a_briefing newest'), 10)

WebUI.closeBrowser()

