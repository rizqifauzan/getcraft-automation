import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
// import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.CreatorEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.CeatorPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/top-menu/a_Projects'))

WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

WebUI.delay(2)

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/order/tab_qoute'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/order/button_add qoute'), 7)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/order/button_add qoute'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/pitch/input_describe your idea'), 10)

WebUI.setText(findTestObject('getcraft.io/project/pitch/input_describe your idea'), 'Test Quote')

'Click Pricing'
WebUI.click(findTestObject('getcraft.io/project/Quote/Btn_Pricing'))

'click Textbox Section\n'
WebUI.setText(findTestObject('getcraft.io/project/Quote/Txt_Section'), 'January')

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/Input_Desc1'), 3)

'filled Section'
WebUI.setText(findTestObject('getcraft.io/project/Quote/Input_Desc1'), 'Foto')

WebUI.setText(findTestObject('getcraft.io/project/Quote/Txt_Qty'), '1')

WebUI.click(findTestObject('getcraft.io/project/Quote/Div_Unit'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/Div_Unit2'), 3)

WebUI.click(findTestObject('getcraft.io/project/Quote/Div_Unit2'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/Input_Price'), 5)

WebUI.setText(findTestObject('getcraft.io/project/Quote/Input_Price'), '1000000')

WebUI.click(findTestObject('getcraft.io/project/Quote/Btn_Item'))

borderblack = WebUI.getCSSValue(findTestObject('getcraft.io/project/Quote/Click_Desc'), 'border')

'Verify Brief Case Background color is Black'
WebUI.verifyMatch(borderblack, '1px solid rgb(170, 170, 170)', false)

WebUI.scrollToPosition(0, 1000)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/Quote/Btn_SendtoClient'))

WebUI.click(findTestObject('getcraft.io/project/Quote/Click_Desc'))

borderorange = WebUI.getCSSValue(findTestObject('getcraft.io/project/Quote/Click_Desc'), 'border')

'Verify Brief Case Background color is Orange'
WebUI.verifyMatch(borderorange, '1px solid rgb(69, 173, 168)', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/Input_Desc2'), 3)

'filled Section'
WebUI.setText(findTestObject('getcraft.io/project/Quote/Input_Desc2'), 'Video')

WebUI.setText(findTestObject('getcraft.io/project/Quote/Txt_Qty1'), '3')

WebUI.click(findTestObject('getcraft.io/project/Quote/Div_Unit3'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/Div_Unit4'), 3)

WebUI.click(findTestObject('getcraft.io/project/Quote/Div_Unit4'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/Txt_Price 2'), 2)

WebUI.setText(findTestObject('getcraft.io/project/Quote/Txt_Price 2'), '10000')

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Quote/Btn_SendtoClient'), 2)

WebUI.click(findTestObject('getcraft.io/project/Quote/Btn_SendtoClient'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/order/tab_qoute'), 10)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

