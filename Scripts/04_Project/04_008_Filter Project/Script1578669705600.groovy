import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.ClientEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.ClientPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/account-setting/allow_notif'), 15)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/select_project status'), 30)

not_run: WebUI.click(findTestObject('getcraft.io/project/select_project status'), FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByLabel(findTestObject('getcraft.io/project/select_project status'), '  Invitation Sent', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Label_InvitationSent'), 5)

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/project/Label_ProjectRunning'), 5)

WebUI.selectOptionByLabel(findTestObject('getcraft.io/project/select_project status'), '  Quote created', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Label_QuoteCreated'), 5)

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/project/Label_InvoiceReceived'), 5)

WebUI.selectOptionByLabel(findTestObject('getcraft.io/project/select_project status'), '  Quote approved', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Label_PONeeded'), 5)

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/project/Label_QuoteCreated'), 5)

WebUI.selectOptionByLabel(findTestObject('getcraft.io/project/select_project status'), '  PO/Invoice created', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Label_InvoiceReceived'), 5)

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/project/Label_PONeeded'), 5)

WebUI.selectOptionByLabel(findTestObject('getcraft.io/project/select_project status'), '  Project running', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Label_ProjectRunning'), 5)

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/project/Label_Cancelled'), 5)

WebUI.selectOptionByLabel(findTestObject('getcraft.io/project/select_project status'), '  Cancelled/Withdrawn', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Label_Cancelled'), 5)

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/project/Label_ProjectRunning'), 5)

WebUI.selectOptionByLabel(findTestObject('getcraft.io/project/select_project status'), 'Completed', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Label_Completed'), 5)

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/project/Label_ProjectRunning'), 5)

WebUI.closeBrowser()

