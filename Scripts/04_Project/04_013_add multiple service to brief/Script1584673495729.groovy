import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
//random string or number
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 15)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'))

WebUI.mouseOver(findTestObject('getcraft.io/product/card title'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/add to cart'))

not_run: WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/preview'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_create brief'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'topi@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/microbrief/btn_send'), 10)

if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'))
} else {
    WebUI.delay(2)
}

//generate random integer
int Random

Random = ((Math.random() * 999) as int)

String New_Name_Value = projectName + Random

WebUI.setText(findTestObject('getcraft.io/project/microbrief/input_project title'), New_Name_Value)

WebUI.setText(findTestObject('getcraft.io/project/microbrief/div_project desc'), description)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/microbrief/div_char limit'), 3)

WebUI.getText(findTestObject('getcraft.io/project/microbrief/div_char limit'))

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/microbrief/div_TOP'))

WebUI.click(findTestObject('getcraft.io/project/microbrief/h3_30 Days Settlement'))

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/microbrief/btn_send'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/order/button_wait and comeback'), 10)

WebUI.click(findTestObject('getcraft.io/project/order/button_wait and comeback'))

not_run: WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 5)

not_run: WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('getcraft.io/project/add_creator'))

WebUI.setText(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), 'jingpin')

WebUI.sendKeys(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), Keys.chord(Keys.ENTER))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 25)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'))

WebUI.click(findTestObject('getcraft.io/project/mini_add new creator'))

WebUI.click(findTestObject('getcraft.io/marketplace/add_this_Creator'))

WebUI.click(findTestObject('getcraft.io/marketplace/btn_Add to project'))

WebUI.click(findTestObject('getcraft.io/marketplace/Bt_view project'))

not_run: WebUI.click(findTestObject('getcraft.io/project/search mini'))

not_run: WebUI.setText(findTestObject('getcraft.io/project/Input_search mini'), 'jingpin')

not_run: WebUI.waitForElementClickable(findTestObject('getcraft.io/marketplace/NewSearch/Tick ServiceCard'), 10)

not_run: WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Tick ServiceCard'))

not_run: WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Tick ServiceCard'))

not_run: WebUI.click(findTestObject('getcraft.io/project/mini_add new creator'))

not_run: WebUI.waitForElementPresent(findTestObject('getcraft.io/project/mini_keep deadline'), 10)

not_run: WebUI.takeScreenshot()

not_run: WebUI.refresh()

not_run: WebUI.click(findTestObject('getcraft.io/project/mini_keep deadline'))

not_run: WebUI.takeScreenshot()

not_run: WebUI.click(findTestObject('getcraft.io/project/mini_close'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/Chat/Chat_1st'), 15)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Jingpin Uniq', false)

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/a_sign out'))

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/button_Sign In'), 5)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'jingpin@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/a_newest project'), 20)

WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

WebUI.click(findTestObject('getcraft.io/project/goto_brief'))

WebUI.verifyTextPresent(New_Name_Value, false)

WebUI.closeBrowser()

