import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Request a quote'))

WebUI.click(findTestObject('getcraft.io/project-add/button_research'))

WebUI.click(findTestObject('getcraft.io/project-add/input_Sub service'))

WebUI.click(findTestObject('getcraft.io/project-add/checkbox_quantitative'))

WebUI.click(findTestObject('getcraft.io/project-add/span_select'))

WebUI.click(findTestObject('getcraft.io/project-add/input_country'))

WebUI.click(findTestObject('getcraft.io/project-add/checkbox_Philippines'))

WebUI.click(findTestObject('getcraft.io/project-add/span_select'))

WebUI.click(findTestObject('getcraft.io/project-add/input_industry topic'))

WebUI.click(findTestObject('getcraft.io/project-add/checkbox_Art'))

WebUI.click(findTestObject('getcraft.io/project-add/span_select'))

WebUI.click(findTestObject('getcraft.io/project-add/button_send'))

WebUI.verifyElementText(findTestObject('getcraft.io/project-add/verify_project title required'), 'required')

WebUI.setText(findTestObject('getcraft.io/project-add/input_project title'), 'Coca Cola No Sugar')

WebUI.click(findTestObject('getcraft.io/project-add/button_send'))

WebUI.verifyElementText(findTestObject('getcraft.io/project-add/verify_project brand name required'), 'required')

WebUI.setText(findTestObject('getcraft.io/project-add/input_brand name'), 'Coca Cola')

WebUI.setText(findTestObject('getcraft.io/project-add/input_project description'), 'New Coca Cola, No Sugar')

WebUI.setText(findTestObject('getcraft.io/project-add/input_target audiance'), 'Man 10 - 30 Years Old')

WebUI.click(findTestObject('getcraft.io/project-add/button_send'))

WebUI.verifyElementText(findTestObject('getcraft.io/project-add/verify_project objective required'), 'Missing requirement! Please ensure you check one of the objectives below:')

WebUI.click(findTestObject('getcraft.io/project-add/checkbox_project objective - generate action'))

WebUI.click(findTestObject('getcraft.io/project-add/button_send'))

WebUI.verifyElementText(findTestObject('getcraft.io/project-add/verify_project budget required'), 'required')

WebUI.setText(findTestObject('getcraft.io/project-add/input_project budget'), 'IDR 1,000,000')

WebUI.click(findTestObject('getcraft.io/project-add/button_send'))

WebUI.verifyElementText(findTestObject('getcraft.io/project-add/verify_terms of payment required'), 'Terms of Payment required')

WebUI.click(findTestObject('getcraft.io/project-add/select_term of payment'))

WebUI.click(findTestObject('getcraft.io/project-add/select_term of payment_100 after'))

WebUI.click(findTestObject('getcraft.io/project-add/button_send'))

WebUI.verifyElementText(findTestObject('getcraft.io/project-add/verify_Settlement Day required'), 'Settlement Day required')

WebUI.click(findTestObject('getcraft.io/project-add/select_setlememt period'))

WebUI.click(findTestObject('getcraft.io/project-add/select_setlement period_30 day'))

WebUI.click(findTestObject('getcraft.io/project-add/button_send'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/sign-in/input_email'), 60)

WebUI.closeBrowser()

