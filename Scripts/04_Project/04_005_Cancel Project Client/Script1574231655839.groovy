import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
//random string or number
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

width = WebUI.getPageWidth()

height = WebUI.getPageHeight()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'topi1@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

not_run: if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))
} else {
    WebUI.delay(2)
}

not_run: WebUI.click(findTestObject('getcraft.io/root/a_my project'))

not_run: WebUI.delay(2)

//if (WebUI.verifyElementVisible(findTestObject('getcraft.io/project/a_newest project')) == true) {
//    WebUI.click(findTestObject('getcraft.io/project/a_newest project'))
//}else {
//CustomKeywords.'IsStaging.clickElement'(findTestObject('getcraft.io/root/a_browse creator'), findTestObject('getcraft.io/root/a_browse vendor'))
//WebUI.click(findTestObject('getcraft.io/project/button_invite creator'))
//}
WebUI.click(findTestObject('getcraft.io/root/a_browse vendor'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 15)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), 'bersatu')

WebUI.sendKeys(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), Keys.chord(Keys.ENTER))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 25)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'))

WebUI.mouseOver(findTestObject('getcraft.io/product/card title'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/add to cart'))

not_run: WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/preview'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_create brief'))

//generate random integer
int Random

Random = ((Math.random() * 999) as int)

String New_Name_Value = projectName + Random

WebUI.setText(findTestObject('getcraft.io/project/microbrief/input_project title'), New_Name_Value)

WebUI.setText(findTestObject('getcraft.io/project/microbrief/div_project desc'), description)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/microbrief/div_char limit'), 3)

WebUI.getText(findTestObject('getcraft.io/project/microbrief/div_char limit'))

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/microbrief/div_TOP'))

WebUI.click(findTestObject('getcraft.io/project/microbrief/h3_30 Days Settlement'))

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/microbrief/btn_send'))

WebUI.delay(5)

not_run: WebUI.setText(findTestObject('getcraft.io/project/add/input_brand name'), brandName)

not_run: WebUI.setText(findTestObject('getcraft.io/project/add/input_project description2'), description)

not_run: WebUI.setText(findTestObject('getcraft.io/project/add/input_target audiance'), 'Men 18 - 24 years old')

not_run: WebUI.setText(findTestObject('getcraft.io/project/add/input_additional requirement'), 'additional requirement')

not_run: WebUI.click(findTestObject('getcraft.io/project/add/checkbox_project refernece build brand'))

not_run: WebUI.click(findTestObject('getcraft.io/project/add/checkbox_project reference generate action'))

not_run: WebUI.click(findTestObject('getcraft.io/project/add/checkbox_project refernece grow brand'))

not_run: WebUI.setText(findTestObject('getcraft.io/project/add/input_project budget'), 'IDR 1,000,000')

not_run: WebUI.click(findTestObject('getcraft.io/project/add/select_term of payment'))

not_run: WebUI.click(findTestObject('getcraft.io/project/add/select_term of payment - 100 after'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('getcraft.io/project/add/select_Setlement period'))

not_run: WebUI.click(findTestObject('getcraft.io/project/add/select_Setlement period - 90'))

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('getcraft.io/project/add/button_Send quote invitation'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/order/button_wait and comeback'), 10)

WebUI.click(findTestObject('getcraft.io/project/order/button_wait and comeback'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/goto_brief'), 15)

WebUI.click(findTestObject('getcraft.io/project/goto_brief'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/cancel_project'), 10)

WebUI.click(findTestObject('getcraft.io/project/cancel_project'))

WebUI.click(findTestObject('getcraft.io/project/cancel_project_popup'))

WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/archive_section'), 60)

WebUI.scrollToElement(findTestObject('getcraft.io/project/archive_section'), 3)

WebUI.click(findTestObject('getcraft.io/project/cancelled_label'))

WebUI.click(findTestObject('getcraft.io/project/goto_brief'))

WebUI.verifyTextPresent('Cancelled', false)

WebUI.verifyElementText(findTestObject('getcraft.io/project/verify_projectName'), New_Name_Value)

WebUI.verifyTextPresent('The project has been archived', false)

GlobalVariable.project_name = New_Name_Value

WebUI.closeBrowser()

