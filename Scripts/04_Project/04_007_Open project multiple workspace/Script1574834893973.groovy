import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'testing2@mailinator.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/a_newest project'), 30)

WebUI.click(findTestObject('getcraft.io/project/a_newest project'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/goto_brief'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/verify_projectName'), 10)

WebUI.click(findTestObject('GETCRAFT/login/present_myproject-WISP1'))

text = WebUI.getText(findTestObject('getcraft.io/project/newest_projectName'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('GETCRAFT/general/select_workspace'))

WebUI.delay(5)

text2 = WebUI.getText(findTestObject('getcraft.io/project/newest_projectName'))

WebUI.verifyNotEqual(text, text2)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/a_newest project'), 30)

WebUI.click(findTestObject('getcraft.io/project/a_newest project'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/goto_brief'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/verify_projectName'), 10)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

