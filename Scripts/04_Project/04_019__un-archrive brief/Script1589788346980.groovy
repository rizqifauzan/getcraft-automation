import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'topi1@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 20)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/archive_section'), 60)

WebUI.scrollToElement(findTestObject('getcraft.io/project/archive_section'), 3)

WebUI.click(findTestObject('getcraft.io/project/cancelled_label'))

WebUI.click(findTestObject('getcraft.io/project/goto_brief'))

WebUI.verifyElementText(findTestObject('getcraft.io/project/verify_projectName'), GlobalVariable.project_name)

WebUI.waitForElementClickable(findTestObject('getcraft.io/project/btn_unarchive project'), 20)

WebUI.click(findTestObject('getcraft.io/project/btn_unarchive project'))

WebUI.click(findTestObject('getcraft.io/project/btn_Unarchive project popup'))

WebUI.delay(3)

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/goto_brief'), 5)

WebUI.click(findTestObject('getcraft.io/project/goto_brief'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('getcraft.io/project/verify_projectName'), GlobalVariable.project_name)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/cancel_project'), 3)

WebUI.closeBrowser()

