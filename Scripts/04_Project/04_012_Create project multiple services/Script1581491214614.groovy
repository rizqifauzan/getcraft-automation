import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
//random string or number
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

width = WebUI.getPageWidth()

height = WebUI.getPageHeight()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'topi@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

not_run: if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))
} else {
    WebUI.delay(2)
}

not_run: WebUI.click(findTestObject('getcraft.io/root/a_my project'))

not_run: WebUI.delay(2)

//if (WebUI.verifyElementVisible(findTestObject('getcraft.io/project/a_newest project')) == true) {
//    WebUI.click(findTestObject('getcraft.io/project/a_newest project'))
//}else {
//CustomKeywords.'IsStaging.clickElement'(findTestObject('getcraft.io/root/a_browse creator'), findTestObject('getcraft.io/root/a_browse vendor'))
//WebUI.click(findTestObject('getcraft.io/project/button_invite creator'))
//}
WebUI.click(findTestObject('getcraft.io/root/a_browse vendor'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 15)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), 'kulo')

WebUI.sendKeys(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), Keys.chord(Keys.ENTER))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 25)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'))

WebUI.mouseOver(findTestObject('getcraft.io/product/card title'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/add to cart'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/btn_close modal createbrief'))

WebUI.mouseOver(findTestObject('getcraft.io/product/card title_Kulo'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/add to cart'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/bt_creator in cart'))

not_run: WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/preview'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_create brief'))

//generate random integer
int Random

Random = ((Math.random() * 999) as int)

String New_Name_Value = projectName + Random

WebUI.setText(findTestObject('getcraft.io/project/microbrief/input_project title'), New_Name_Value)

WebUI.setText(findTestObject('getcraft.io/project/microbrief/div_project desc'), description)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/microbrief/div_char limit'), 3)

WebUI.getText(findTestObject('getcraft.io/project/microbrief/div_char limit'))

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/microbrief/div_TOP'))

WebUI.click(findTestObject('getcraft.io/project/microbrief/h3_30 Days Settlement'))

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/microbrief/btn_send'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/order/button_wait and comeback'), 20)

WebUI.click(findTestObject('getcraft.io/project/order/button_wait and comeback'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/root/a_sign out'))

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/button_Sign In'), 15)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'jingpin@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/a_newest project'), 20)

WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

WebUI.click(findTestObject('getcraft.io/project/goto_brief'))

WebUI.verifyElementText(findTestObject('getcraft.io/project/verify_projectName'), New_Name_Value)

WebUI.closeBrowser()

