import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
//random string or number
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
//get project dir
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.ClientEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.ClientPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

not_run: GlobalVariable.project_name = verifyprojectName

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 20)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/a_browse creator'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 15)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), 'creator03')

WebUI.sendKeys(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), Keys.chord(Keys.ENTER))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/div_first Creator'), 15)

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/marketplace/div_first Creator'))

not_run: WebUI.mouseOver(findTestObject('getcraft.io/product/card title'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/add to cart'))

//WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/preview'))
WebUI.click(findTestObject('getcraft.io/marketplace/btn_Send message'))

WebUI.delay(5)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/microbrief/div_toolbar'), 3)

verifyprojectName = ((projectName + ' ') + RandomStringUtils.randomNumeric(3))

WebUI.setText(findTestObject('getcraft.io/project/microbrief/input_project title'), verifyprojectName)

GlobalVariable.project_name = verifyprojectName

WebUI.setText(findTestObject('getcraft.io/project/microbrief/div_project desc'), description)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/microbrief/div_char limit'), 3)

WebUI.getText(findTestObject('getcraft.io/project/microbrief/div_char limit'))

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('getcraft.io/project/microbrief/div_TOP'))

not_run: WebUI.click(findTestObject('getcraft.io/project/microbrief/h3_30 Days Settlement'))

not_run: WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/microbrief/btn_send'))

WebUI.delay(5)

not_run: WebUI.waitForElementPresent(findTestObject('getcraft.io/project/order/button_wait and comeback'), 10)

not_run: WebUI.click(findTestObject('getcraft.io/project/order/button_wait and comeback'))

not_run: WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))

not_run: CustomKeywords.'IsStaging.verifyObjectFromList'('//*[@id = \'cardTitle\']', -1, 'New Brief Created')

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('getcraft.io/top-menu/bel notif'), FailureHandling.STOP_ON_FAILURE)

not_run: def text = WebUI.getText(findTestObject('getcraft.io/top-menu/bel notif value'))

not_run: if (text.contains(verifyprojectName)) {
    WebUI.closeBrowser()
} else {
    KeywordUtil.markFailed('fail')
}

