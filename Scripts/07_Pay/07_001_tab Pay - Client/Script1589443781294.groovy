import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String status = GlobalVariable.TC_Status

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.ClientEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.ClientPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/top-menu/a_Projects'))

WebUI.selectOptionByLabel(findTestObject('getcraft.io/project/select_project status'), '  Quote created', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Label_QuoteCreated'), 5)

WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

//WebUI.waitForElementPresent(findTestObject('getcraft.io/project/Chat/Chat_1st'), 15)
//
//WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))
//
//CustomKeywords.'IsStaging.verifyObjectFromList'('//*[@id = \'cardTitle\']', -1, 'Quote is Selected!')
WebUI.delay(5)

WebUI.verifyTextPresent(GlobalVariable.project_name, false)

WebUI.click(findTestObject('getcraft.io/project/order/tab_payment'))

not_run: statusProject = WebUI.getText(findTestObject('getcraft.io/project/order/verify_project status'))

not_run: if (statusProject == 'Quote created') {
    //Selebumnya "Quote Created"
    KeywordUtil.markPassed(statusProject)
} else {
    KeywordUtil.markFailed(statusProject)
}

not_run: WebUI.click(findTestObject('getcraft.io/project/payment/box_quotation approval'))

not_run: WebUI.click(findTestObject('getcraft.io/project/payment/bt_Approve this quote'))

not_run: WebUI.click(findTestObject('getcraft.io/project/payment/button_approve qoutation'))

not_run: WebUI.click(findTestObject('getcraft.io/project/payment/button_approve qoutation pop up'))

not_run: WebUI.delay(5)

not_run: poNeeded = WebUI.getText(findTestObject('getcraft.io/project/order/verify_project status'))

//not_run: if (poNeeded == 'PO needed') {
//    not_run: KeywordUtil.markPassed(poNeeded)
//} else {
//  not_run:  KeywordUtil.markFailed(poNeeded)
//}

WebUI.click(findTestObject('getcraft.io/project/payment/box_complete purchase order'))

WebUI.click(findTestObject('getcraft.io/project/payment/button_we dont issue PO'))

WebUI.click(findTestObject('getcraft.io/project/payment/button_PO - yes'))

//invoiceReceived = WebUI.getText(findTestObject('getcraft.io/project/order/verify_project status'))
//
//not_run: if (invoiceReceived == 'Invoice received') {
//    KeywordUtil.markPassed(invoiceReceived)
//} else {
//    KeywordUtil.markFailed(invoiceReceived)
//}
//
WebUI.delay(5)

//String projectRunning = WebUI.getText(findTestObject('getcraft.io/project/order/verify_status running'))
//
//KeywordUtil.logInfo(projectRunning)
//
//if (projectRunning == 'Project running') {
//    KeywordUtil.markPassed(projectRunning)
//} else {
//    KeywordUtil.markFailed(projectRunning)
//}
//
//GlobalVariable.ProjectRun = projectRunning
//WebUI.delay(3)
//
//WebUI.closeBrowser()
WebUI.click(findTestObject('getcraft.io/project/payment/btn_Pay now'), 2)

WebUI.click(findTestObject('getcraft.io/project/payment/btn_VA BCA'), 2)

WebUI.click(findTestObject('getcraft.io/project/payment/btn_Yes ConfirmVA'), 2)

