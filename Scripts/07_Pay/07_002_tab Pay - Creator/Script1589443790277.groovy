import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String status = GlobalVariable.TC_Status

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.CreatorEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.CreatorPassw)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/top-menu/a_Projects'))

WebUI.scrollToElement(findTestObject('getcraft.io/project/confirmed_section'), 5)

WebUI.click(findTestObject('getcraft.io/project/projectRunning_label'))

WebUI.delay(5)

not_run: projectName = WebUI.getText(findTestObject('getcraft.io/project/verify_projectName'))

not_run: WebUI.verifyEqual(projectName, GlobalVariable.project_name, FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/project/order/tab_payment'))

WebUI.verifyTextPresent(GlobalVariable.project_name, false)

//WebUI.verifyTextPresent('Project running', false)
WebUI.getText(findTestObject('getcraft.io/project/order/verify_status running'))

not_run: WebUI.delay(3)

WebUI.closeBrowser()

