import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

'Input wrong format email\n'
WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'rahmat')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.creator_password)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/New Homepage/Alert_UsetNotFound'), 5)

WebUI.refresh()

'Input user that nor registered yet'
WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'rahmat@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.creator_password)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/New Homepage/Alert_UsetNotFound'), 5)

WebUI.refresh()

'Input wit empty email'
WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), '')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.creator_password)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/New Homepage/Alert_UsetNotFound'), 5)

WebUI.refresh()

'Input wit Password email'
WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.creator_email)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), '')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/sign-in/alert_required'), 5)

WebUI.refresh()

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'testing3@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.closeBrowser()

