import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String status = GlobalVariable.TC_Status

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.ClientEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.ClientPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByLabel(findTestObject('getcraft.io/project/select_project status'), 'Completed', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Label_Completed'), 5)

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/project/Label_ProjectRunning'), 5)

WebUI.scrollToElement(findTestObject('getcraft.io/project/Label_Completed'), 5)

WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

WebUI.delay(5)

WebUI.verifyTextPresent(GlobalVariable.project_name, false)

WebUI.click(findTestObject('getcraft.io/project/order/tab_payment'))

statusProject = WebUI.getText(findTestObject('getcraft.io/project/order/verify_project status'))

WebUI.verifyMatch(statusProject, 'Completed', false)

WebUI.click(findTestObject('getcraft.io/project/payment/box_complete payment'))

WebUI.verifyTextPresent('Invoice settled', false)

WebUI.closeBrowser()

