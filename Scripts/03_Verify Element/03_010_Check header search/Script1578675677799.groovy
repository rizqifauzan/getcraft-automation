import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/button_Sign In'), 10)

WebUI.verifyTextPresent('GetCraft is a premium creative network. It’s the best place to connect with creative professionals and businesses looking for projects or work.', 
    false)

not_run: WebUI.scrollToElement(findTestObject('getcraft.io/root/middle element_homapage'), 3)

not_run: WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/search_header'), 5)

WebUI.closeBrowser()

