import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.email_change_passw)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.passw_draft)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/account-setting/allow_notif'), 30)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

not_run: WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

not_run: WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('getcraft.io/root/a_Account Setting'))

WebUI.click(findTestObject('getcraft.io/account-setting/change_password'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/account-setting/current_password'), 5)

WebUI.setText(findTestObject('getcraft.io/account-setting/current_password'), GlobalVariable.passw_draft)

WebUI.setText(findTestObject('getcraft.io/account-setting/new_password'), GlobalVariable.passw_draft)

WebUI.click(findTestObject('getcraft.io/account-setting/save_changes'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/account-setting/label_passw_changes'), 0)

WebUI.closeBrowser()

