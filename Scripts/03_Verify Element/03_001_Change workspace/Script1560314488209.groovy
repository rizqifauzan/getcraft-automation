import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

not_run: if (WebUI.verifyElementVisible(findTestObject('Object Repository/GETCRAFT/create_brief/Language_indo'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('GETCRAFT/login/Select_language'))

    WebUI.click(findTestObject('GETCRAFT/login/english_language'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(3)
} else {
    WebUI.delay(3)
}

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'bali8@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 30)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

WebUI.delay(2)

text = WebUI.getText(findTestObject('GETCRAFT/general/workspace_name'))

not_run: WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('GETCRAFT/general/select_workspace'))

WebUI.delay(5)

text2 = WebUI.getText(findTestObject('GETCRAFT/general/workspace_name'))

WebUI.verifyNotEqual(text, text2)

WebUI.delay(3)

not_run: WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('GETCRAFT/general/select_workspace'))

WebUI.delay(5)

text3 = WebUI.getText(findTestObject('GETCRAFT/general/workspace_name'))

WebUI.verifyEqual(text, text3)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

