import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String status = GlobalVariable.TC_Status

if ((status == 'FAILED') || (status == 'ERROR')) {
    KeywordUtil.markErrorAndStop('---->|' //, FailureHandling.CONTINUE_ON_FAILURE)
        //, FailureHandling.CONTINUE_ON_FAILURE)
        //CONTINUE_ON_FAILURE
        //CONTINUE_ON_FAILURE
        // WebUI.click(findTestObject('getcraft.io/root/a_my project'))
        )
} else {
    WebUI.openBrowser('')

    WebUI.navigateToUrl(GlobalVariable.Staging_URL)

    WebUI.maximizeWindow()

    WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

    WebUI.delay(2)

    WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.CreatorEmail)

    WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.CeatorPasswd)

    WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

    WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(2)

    verifyProject = WebUI.getText(findTestObject('getcraft.io/project/a_newest project'))

    if (verifyProject == GlobalVariable.project_name) {
        WebUI.delay(3)
    }
    
    WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

    KeywordUtil.logInfo(verifyProject)

    KeywordUtil.logInfo(GlobalVariable.project_name)

    WebUI.click(findTestObject('getcraft.io/project/order/tab_qoute'))

    WebUI.waitForElementPresent(findTestObject('getcraft.io/project/order/button_add qoute'), 7)

    WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

    WebUI.click(findTestObject('getcraft.io/project/order/button_add qoute'))

    WebUI.setText(findTestObject('getcraft.io/project/pitch/input_describe your idea'), 'i will serve coca cola in table')

    WebUI.setText(findTestObject('getcraft.io/project/pitch/input_price'), '10000000')

    WebUI.setText(findTestObject('getcraft.io/project/pitch/input_quantity'), '1')

    not_run: WebUI.setText(findTestObject('getcraft.io/project/pitch/input_guarantee'), '10000000')

    WebUI.click(findTestObject('getcraft.io/project/pitch/button_submit quote'))

    not_run: WebUI.verifyElementText(findTestObject('getcraft.io/project/order/verify_guaranteed'), '10,000,000 views', 
        FailureHandling.CONTINUE_ON_FAILURE)

    not_run: WebUI.verifyElementText(findTestObject('getcraft.io/project/order/verify_revenue'), '10.000.000')

    not_run: WebUI.verifyElementText(findTestObject('getcraft.io/project/order/verify_idea'), 'i wiil post coca cola in table')

    WebUI.closeBrowser()
}

