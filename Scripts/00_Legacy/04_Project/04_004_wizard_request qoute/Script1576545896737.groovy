import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_find creator'))

WebUI.click(findTestObject('getcraft.io/root/wizard/button_OK Sure'))

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('getcraft.io/root/wizard/verify_1st_choice'), 'Strategic specialists')

WebUI.verifyElementText(findTestObject('getcraft.io/root/wizard/verify_2nd_choice'), 'Content producers')

not_run: WebUI.verifyElementText(findTestObject('getcraft.io/root/wizard/verify_3rd_choice'), 'Sponsored content')

WebUI.click(findTestObject('getcraft.io/root/wizard/verify_1st_choice'))

WebUI.click(findTestObject('getcraft.io/root/wizard/button_next'))

WebUI.verifyElementText(findTestObject('getcraft.io/root/wizard/verify_1st_choice'), 'Marketing Specialists')

WebUI.verifyElementText(findTestObject('getcraft.io/root/wizard/verify_2nd_choice'), 'Media Buyers')

WebUI.verifyElementText(findTestObject('getcraft.io/root/wizard/verify_3rd_choice'), 'Researchers')

WebUI.verifyElementText(findTestObject('getcraft.io/root/wizard/verify_4th_choice'), 'Tech Specialists')

WebUI.click(findTestObject('getcraft.io/root/wizard/verify_1st_choice'))

WebUI.click(findTestObject('getcraft.io/root/wizard/button_next'))

WebUI.click(findTestObject('getcraft.io/root/wizard/radio_1st_choice'))

WebUI.click(findTestObject('getcraft.io/root/wizard/button_next 2'))

WebUI.click(findTestObject('getcraft.io/root/wizard/checkbox_1st_choice'))

WebUI.click(findTestObject('getcraft.io/root/wizard/button_next 2'))

WebUI.click(findTestObject('getcraft.io/root/wizard/checkbox_1st_choice'))

WebUI.click(findTestObject('getcraft.io/root/wizard/button_next 2'))

WebUI.click(findTestObject('getcraft.io/root/wizard/button_next 2'))

WebUI.verifyElementText(findTestObject('getcraft.io/root/wizard/verify_congratulation'), 'Congratulation!')

WebUI.click(findTestObject('getcraft.io/root/wizard/button_request qoute'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project-add/input_brand name'), 0)

WebUI.closeBrowser()

