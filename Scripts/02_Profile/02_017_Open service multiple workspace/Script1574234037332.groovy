import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'testing2@mailinator.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/root/a_profile'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/product_name'), 15)

product_title = WebUI.getText(findTestObject('getcraft.io/product/product_name'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu WISP2'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu WISP2'))

WebUI.click(findTestObject('getcraft.io/root/select_worksapce WISP2'))

WebUI.delay(10, FailureHandling.STOP_ON_FAILURE)

product_title2 = WebUI.getText(findTestObject('getcraft.io/product/product_name'))

WebUI.verifyNotEqual(product_title, product_title2)

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu WISP2'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu WISP2'))

WebUI.click(findTestObject('getcraft.io/root/select_worksapce WISP2'))

WebUI.delay(10, FailureHandling.STOP_ON_FAILURE)

product_title3 = WebUI.getText(findTestObject('getcraft.io/product/product_name'))

WebUI.verifyEqual(product_title3, product_title)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

