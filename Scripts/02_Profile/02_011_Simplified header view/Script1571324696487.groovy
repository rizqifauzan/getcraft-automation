import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL + 'ghinaya')

WebUI.maximizeWindow()

'Verifying Header elements'
WebUI.verifyElementPresent(findTestObject('getcraft.io/img_logo'), 5)

logoGetCRAFT = findTestObject('getcraft.io/img_logo')

//linkHelp = findTestObject('getcraft.io/a_help')
btnSignUp = findTestObject('getcraft.io/creator-profile/btn_Sign up - wisp 2')

imgAlt = WebUI.getAttribute(logoGetCRAFT, 'alt')

//helpClicked = WebUI.verifyElementClickable(linkHelp)
signinClick = findTestObject('getcraft.io/creator-profile/btn_Sign in - wisp 2')

WebUI.verifyMatch(imgAlt, 'GetCraft - Premium Creative Network', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/button_Sign In'), 3)

WebUI.verifyElementPresent(findTestObject('getcraft.io/sign-up/btn_sign up'), 3)

WebUI.closeBrowser()

