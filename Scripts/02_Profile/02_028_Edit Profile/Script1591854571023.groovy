import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'topi6@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/root/a_profile'), FailureHandling.STOP_ON_FAILURE)

profile_name1 = WebUI.getText(findTestObject('getcraft.io/product/profile name'))

WebUI.click(findTestObject('getcraft.io/product/bt_edit profile'))

WebUI.setText(findTestObject('getcraft.io/product/insert_Profilename'), 'm')

WebUI.setText(findTestObject('getcraft.io/product/insert_ProfileID'), 'm')

WebUI.click(findTestObject('getcraft.io/creator-profile/save'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('getcraft.io/root/a_browse creator'), 10)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.refresh()

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/bt_edit profile'), 10)

WebUI.waitForElementClickable(findTestObject('getcraft.io/root/a_browse creator'), 10)

profile_name2 = WebUI.getText(findTestObject('getcraft.io/product/profile name'))

WebUI.verifyNotEqual(profile_name1, profile_name2)

Url = WebUI.getUrl()

if (Url.contains(profile_name2)) {
    println('match')
} else {
    KeywordUtil.markFailed('Message does not contain expected text.')
}

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

