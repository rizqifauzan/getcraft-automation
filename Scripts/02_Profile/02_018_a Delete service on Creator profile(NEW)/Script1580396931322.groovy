import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'topi5@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

not_run: WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

not_run: WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('getcraft.io/root/a_profile'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/product/a_Add a new service'), 10)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/product/a_Add a new service'))

//generate random integer
String Random

Random = ((Math.random() * 999) as int)

WebUI.setText(findTestObject('getcraft.io/product/Add/input_service title'), serviceTitle + Random)

WebUI.setText(findTestObject('getcraft.io/product/Add/div_service topic'), 'au')

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/select_jakarta'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_category'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_category_Celebrities'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_sub-category'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_sub-category_instagram photo'))

WebUI.setText(findTestObject('getcraft.io/product/Add/div_Service description'), serviceDescription)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/product/Add/div_service language'))

WebUI.click(findTestObject('getcraft.io/product/Add/div_Bahasa Malaysia'))

WebUI.setText(findTestObject('getcraft.io/product/Add/input_Cost'), '10000000')

WebUI.setText(findTestObject('getcraft.io/product/Add/input_Guaranteed Views'), '500000')

WebUI.uploadFile(findTestObject('getcraft.io/product/Add/input_upload sample'), GlobalVariable.imagePath)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.uploadFile(findTestObject('getcraft.io/product/Add/input_upload sample'), GlobalVariable.imagePath2)

not_run: WebUI.uploadFile(findTestObject('getcraft.io/product/Add/input_upload sample'), GlobalVariable.imagePath3)

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/product/Add/cancel_upload'), 10)

WebUI.click(findTestObject('getcraft.io/product/Add/button_send for review'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/product_name'), 60)

if (WebUI.verifyElementVisible(findTestObject('getcraft.io/account-setting/allow_notif'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))
} else {
    WebUI.delay(2)
}

not_run: WebUI.refresh()

not_run: WebUI.waitForElementPresent(findTestObject('getcraft.io/product/product_name'), 60)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('getcraft.io/product/product_name'), serviceTitle + Random)

WebUI.verifyElementText(findTestObject('getcraft.io/product/verify_newest service status-In Review'), 'In review')

WebUI.click(findTestObject('getcraft.io/product/button_more newest service'))

WebUI.click(findTestObject('getcraft.io/product/delete_Service'))

WebUI.click(findTestObject('getcraft.io/product/delete_Service_popup'))

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextNotPresent(serviceTitle + Random, false)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

