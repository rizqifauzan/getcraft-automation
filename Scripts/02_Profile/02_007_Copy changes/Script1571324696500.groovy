import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Sign up here'))

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_firsrname'), 'Client')

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_lastname'), 'Automation')

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_Email'), RandomStringUtils.randomAlphabetic(10) + 'Client@mailnesia.com')

WebUI.setEncryptedText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input Password'), 'uNO6Bw6rL/yn44aqSHH1sg==')

WebUI.clickOffset(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_checkbox'), 0, 0)

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Continue button'))

Textnya = WebUI.getText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Button_Request a code'))


if( Textnya == "Request a code"){
	KeywordUtil.markPassed("Text as Expected!")
}else{
	KeywordUtil.markFailed("Different Text bro!")
}


WebUI.delay(3)

WebUI.closeBrowser()

