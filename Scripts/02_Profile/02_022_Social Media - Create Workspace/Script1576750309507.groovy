import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
//random string or number
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
//get project dir
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(null)

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.waitForPageLoad(GlobalVariable.G_Timeout_Small)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), uname)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), passwd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))
} else {
    WebUI.delay(2)
}

if (WebUI.verifyElementVisible(findTestObject('getcraft.io/project/add/amanda_chat popup'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/project/add/amanda_chat popup'))
} else {
    WebUI.delay(2)
}

WebUI.click(findTestObject('getcraft.io/root/a_profile'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/icon_user menu - profile'), 5)

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user menu - profile'))

WebUI.delay(3)

WebUI.click(findTestObject('getcraft.io/root/btn_Create new workspace'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/btn_Role creator'), 5)

WebUI.click(findTestObject('getcraft.io/root/btn_Role creator'))

WebUI.click(findTestObject('getcraft.io/root/btn_Start crafting'))

String Acak

Acak = ((Math.random() * 5) as int)

WebUI.delay(3)

AssetsDir = (RunConfiguration.getProjectDir() + '/Assets/')

WebUI.verifyElementClickable(findTestObject('getcraft.io/root/lbl_photo workspace'), FailureHandling.STOP_ON_FAILURE)

WebUI.uploadFile(findTestObject('getcraft.io/root/lbl_photo workspace'), (AssetsDir + Acak) + '.jpg')

WebUI.click(findTestObject('getcraft.io/root/btn_save photo workspace'))

WebUI.setText(findTestObject('getcraft.io/root/txt_workspace name'), 'CreatifBGT' + RandomStringUtils.randomNumeric(3))

WebUI.click(findTestObject('getcraft.io/root/btn_workspace type'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/txt_city country'), 6)

WebUI.setText(findTestObject('getcraft.io/root/txt_city country'), 'Bogor')

WebUI.delay(2)

WebUI.sendKeys(findTestObject('getcraft.io/root/txt_city country'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('getcraft.io/root/txt_occupation compay'), 'Project')

WebUI.delay(2)

WebUI.sendKeys(findTestObject('getcraft.io/root/txt_occupation compay'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('getcraft.io/root/txt_describe workspace'), 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ')

WebUI.setText(findTestObject('getcraft.io/root/txt_industry topic'), 'Gam')

WebUI.delay(2)

WebUI.sendKeys(findTestObject('getcraft.io/root/txt_industry topic'), Keys.chord(Keys.ENTER))

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/btn_sosmed ig'), 5)

WebUI.click(findTestObject('getcraft.io/root/btn_sosmed ig'))

WebUI.setText(findTestObject('getcraft.io/root/txt_ig username'), igUname)

WebUI.click(findTestObject('getcraft.io/root/btn_submit sosmed'))

WebUI.verifyElementClickable(findTestObject('getcraft.io/root/btn_Continue - create workspace'))

WebUI.delay(3)

WebUI.click(findTestObject('getcraft.io/root/btn_Continue - create workspace'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/a_profile'), 15)

WebUI.click(findTestObject('getcraft.io/root/a_profile'))

WebUI.delay(5)

WebUI.acceptAlert()

WebUI.delay(5)

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user menu - profile'))

WebUI.click(findTestObject('getcraft.io/root/btn_betacreatif - profile2'))

WebUI.delay(8)

WebUI.closeBrowser()

