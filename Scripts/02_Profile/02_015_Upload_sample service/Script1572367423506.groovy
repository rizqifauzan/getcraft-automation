import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
//random string or number
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
//get project dir
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

if (WebUI.verifyElementVisible(findTestObject('Object Repository/GETCRAFT/create_brief/Language_indo'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('GETCRAFT/login/Select_language'))

    WebUI.click(findTestObject('GETCRAFT/login/english_language'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(3)
} else {
    WebUI.delay(3)
}

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.CreatorEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.ClientPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))


if (WebUI.verifyElementVisible(findTestObject('getcraft.io/account-setting/allow_notif'), FailureHandling.OPTIONAL)) {
	WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))
} else {
	WebUI.delay(5)
}

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/Goto_profile'))

if (WebUI.verifyElementVisible(findTestObject('getcraft.io/account-setting/allow_notif'), FailureHandling.OPTIONAL)) {
	WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))
} else {
	WebUI.delay(3)
}

WebUI.waitForElementClickable(findTestObject('getcraft.io/product/a_Add a new service'), 0)

WebUI.click(findTestObject('getcraft.io/product/a_Add a new service'))

//generate random integer
String Random

Random = ((Math.random() * 999) as int)

WebUI.setText(findTestObject('getcraft.io/product/Add/input_service title'), serviceTitle + Random)

WebUI.setText(findTestObject('getcraft.io/product/Add/div_service topic'), 'au')

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/select_jakarta'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_category'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_category_Celebrities'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_sub-category'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_sub-category_instagram photo'))

WebUI.setText(findTestObject('getcraft.io/product/Add/div_Service description'), serviceDescription)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/product/Add/div_service language'))

WebUI.click(findTestObject('getcraft.io/product/Add/div_Bahasa Malaysia'))

WebUI.setText(findTestObject('getcraft.io/product/Add/input_Cost'), '10000000')

WebUI.setText(findTestObject('getcraft.io/product/Add/input_Guaranteed Views'), '500000')

WebUI.uploadFile(findTestObject('getcraft.io/product/Add/input_upload file'), '/Users/rizu-mac/Work/Assets/HappyWorking.jpeg')

//WebUI.click(findTestObject('getcraft.io/project/add/input_upload file -- submit'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/creator-profile/upload_sample/UploadBar'))

WebUI.click(findTestObject('getcraft.io/project/add/input_upload file -- submit'))

WebUI.delay(10)

