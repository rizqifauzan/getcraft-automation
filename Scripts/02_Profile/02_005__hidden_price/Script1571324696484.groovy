import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'julo5@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/account-setting/allow_notif'), 30)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

WebUI.click(findTestObject('getcraft.io/root/a_profile'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/product_name'), 20)

WebUI.click(findTestObject('getcraft.io/product/bt_edit profile'))

toogle = WebUI.getAttribute(findTestObject('getcraft.io/product/verify_toogle(PROD)'), 'value')

if (toogle.equals('true')) {
    WebUI.delay(2)
} else {
    WebUI.click(findTestObject('getcraft.io/product/toogle_hidden_price'))
}

WebUI.click(findTestObject('getcraft.io/creator-profile/save'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('getcraft.io/root/a_browse creator'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 20)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu WISP2'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/root/Signout_WISP2'))

WebUI.delay(3)

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 15)

WebUI.setText(findTestObject('getcraft.io/root/Searchbox_Homepage'), 'jiren')

WebUI.sendKeys(findTestObject('getcraft.io/root/Searchbox_Homepage'), Keys.chord(Keys.ENTER))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 25)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'))

not_run: WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/click_ProfileID'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/product/product_price hide'), 10)

WebUI.mouseOver(findTestObject('getcraft.io/product/card title'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/btn_see details'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('getcraft.io/product/product_price hide'), 10)

WebUI.refresh()

WebUI.verifyElementPresent(findTestObject('getcraft.io/product/product_price hide-Details'), 10)

WebUI.closeBrowser()

