import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'betacraft@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'Pass4312')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

//if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.OPTIONAL)) {
//    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))
//} else {
//    WebUI.delay(2)
//}
WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 20)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/root/a_profile'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/product_name'), 20)

WebUI.delay(3)

String serviceName = WebUI.getText(findTestObject('getcraft.io/product/product_name'))

if (serviceName) {
    KeywordUtil.markPassed(serviceName)
} else {
    KeywordUtil.markFailed(serviceName)
}

if (WebUI.verifyElementPresent(findTestObject('getcraft.io/creator-profile/product_timestamp'), 5) == true) {
    timeStamp = WebUI.getText(findTestObject('getcraft.io/creator-profile/product_timestamp'))

    KeywordUtil.markPassed(timeStamp)
}

WebUI.closeBrowser()

