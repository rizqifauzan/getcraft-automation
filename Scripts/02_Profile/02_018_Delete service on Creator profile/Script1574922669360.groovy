import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.openBrowser(null)

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'betacraft02@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'Pass4312')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

not_run: WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

WebUI.verifyTextNotPresent('My service', false)

not_run: WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

not_run: WebUI.click(findTestObject('getcraft.io/kebab-menu/img_profile'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/product/a_Add a new service'), 5)

WebUI.click(findTestObject('getcraft.io/product/a_Add a new service'))

//generate random integer
String Random

Random = ((Math.random() * 999) as int)

WebUI.setText(findTestObject('getcraft.io/product/Add/input_service title'), serviceTitle + Random)

WebUI.setText(findTestObject('getcraft.io/product/Add/div_service topic'), 'au')

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/select_jakarta'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_category'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_category_Celebrities'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_sub-category'))

WebUI.click(findTestObject('getcraft.io/product/Add/input_sub-category_instagram photo'))

WebUI.setText(findTestObject('getcraft.io/product/Add/div_Service description'), serviceDescription)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/product/Add/div_service language'))

WebUI.click(findTestObject('getcraft.io/product/Add/div_Bahasa Malaysia'))

WebUI.setText(findTestObject('getcraft.io/product/Add/input_Cost'), '10000000')

WebUI.setText(findTestObject('getcraft.io/product/Add/input_Guaranteed Views'), '500000')

String RandomImg

RandomImg = ((Math.random() * 5) as int)

AssetsDir = (RunConfiguration.getProjectDir() + '/Assets/')

for (i = 1; i <= 3; i++) {
    WebUI.uploadFile(findTestObject('getcraft.io/product/Add/input_upload sample'), (AssetsDir + RandomImg) + '.jpg')

    WebUI.delay(5)
}

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/product/Add/cancel_upload'), 10)

WebUI.click(findTestObject('getcraft.io/product/Add/button_send for review'))

WebUI.delay(5)

if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'))
} else {
    WebUI.delay(2)
}

//
//WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'))
String serviceName = WebUI.getText(findTestObject('getcraft.io/product/product_name'))

if (serviceName) {
    KeywordUtil.markPassed(serviceName)
} else {
    KeywordUtil.markFailed(serviceName)
}

WebUI.waitForElementClickable(findTestObject('getcraft.io/creator-profile/btn_kebab menu service'), 3)

WebUI.click(findTestObject('getcraft.io/creator-profile/btn_kebab menu service'))

WebUI.click(findTestObject('getcraft.io/creator-profile/btn_delete service'))

WebUI.click(findTestObject('getcraft.io/creator-profile/popup_service delete'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/product_name'), 10)

String confirmed = WebUI.getText(findTestObject('getcraft.io/product/product_name'))

WebUI.delay(7)

KeywordUtil.markPassed(serviceName)

KeywordUtil.markPassed(confirmed)

if (confirmed == serviceName) {
    KeywordUtil.markFailed(confirmed)
} else {
    KeywordUtil.markPassed(confirmed)
}

WebUI.closeBrowser()

