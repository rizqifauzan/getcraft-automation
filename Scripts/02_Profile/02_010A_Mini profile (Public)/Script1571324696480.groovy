import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.openBrowser(null)

WebUI.navigateToUrl(GlobalVariable.Staging_URL + 'betacreator')

WebUI.maximizeWindow()

'Verifying mini profile is show'
Mini = findTestObject('getcraft.io/creator-profile/topbar_mini profile')

if (Mini) {
    KeywordUtil.markPassed("Mini Profile is show")
} else {
    KeywordUtil.markFailed("xxX Mini Profile is not show Xxx")
}

WebUI.waitForElementClickable(findTestObject('getcraft.io/creator-profile/btn_share profile Update'), 7)

'Verifying share button is clickable'
btn_share = findTestObject('getcraft.io/creator-profile/btn_share profile Update')

WebUI.delay(5)

yesclick = WebUI.verifyElementClickable(btn_share)

if (Mini && yesclick) {
    KeywordUtil.markPassed('Button share is clickable')
} else {
    KeywordUtil.markFailed('xxX Button share is clickable Xxx')
}

'Verifying element in Mini profile'
imgProfile = findTestObject('getcraft.io/creator-profile/Mini profile/img_Profile Mini')
//profileType = findTestObject('getcraft.io/creator-profile/Mini profile/txt_Profiletype Mini')

WebUI.verifyElementPresent(imgProfile, 2)
WebUI.verifyElementPresent(btn_share, 2)
//WebUI.verifyElementPresent(profileType, 2)

WebUI.closeBrowser()

