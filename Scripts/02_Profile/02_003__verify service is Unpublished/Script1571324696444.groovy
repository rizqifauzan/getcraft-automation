import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'julo4@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/account-setting/allow_notif'), 30)

WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/Goto_profile'))

WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))

WebUI.verifyElementText(findTestObject('getcraft.io/product/verify_newest service status-In Review'), 'Published')

WebUI.click(findTestObject('getcraft.io/product/Option_button'))

WebUI.click(findTestObject('getcraft.io/product/a_unpublish'))

WebUI.click(findTestObject('getcraft.io/product/a_unpublish_popup'))

WebUI.verifyElementText(findTestObject('getcraft.io/product/verify_newest service status-In Review'), 'Unpublished')

Title = WebUI.getText(findTestObject('getcraft.io/product/product_name'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/root/a_browse creator'))

WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))

WebUI.click(findTestObject('getcraft.io/marketplace/a_celebrities'))

WebUI.setText(findTestObject('getcraft.io/marketplace/input_search'), Title)

WebUI.click(findTestObject('getcraft.io/marketplace/button_submit search'))

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/product/verify_card_MP'), 10)

WebUI.closeBrowser()

