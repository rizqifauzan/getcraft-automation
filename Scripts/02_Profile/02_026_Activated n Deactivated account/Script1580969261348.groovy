import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), Uname)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), Passw)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.refresh()

WebUI.click(findTestObject('getcraft.io/root/a_profile'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/product_name'), 20)

WebUI.verifyElementText(findTestObject('getcraft.io/product/verify_newest service status-In Review'), 'Published')

WebUI.executeJavaScript('window.open();', [])

currentWindow = WebUI.getWindowIndex()

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.navigateToUrl('https://beta.getcraft.com/search')

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 20)

WebUI.delay(3)

WebUI.setText(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), 'minyak telon')

WebUI.sendKeys(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), Keys.chord(Keys.ENTER))

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/product_name'), 10)

WebUI.verifyElementPresent(findTestObject('getcraft.io/product/profile topi4'), 5)

WebUI.switchToWindowIndex(0)

WebUI.click(findTestObject('getcraft.io/product/bt_edit profile'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/product/bt_deactivated account'), 10)

WebUI.click(findTestObject('getcraft.io/product/bt_deactivated account'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/product/Really deactivated account'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/product/bt_OK'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Unpublished', false)

WebUI.switchToWindowIndex(1)

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/product/profile topi4'), 5)

WebUI.switchToWindowIndex(0)

WebUI.click(findTestObject('getcraft.io/product/bt_edit profile'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/product/bt_activated account'), 10)

WebUI.click(findTestObject('getcraft.io/product/bt_activated account'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/product/Really activated account'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/product/bt_OK'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Published', false)

WebUI.switchToWindowIndex(1)

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.verifyElementPresent(findTestObject('getcraft.io/product/profile topi4'), 5)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

