import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), uname_Mini)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), passwd_Mini)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

if (WebUI.verifyElementPresent(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))
} else {
    WebUI.delay(2)
}

WebUI.delay(2)

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/kebab-menu/img_profile'))

WebUI.scrollToPosition(9999999, 9999999)

'Verifying mini profile is show'
Mini = findTestObject('getcraft.io/creator-profile/topbar_mini profile')

if (Mini) {
    KeywordUtil.markPassed('Mini profile is show')
} else {
    KeywordUtil.markFailed('xxX Mini profile is not show! Xxx')
}

'Verifying share button is clickable'
btn_share = findTestObject('getcraft.io/creator-profile/btn_share profile Update')

yesclick = WebUI.verifyElementClickable(btn_share)

if (Mini && yesclick) {
    KeywordUtil.markPassed('Button share is clickable')
} else {
    KeywordUtil.markFailed('xxX Button share is clickable Xxx')
}

'Change Img Profile'
String Random

Random = ((Math.random() * 5) as int)

AssetsDir = (RunConfiguration.getProjectDir() + '/Assets/')

WebUI.uploadFile(findTestObject('getcraft.io/root/lbl_photo workspace'), (AssetsDir + Random) + '.jpg')

WebUI.click(findTestObject('getcraft.io/root/btn_save photo workspace'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/creator-profile/img_profile'), 5)

WebUI.verifyElementPresent(findTestObject('getcraft.io/creator-profile/img_profile'), 3)

imgSrc = WebUI.getAttribute(findTestObject('getcraft.io/creator-profile/img_profile'), 'src')

WebUI.delay(5)

WebUI.executeJavaScript('window.open();', [])

currentWindow = WebUI.getWindowIndex()

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.navigateToUrl(imgSrc)

errorImg = WebUI.verifyTextNotPresent('Error', false)

imgSrc_NewTab = WebUI.getAttribute(findTestObject('getcraft.io/creator-profile/img_NewTab'), 'src')

WebUI.verifyMatch(imgSrc, imgSrc_NewTab, false)

WebUI.closeWindowIndex(currentWindow + 1)

WebUI.delay(3)

//'Verifying element in Mini profile'
//imgProfile = findTestObject('getcraft.io/creator-profile/Mini profile/img_Profile Mini')
//
//profileType = findTestObject('getcraft.io/creator-profile/Mini profile/txt_Profiletype Mini')
//
//WebUI.verifyElementPresent(imgProfile, 5)
//
//WebUI.verifyElementPresent(btn_share, 5)
//
//WebUI.verifyElementPresent(profileType, 5)
WebUI.closeBrowser()

