import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIKeywordMain as WebUIKeywordMain
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//random string or number
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.CreatorEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.CreatorPassw)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/root/a_profile'), FailureHandling.STOP_ON_FAILURE)

not_run: if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))
} else {
    WebUI.delay(2)
}

not_run: WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

not_run: WebUI.click(findTestObject('getcraft.io/kebab-menu/img_profile'))

WebUI.click(findTestObject('getcraft.io/product/a_Add a new service'))

String Random = ((Math.random() * 5) as int)

AssetsDir = (RunConfiguration.getProjectDir() + '/Assets/')

WebUI.uploadFile(findTestObject('getcraft.io/product/Add/input_upload sample'), (AssetsDir + Random) + '.jpg')

Random = ((Math.random() * 5) as int)

WebUI.uploadFile(findTestObject('getcraft.io/product/Add/input_upload sample'), (AssetsDir + Random) + '.jpg')

Random = ((Math.random() * 5) as int)

WebUI.uploadFile(findTestObject('getcraft.io/product/Add/input_upload sample'), (AssetsDir + Random) + '.jpg')

//Make sure heightBefore is not 0px;
heightBefore = WebUI.getAttribute(findTestObject('getcraft.io/creator-profile/upload_sample/div_progressList Dock'), 'style')

KeywordUtil.logInfo(heightBefore)

//Click uploadBar (Minimize)
WebUI.click(findTestObject('getcraft.io/creator-profile/upload_sample/div_uploadBar Dock'))

//height after upload dock minimized (should be 0px;)
height = WebUI.getAttribute(findTestObject('getcraft.io/creator-profile/upload_sample/div_progressList Dock'), 'style')

KeywordUtil.logInfo(height)

if (height == 'height: 0px;') {
    KeywordUtil.logInfo(height)
} else {
    KeywordUtil.logInfo(height)
}

////Close dock
WebUI.waitForElementNotPresent(findTestObject('getcraft.io/creator-profile/upload_sample/svg_closeUploadProgress'), 20)

WebUI.click(findTestObject('getcraft.io/creator-profile/upload_sample/svg_closeUploadProgress'))

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/creator-profile/upload_sample/div_progressList Dock'), 3)

WebUI.closeBrowser()

