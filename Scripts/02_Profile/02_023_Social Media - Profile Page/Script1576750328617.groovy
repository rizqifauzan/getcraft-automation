import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), uname)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), passwd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

not_run: if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))
} else {
    WebUI.delay(2)
}

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/a_profile'), 5)

txtProfile = WebUI.getText(findTestObject('getcraft.io/root/a_profile'))

if (txtProfile == 'Profile') {
    WebUI.click(findTestObject('getcraft.io/root/a_profile'))
} else {
    KeywordUtil.markFailed(txtProfile)
}

'Instagram Connection'
WebUI.verifyElementNotPresent(findTestObject('getcraft.io/root/div_ig connected'), 5)

WebUI.scrollToElement(findTestObject('getcraft.io/root/btn_sosmed ig'), 200)

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/root/btn_sosmed ig'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/txt_ig username'), 5)

WebUI.setText(findTestObject('getcraft.io/root/txt_ig username'), igUname)

WebUI.click(findTestObject('getcraft.io/root/btn_submit sosmed'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/div_ig connected'), 5)

verifyIG = WebUI.getAttribute(findTestObject('getcraft.io/root/div_connected IG h5'), 'title')

if (verifyIG == igUname) {
    WebUI.click(findTestObject('getcraft.io/root/btn_sosmed ig'))

    WebUI.verifyElementPresent(findTestObject('getcraft.io/root/div_yt connected'), 5)

    WebUI.click(findTestObject('getcraft.io/root/btn_sosmed dc sure'))
} else {
    KeywordUtil.markFailed(verifyIG)
}

'Youtube Connection'
not_run: WebUI.verifyElementNotPresent(findTestObject('getcraft.io/root/div_yt connected'), 5)

not_run: WebUI.click(findTestObject('getcraft.io/root/btn_sosmed youtube - profile'))

not_run: WebUI.waitForElementPresent(findTestObject('getcraft.io/root/txt_yt link - profile'), 5)

not_run: WebUI.setText(findTestObject('getcraft.io/root/txt_yt link - profile'), youtubeLink)

not_run: WebUI.focus(findTestObject('getcraft.io/root/dialog_submit sosmed'))

not_run: WebUI.click(findTestObject('getcraft.io/root/btn_submit sosmed'))

not_run: verifyYT = WebUI.getAttribute(findTestObject('getcraft.io/root/div_connected YT h5'), 'title')

not_run: if (verifyYT == 'connected') {
    WebUI.click(findTestObject('getcraft.io/root/btn_sosmed youtube - profile'))

    WebUI.click(findTestObject('getcraft.io/root/btn_sosmed dc sure'))
} else {
    KeywordUtil.markFailed(verifyYT)
}

WebUI.closeBrowser()

