import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(null)

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.waitForPageLoad(GlobalVariable.G_Timeout_Small)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'betacraft02@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'Pass4312')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))
	
	if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.OPTIONAL)) {
		WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))
	} else {
		WebUI.delay(2)
	}

WebUI.verifyTextNotPresent('My service', false)

not_run: WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

not_run: WebUI.click(findTestObject('getcraft.io/kebab-menu/img_profile'))

String serviceName = WebUI.getText(findTestObject('getcraft.io/product/product_name'))

if (serviceName) {
    KeywordUtil.markPassed(serviceName)
} else {
    KeywordUtil.markFailed(serviceName)
}

WebUI.waitForElementClickable(findTestObject('getcraft.io/creator-profile/btn_kebab menu service'), 3)

WebUI.click(findTestObject('getcraft.io/creator-profile/btn_kebab menu service'))

WebUI.click(findTestObject('getcraft.io/creator-profile/btn_edit service'))

WebUI.setText(findTestObject('getcraft.io/product/Add/div_Service description'), ' [EDITED]')

WebUI.click(findTestObject('getcraft.io/product/Add/button_save draft'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'), 3)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/creator-profile/btn_kebab menu service'), 3)

WebUI.click(findTestObject('getcraft.io/creator-profile/btn_kebab menu service'))

WebUI.click(findTestObject('getcraft.io/creator-profile/btn_edit service'))

serviceDesc = WebUI.getText(findTestObject('getcraft.io/product/Add/div_Service description'))
editedDesc = "[EDITED]"+serviceDesc

if (serviceDesc == editedDesc) {
    KeywordUtil.markPassed(serviceDesc)
} else {
    KeywordUtil.markFailed(serviceDesc)
}

WebUI.click(findTestObject('getcraft.io/product/Add/button_save draft'))

