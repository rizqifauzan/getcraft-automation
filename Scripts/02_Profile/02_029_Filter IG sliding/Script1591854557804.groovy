import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 25)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Stats'))

Follower_OLD = WebUI.getAttribute(findTestObject('getcraft.io/marketplace/NewSearch/IG_Maxx'), 'value')

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), uname)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), passwd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/root/a_profile'), FailureHandling.STOP_ON_FAILURE)

'Instagram Connection'
WebUI.verifyElementNotPresent(findTestObject('getcraft.io/root/div_ig connected'), 5)

WebUI.scrollToElement(findTestObject('getcraft.io/root/btn_sosmed ig'), 200)

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/root/btn_sosmed ig'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/txt_ig username'), 5)

WebUI.setText(findTestObject('getcraft.io/root/txt_ig username'), igUname)

WebUI.click(findTestObject('getcraft.io/root/btn_submit sosmed'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/div_ig connected'), 5)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.executeJavaScript('window.open();', [])

currentWindow = WebUI.getWindowIndex()

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.navigateToUrl('beta.getcraft.com')

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 25)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Stats'))

Follower_NEW = WebUI.getAttribute(findTestObject('getcraft.io/marketplace/NewSearch/IG_Maxx'), 'value')

WebUI.verifyNotEqual(Follower_OLD, Follower_NEW)

WebUI.switchToWindowIndex(currentWindow)

WebUI.click(findTestObject('getcraft.io/root/btn_sosmed ig'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/div_yt connected'), 5)

WebUI.click(findTestObject('getcraft.io/root/btn_sosmed dc sure'))

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 25)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Stats'))

Follower_NEWest = WebUI.getAttribute(findTestObject('getcraft.io/marketplace/NewSearch/IG_Maxx'), 'value')

WebUI.verifyEqual(Follower_OLD, Follower_NEWest)

WebUI.closeBrowser()

