import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'julo7@mailnesia.com')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.click(findTestObject('getcraft.io/root/a_profile'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'), 20)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'))

WebUI.delay(5)

WebUI.waitForElementClickable(findTestObject('getcraft.io/product/a_Add a new service'), 0)

WebUI.click(findTestObject('getcraft.io/product/a_Add a new service'))

//generate random draftname
int Random

Random = ((Math.random() * 999) as int)

String New_Draft_Value = serviceTitle + Random

WebUI.setText(findTestObject('getcraft.io/product/Add/input_service title'), New_Draft_Value)

WebUI.click(findTestObject('getcraft.io/product/Add/button_save draft'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/product_name'), 50)

not_run: WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.waitForElementPresent(findTestObject('getcraft.io/product/product_name'), 20)

WebUI.delay(3)

WebUI.verifyTextPresent(New_Draft_Value, true)

WebUI.verifyElementText(findTestObject('getcraft.io/product/verify_newest service status-Draft'), 'Draft')

WebUI.closeBrowser()

