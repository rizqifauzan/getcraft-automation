import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.openBrowser(null)

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.ClientEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.ClientPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextNotPresent('My service', false)

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/kebab-menu/btn_manage_team'))

textnya = 'General Setting'

textgone = WebUI.verifyTextNotPresent(textnya, true, FailureHandling.CONTINUE_ON_FAILURE)

'Verifying General Setting has been removed'
if (textgone) {
    KeywordUtil.markPassed('General Setting is removed')
} else {
    KeywordUtil.markFailed('xxX General Setting is not removed Xxx')
}

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/kebab-menu/btn_making payment'))

textnya = 'General Setting'

textgone = WebUI.verifyTextNotPresent(textnya, true, FailureHandling.CONTINUE_ON_FAILURE)

'Verifying General Setting has been removed from making payment button'
if (textgone) {
    KeywordUtil.markPassed('General Setting is removed from making payment button')
} else {
    KeywordUtil.markFailed('xxX General Setting is not removed from making payment button Xxx')
}

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/kebab-menu/btn_receiving payment'))

textnya = 'General Setting'

textgone = WebUI.verifyTextNotPresent(textnya, true, FailureHandling.CONTINUE_ON_FAILURE)

'Verifying General Setting has been removed from receiving payment button'
if (textgone) {
    KeywordUtil.markPassed('General Setting is removed from receiving payment button')
} else {
    KeywordUtil.markFailed('xxX General Setting is not removed from receiving payment button Xxx')
}

WebUI.closeBrowser()

