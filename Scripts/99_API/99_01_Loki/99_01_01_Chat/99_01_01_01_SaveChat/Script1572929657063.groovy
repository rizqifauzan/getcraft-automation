import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper

Date now = new Date()

long timeStamp = now.getTime() / 1000

JsonSlurper slurper = new JsonSlurper()

def loginRespond = WS.sendRequestAndVerify(findTestObject('API/gandalf/Sign In', [('email') : 'rizqi.fauzan@mailnesia.com'
            , ('password') : 'p@ssw0rd1!', ('baseUrl') : GlobalVariable.baseAPI]))

def parsedlogin = slurper.parseText(loginRespond.getResponseText())

String userId = parsedlogin.get('user').get('id')

String token = parsedlogin.get('token')

String profileId = parsedlogin.get('profiles').get(0).get('id')

def getObrderByIdRespond = WS.sendRequestAndVerify(findTestObject('API/Ancient/Order/Get Order By Id', [('baseUrl') : GlobalVariable.baseAPI, ('token') : token
            , ('userId') : userId, ('profileId') : profileId]))

def parsedOrders = slurper.parseText(getObrderByIdRespond.getResponseText())

// get newest project
def i = parsedOrders.size()-1

String orderId = parsedOrders.get(i).get('order').get('id')

def respondGetChatRoomList = WS.sendRequest(findTestObject('API/loki/Room/getAllChatRoomList', [('token') : token, ('baseUrl') : GlobalVariable.baseAPI
	, ('orderId') : orderId]))

def parsedJson = slurper.parseText(respondGetChatRoomList.getResponseText())

String roomId = parsedJson.get('data').get(0).get('id')

String message = 'Hai From API Katalon API'

def respondSend = WS.sendRequest(findTestObject('API/loki/Chat/Send Chat', [('timeStamp') : timeStamp, ('token') : token, ('baseUrl') : GlobalVariable.baseAPI
	, ('message') : message, ('roomId') : roomId]))

def respond2 = WS.sendRequest(findTestObject('API/loki/Chat/Get Chat by Room ID', [('baseUrl') : GlobalVariable.baseAPI, ('timeStamp') : 0
	, ('token') : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiXCI1ZGJmZjgzZThmOWY3NDAwMWU0NTEwNjhcIiIsImlhdCI6MTU3MjkyNTk2NCwiYXVkIjoiZ2V0Y3JhZnQuY29tIiwiaXNzIjoiZ2V0Y3JhZnQuY29tIn0.-d__kfSlQtK_zNXY3PulFCh3Bz6xVGj84rH-T1JGRlQ'
	, ('roomId') : roomId]))

WS.verifyElementPropertyValue(respond2, 'data[0].content', message)