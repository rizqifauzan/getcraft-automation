import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData 
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper
import static org.assertj.core.api.Assertions.*


JsonSlurper slurper = new JsonSlurper()

Date now = new Date()

long timeStamp = now.getTime() / 1000

def respond = WS.sendRequest(findTestObject('API/Xafier/User/Sign In', [('email') : 'rizqi.fauzan@mailnesia.com', ('password') : 'p@ssw0rd1!'
            , ('baseUrl') : GlobalVariable.baseAPI]))

def token = respond.getResponseText()

token = token.replaceAll('"', '')

def respondGetChatRoomList = WS.sendRequest(findTestObject('API/loki/Room/getAllChatRoomList', [('token') : token, ('baseUrl') : GlobalVariable.baseAPI
	, ('orderId') : '5dd24a65711aa5001b34736b']))

def parsedJson = slurper.parseText(respondGetChatRoomList.getResponseText())

String roomId = parsedJson.get('data').get(0).get('id')

WS.sendRequestAndVerify(findTestObject('API/loki/Room/Add Muted Push Notification', [('baseUrl') : GlobalVariable.baseAPI
            , ('token') : token, ('roomId') : roomId]))

// update charRoom 
respondGetChatRoomList = WS.sendRequest(findTestObject('API/loki/Room/getAllChatRoomList', [('token') : token, ('baseUrl') : GlobalVariable.baseAPI
	, ('orderId') : '5dd24a65711aa5001b34736b']))

def parsedJsonRespondGetChatRoomList = slurper.parseText(respondGetChatRoomList.getResponseText())

def muteEmailUser = parsedJsonRespondGetChatRoomList.get('data').get(0).get('mutedEmailUsers').size()

assertThat(muteEmailUser, 0)

WS.sendRequestAndVerify(findTestObject('API/loki/Room/Add Muted Email', [('baseUrl') : GlobalVariable.baseAPI
	, ('token') : token, ('roomId') : roomId]))

// update charRoom
respondGetChatRoomList = WS.sendRequest(findTestObject('API/loki/Room/getAllChatRoomList', [('token') : token, ('baseUrl') : GlobalVariable.baseAPI
	, ('orderId') : '5dd24a65711aa5001b34736b']))

parsedJsonRespondGetChatRoomList = slurper.parseText(respondGetChatRoomList.getResponseText())

muteEmailUser = parsedJsonRespondGetChatRoomList.get('data').get(0).get('mutedEmailUsers').size()

assertThat(muteEmailUser, 1)

WS.sendRequestAndVerify(findTestObject('API/loki/Room/Add Muted Email', [('baseUrl') : GlobalVariable.baseAPI
	, ('token') : token, ('roomId') : "5dbff946ce468700015668c3"]))

