import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.sun.xml.internal.ws.policy.sourcemodel.AssertionData as AssertionData
import cucumber.runtime.junit.Assertions as Assertions
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper
import static org.assertj.core.api.Assertions.*
import org.testng.asserts.Assertion as Assertion

JsonSlurper slurper = new JsonSlurper()

def respond = WS.sendRequestAndVerify(findTestObject('API/Xafier/User/Sign In', [('email') : 'rizqi.fauzan@mailnesia.com'
            , ('password') : 'p@ssw0rd1!', ('baseUrl') : GlobalVariable.baseAPI]))

def token = respond.getResponseText()

token = token.replaceAll('"', '')

def respond2 = WS.sendRequestAndVerify(findTestObject('API/Xafier/User/GetNewToken', [('baseUrl') : GlobalVariable.baseAPI
            , ('token') : token]))

CucumberKW.comment('')

def parsedJson = slurper.parseText(respond2.getResponseText())

String oldToken = parsedJson.get('oldToken')

String newToken = parsedJson.get('refreshedToken')

assertThat(oldToken, token)

CustomKeywords.'Api.verifyNotSame'(newToken, oldToken)

WS.sendRequestAndVerify(findTestObject('API/Xafier/User/SignOut', [('baseUrl') : GlobalVariable.baseAPI, ('token') : newToken]))

