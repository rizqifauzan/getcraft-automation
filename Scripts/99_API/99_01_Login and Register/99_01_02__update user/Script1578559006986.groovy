import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def respond = WS.sendRequestAndVerify(findTestObject('API/Xafier/User/Sign In', [('email') : 'rizqi.fauzan@mailnesia.com'
            , ('password') : 'p@ssw0rd1!', ('baseUrl') : GlobalVariable.baseAPI]))

def token = respond.getResponseText()

def respon2 = WS.sendRequestAndVerify(findTestObject('API/Xafier/User/getDetailUserByToken', [('baseUrl') : GlobalVariable.baseAPI, ('token') : token]))

WS.sendRequestAndVerify(findTestObject('API/Xafier/User/SignOut', [('baseUrl') : GlobalVariable.baseAPI, ('token') : token]))

