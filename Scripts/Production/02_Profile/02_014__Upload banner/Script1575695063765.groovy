import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'creator02@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/product/VIew_My_Profile'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/product/Banner/element_banner'), 8)

WebUI.takeScreenshot()

WebUI.uploadFile(findTestObject('getcraft.io/product/Banner/uplad banner'), GlobalVariable.imagePath3)

WebUI.delay(3)

WebUI.waitForElementPresent(findTestObject('getcraft.io/creator-profile/save'), 15)

WebUI.click(findTestObject('getcraft.io/creator-profile/save'))

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/product/Banner/element_banner'), 8)

WebUI.focus(findTestObject('getcraft.io/product/Banner/overlay_banner'))

WebUI.click(findTestObject('getcraft.io/product/Banner/remove_banner'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/product/Banner/element_banner'), 8)

WebUI.closeBrowser()

