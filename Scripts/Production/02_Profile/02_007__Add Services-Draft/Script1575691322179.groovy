import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'akun_service@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

if (WebUI.verifyElementVisible(findTestObject('getcraft.io/account-setting/allow_notif'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))
} else {
    WebUI.delay(2)
}

WebUI.waitForElementClickable(findTestObject('getcraft.io/product/a_Add a new service'), 10)

WebUI.click(findTestObject('getcraft.io/product/a_Add a new service'))

//generate random draftname
int Random

Random = ((Math.random() * 999) as int)

String New_Draft_Value = serviceTitle + Random

WebUI.setText(findTestObject('getcraft.io/product/Add/input_service title'), New_Draft_Value)

not_run: WebUI.click(findTestObject('getcraft.io/product/Add/input_category'))

not_run: WebUI.click(findTestObject('getcraft.io/product/Add/input_category_Celebrities'))

not_run: WebUI.click(findTestObject('getcraft.io/product/Add/input_sub-category'))

not_run: WebUI.click(findTestObject('getcraft.io/product/Add/input_sub-category_instagram photo'))

not_run: WebUI.click(findTestObject('getcraft.io/product/Add/div_service topic'))

not_run: WebUI.click(findTestObject('getcraft.io/product/Add/div_service topic - sub'))

not_run: WebUI.click(findTestObject('getcraft.io/product/Add/div_service language'))

not_run: WebUI.click(findTestObject('getcraft.io/product/Add/div_Bahasa Malaysia'))

WebUI.click(findTestObject('getcraft.io/product/Add/button_save draft'))

WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))

WebUI.delay(5)

WebUI.verifyTextPresent(New_Draft_Value, true)

WebUI.verifyElementText(findTestObject('getcraft.io/product/verify_newest service status-Draft'), 'Draft')

WebUI.click(findTestObject('getcraft.io/product/button_more newest service'))

WebUI.click(findTestObject('getcraft.io/product/delete_Service'))

WebUI.click(findTestObject('getcraft.io/product/delete_Service_popup'))

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextNotPresent(New_Draft_Value, false)

WebUI.closeBrowser()

