import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'ilyanahedquiban@gmail.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft123')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.refresh()

WebUI.refresh()

WebUI.click(findTestObject('getcraft.io/root/a_profile'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/product/bt_edit workspace'))

WebUI.click(findTestObject('getcraft.io/product/delete_occup'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/product/delete_occup'))

WebUI.setText(findTestObject('getcraft.io/product/input_occup'), 'con')

WebUI.click(findTestObject('getcraft.io/product/occup_ContenManager'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('getcraft.io/product/input_expertise'), 'cor')

WebUI.click(findTestObject('getcraft.io/product/expertise_corporate'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/creator-profile/save'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/check_occup'), 15)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('getcraft.io/product/check_occup'), 'Content Creator')

WebUI.executeJavaScript('window.open();', [])

currentWindow = WebUI.getWindowIndex()

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.navigateToUrl('getcraft.com/')

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 15)

WebUI.setText(findTestObject('getcraft.io/root/Searchbox_Homepage'), 'Ilyanah Edquiban\n')

not_run: WebUI.click(findTestObject('getcraft.io/root/Searchbox_Homepage Bt'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 25)

WebUI.verifyElementText(findTestObject('getcraft.io/marketplace/MP_Occups'), 'Content Creator')

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'))

WebUI.click(findTestObject('getcraft.io/marketplace/View more'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/occup contenS_PROD'), 5)

WebUI.switchToWindowIndex(currentWindow)

WebUI.refresh()

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('getcraft.io/product/bt_edit profile'), 200)

WebUI.click(findTestObject('getcraft.io/product/bt_edit workspace'))

WebUI.click(findTestObject('getcraft.io/product/delete_occup'))

WebUI.click(findTestObject('getcraft.io/product/delete_occup'))

WebUI.setText(findTestObject('getcraft.io/product/input_occup'), 'med')

WebUI.click(findTestObject('getcraft.io/product/occup_ContenManager'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('getcraft.io/product/input_expertise'), 'commer')

WebUI.click(findTestObject('getcraft.io/product/expertise_corporate'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/creator-profile/save'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.refresh()

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/check_occup'), 15)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('getcraft.io/product/check_occup'), 'Media Buyer')

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.refresh()

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 20)

WebUI.verifyElementText(findTestObject('getcraft.io/marketplace/MP_Occups'), 'Media Buyer')

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'))

WebUI.click(findTestObject('getcraft.io/marketplace/View more'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/occup media buyers'), 5)

WebUI.closeBrowser()

