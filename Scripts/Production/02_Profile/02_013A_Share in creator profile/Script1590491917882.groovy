import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), Uname)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), Passw)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/root/a_profile'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/creator-profile/btn_share profile Update'))

not_run: WebUI.focus(findTestObject('getcraft.io/creator-profile/modal_share profile'))

btnCopy = WebUI.verifyElementClickable(findTestObject('getcraft.io/creator-profile/btn_copy'))

urlPage = WebUI.getUrl()

not_run: urlModal = WebUI.getAttribute(findTestObject('getcraft.io/creator-profile/input_link modal_PROD'), 'value')

not_run: btnSendWA = WebUI.verifyElementClickable(findTestObject('getcraft.io/creator-profile/btn_send via wa_PROD'))

if (btnCopy) {
    KeywordUtil.markPassed('Button Copy is Clickable')
} else {
    KeywordUtil.markFailed('xxX Button Copy is Not Clickable Xxx')
}

if (urlModal == urlPage) {
    urlModal = WebUI.getAttribute(findTestObject('getcraft.io/creator-profile/input_link modal_PROD'), 'value')

    KeywordUtil.markPassed('URL as Expected!')
} else {
    KeywordUtil.markFailed('xxX URL is Not Match Xxx')
}

if (btnSendWA) {
    KeywordUtil.markPassed('Button send via WA is Clickable')
} else {
    KeywordUtil.markFailed('xxX Button send via WA is Not Clickable! Xxx')
}

WebUI.closeBrowser()

