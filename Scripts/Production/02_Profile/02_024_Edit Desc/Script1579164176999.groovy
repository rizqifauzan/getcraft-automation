import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'ilyanahedquiban@gmail.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft123')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/root/a_profile'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/product/bt_edit workspace'))

//generate random integer
def Desc = RandomStringUtils.randomAlphabetic(1)

not_run: WebUiCommonHelper.findWebElement(findTestObject('getcraft.io/product/input_desc'), 30).clear()

not_run: WebUI.sendKeys(findTestObject('getcraft.io/product/input_desc'), Keys.chord(Keys.ENTER, ‘a’))

not_run: WebUI.sendKeys(findTestObject('getcraft.io/product/input_desc'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('getcraft.io/product/input_desc'), Desc)

WebUI.click(findTestObject('getcraft.io/creator-profile/save'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/product/bt_edit workspace'))

Desc = WebUI.getText(findTestObject('getcraft.io/product/input_desc'))

WebUI.click(findTestObject('getcraft.io/product/Cancel profile'))

WebUI.navigateToUrl('getcraft.com')

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 15)

WebUI.setText(findTestObject('getcraft.io/root/Searchbox_Homepage'), 'Ilyanah Edquiban\n')

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 20)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'))

WebUI.verifyTextPresent(Desc, false)

WebUI.closeBrowser()

