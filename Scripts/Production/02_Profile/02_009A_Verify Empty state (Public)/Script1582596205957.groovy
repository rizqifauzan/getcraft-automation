import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIKeywordMain as WebUIKeywordMain
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//random string or number
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL + 'wisp1.153')

WebUI.maximizeWindow()

WebUI.waitForPageLoad(GlobalVariable.G_Timeout_Small)

GeneratedAlt = WebUI.getAttribute(findTestObject('getcraft.io/creator-profile/img_empty service'), 'alt')

Notifnya = WebUI.getText(findTestObject('getcraft.io/creator-profile/text_notif empty'))

Altnya = 'https://assets.getcraft.com/images/empty-state-service.svg'

Textnya = 'This creator hasn\'t added any service yet'

'Verify Img and Text show and correct one'
if ((GeneratedAlt == Altnya) && (Notifnya == Textnya)) {
    KeywordUtil.markPassed('Img & Notif as Expected!')
} else {
    KeywordUtil.markFailed('Different Img & Notif bro!')
}

WebUI.closeBrowser()

