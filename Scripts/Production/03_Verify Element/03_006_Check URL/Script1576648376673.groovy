import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

url = WebUI.getUrl()

// open new  window
WebUI.delay(3)

WebUI.executeJavaScript('window.open();', [])

currentWindow = WebUI.getWindowIndex()

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.navigateToUrl(url)

WebUI.verifyElementPresent(findTestObject('getcraft.io/sign-in/a_forgot password'), 10)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(currentWindow)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In Back'))

WebUI.click(findTestObject('getcraft.io/sign-up/btn_sign up'))

url = WebUI.getUrl()

// open new  window
WebUI.delay(3)

WebUI.executeJavaScript('window.open();', [])

currentWindow = WebUI.getWindowIndex()

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.navigateToUrl(url)

WebUI.verifyElementPresent(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_checkbox'), 
    10)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(currentWindow)

WebUI.click(findTestObject('getcraft.io/root/button_Sign Up Back'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/Join_As Creator'), 5)

WebUI.click(findTestObject('getcraft.io/root/Join_As Creator'))

WebUI.delay(2)

WebUI.verifyTextPresent('creative business', false)

url = WebUI.getUrl()

WebUI.click(findTestObject('getcraft.io/root/homepage'))

WebUI.click(findTestObject('getcraft.io/root/a_briefs'))

// open new  window
WebUI.delay(3)

url = WebUI.getUrl()

WebUI.executeJavaScript('window.open();', [])

currentWindow = WebUI.getWindowIndex()

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.navigateToUrl(url)

WebUI.delay(2)

WebUI.verifyTextPresent('Creative Briefs & Jobs', false)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(currentWindow)

WebUI.click(findTestObject('getcraft.io/root/btn_free consul'))

WebUI.delay(4)

WebUI.verifyTextPresent('Visit GetCraft', false)

WebUI.navigateToUrl('https://getcraft.com/briefs')

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/a_Managed'), 5)

WebUI.click(findTestObject('getcraft.io/root/a_Managed'))

WebUI.delay(4)

WebUI.verifyTextPresent('creative project manager', false)

WebUI.back(FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/root/a_education'))

WebUI.click(findTestObject('getcraft.io/root/a_crafters'))

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.delay(4)

WebUI.verifyTextPresent('CRAFTERS PREMIUM', false)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(currentWindow)

WebUI.click(findTestObject('getcraft.io/root/a_education'))

WebUI.click(findTestObject('getcraft.io/root/a_MarketingCraft'))

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.delay(4)

WebUI.verifyTextPresent('Visit GetCraft', false)

WebUI.closeBrowser()

