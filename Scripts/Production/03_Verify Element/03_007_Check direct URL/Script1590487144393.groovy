import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getcraft.com/marketplace')

WebUI.maximizeWindow()

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/button_Sign In'), 10)

WebUI.navigateToUrl('https://getcraft.com/marketplace/search')

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/button_Sign In'), 10)

WebUI.navigateToUrl('https://getcraft.com/network')

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/button_Sign In'), 10)

WebUI.navigateToUrl('https://getcraft.com/search')

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/button_Sign In'), 10)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.creator_email)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.creator_password)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl('https://getcraft.com/marketplace')

WebUI.maximizeWindow()

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.navigateToUrl('https://getcraft.com/marketplace/search')

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.navigateToUrl('https://getcraft.com/network')

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.navigateToUrl('https://getcraft.com/search')

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.closeBrowser()

