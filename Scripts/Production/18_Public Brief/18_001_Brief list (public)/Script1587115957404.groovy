import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getcraft.com')

WebUI.maximizeWindow()

WebUI.delay(5)

WebUI.click(findTestObject('getcraft.io/project/public brief/a_entryPublicBrief'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_Public Brief'), 5)

WebUI.getText(findTestObject('getcraft.io/project/public brief/btn_Public Brief'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_Schedule Consul'), 5)

WebUI.getText(findTestObject('getcraft.io/project/public brief/btn_Schedule Consul'))

WebUI.click(findTestObject('getcraft.io/project/public brief/btn_Schedule Consul'))

'Switch to Main window'
WebUI.switchToWindowIndex(0)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_All Categories'), 5)

WebUI.getText(findTestObject('getcraft.io/project/public brief/btn_All Categories'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_All Countries'), 5)

WebUI.getText(findTestObject('getcraft.io/project/public brief/btn_All Countries'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/btn_Apply'), 5)

WebUI.getText(findTestObject('getcraft.io/project/public brief/btn_Apply'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/div_Date Created'), 5)

WebUI.getText(findTestObject('getcraft.io/project/public brief/div_Date Created'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/div_Project Detail'), 5)

String rawDetail = WebUI.getText(findTestObject('getcraft.io/project/public brief/div_Project Detail'))

String projectTitle = rawDetail.split('\\n')[0]

String projectCat = rawDetail.split('\\n')[1]

String projectPrice = rawDetail.split('\\n')[2]

System.out.println(projectTitle)

System.out.println(projectCat)

System.out.println(projectPrice)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/public brief/div_Creator'), 5)

//WebUI.closeBrowser()

