import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
//random string or number
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
import org.openqa.selenium.Keys as Keys
//get project dir
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getcraft.com')

WebUI.maximizeWindow()

WebUI.delay(5)

WebUI.click(findTestObject('getcraft.io/project/public brief/a_entryPublicBrief'))

WebUI.delay(5)

WebUI.click(findTestObject('getcraft.io/project/public brief/div_firstPB'))

WebUI.delay(5)

WebUI.click(findTestObject('getcraft.io/project/public brief/btn_Apply'))


'Verify directly to sign-up creator page'
def URLnya = WebUI.getUrl()


'Verify URL have ref=public-brief'
if(URLnya.contains('?ref=public-brief')){
	KeywordUtil.markPassed(URLnya)
}

WebUI.closeBrowser()

