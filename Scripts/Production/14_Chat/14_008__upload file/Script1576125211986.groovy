import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'firstlater@yahoo.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/cancelled_label'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/Chat/Chat_1st'), 15)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))

def AssetsDir = RunConfiguration.getProjectDir() + '/Assets'

def file = '0.jpg'

WebUI.uploadFile(findTestObject('getcraft.io/project/Chat/span_upload file'), (AssetsDir + '/') + file)

WebUI.click(findTestObject('getcraft.io/project/Chat/button_send upload'))

WebUI.waitForElementNotVisible(findTestObject('getcraft.io/project/Chat/label_prosesUpload'), 10)

WebUI.closeBrowser()

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'firstlater@yahoo.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/project/cancelled_label'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/Chat/Chat_1st'), 15)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))

WebUI.delay(2)

CustomKeywords.'IsStaging.clickObjectFromList'('//img[@alt=\'none\']', -1)

fileName = WebUI.getText(findTestObject('getcraft.io/project/Chat/label_media_share_fileName'))

WebUI.verifyMatch(fileName, file, false)

WebUI.waitForElementClickable(findTestObject('getcraft.io/project/Chat/btn_back_imagePreview'), 30)

WebUI.click(findTestObject('getcraft.io/project/Chat/btn_back_imagePreview'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

AssetsDir = (RunConfiguration.getProjectDir() + '/Assets')

file = '1.jpg'

WebUI.waitForElementClickable(findTestObject('getcraft.io/project/Chat/span_upload file'), 10)

WebUI.uploadFile(findTestObject('getcraft.io/project/Chat/span_upload file'), (AssetsDir + '/') + file)

WebUI.click(findTestObject('getcraft.io/project/Chat/button_send upload'))

WebUI.waitForElementNotVisible(findTestObject('getcraft.io/project/Chat/label_prosesUpload'), 10)

WebUI.closeBrowser()

