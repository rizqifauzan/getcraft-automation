import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'firstlater@yahoo.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

not_run: WebUI.click(findTestObject('getcraft.io/root/a_my project'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

String xpath = '//span[(text() = \'Cancelled\' or . = \'Cancelled\')]'

CustomKeywords.'IsStaging.clickObjectFromList'(xpath, -1)

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/Chat/Chat_1st'), 15)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))

def chatMessage = 'فوزان'

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/span_chat box'), chatMessage)

WebUI.click(findTestObject('getcraft.io/project/Chat/button_send'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

CustomKeywords.'IsStaging.verifyObjectFromList'('//p[contains(@class, \'Chat__contentChat\')]', -1, chatMessage)

chatMessage = 'こんにちは'

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/input_chat box'), chatMessage)

WebUI.click(findTestObject('getcraft.io/project/Chat/button_send'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

CustomKeywords.'IsStaging.verifyObjectFromList'('//p[contains(@class, \'Chat__contentChat\')]', -1, chatMessage)

chatMessage = 'ꦗꦫꦤ꧀ꦝꦮꦸꦏ꧀'

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/input_chat box'), chatMessage)

WebUI.click(findTestObject('getcraft.io/project/Chat/button_send'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.verifyObjectFromList'('//p[contains(@class, \'Chat__contentChat\')]', -1, chatMessage)

chatMessage = 'доброе утро'

WebUI.sendKeys(findTestObject('getcraft.io/project/Chat/input_chat box'), chatMessage)

WebUI.click(findTestObject('getcraft.io/project/Chat/button_send'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.verifyObjectFromList'('//p[contains(@class, \'Chat__contentChat\')]', -1, chatMessage)

WebUI.closeBrowser()

