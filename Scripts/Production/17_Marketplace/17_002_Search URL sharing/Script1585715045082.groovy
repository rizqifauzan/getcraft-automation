import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://getcraft.com/search')

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Catagory'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_videographer'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/01. Temp/search_box'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_SubCatagory'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_videoEditing'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/01. Temp/search_box'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Expertise'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_travel'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/01. Temp/search_box'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Business type'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_Individual'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/01. Temp/search_box'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_ServiceLanguage'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_BahasaIndo'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/01. Temp/search_box'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Location'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_Location Indo'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/01. Temp/search_box'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

String URLnya1 = WebUI.getUrl()

WebUI.executeJavaScript('window.open();', [])

int tabSatu = WebUI.getWindowIndex()

WebUI.switchToWindowIndex(tabSatu + 1)

WebUI.navigateToUrl(URLnya1)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.verifyTextPresent('hertrinurpamungkas', false)

String URLnya2 = WebUI.getUrl()

if (URLnya1 == URLnya2) {
    WebUI.closeBrowser()
} else {
    KeywordUtil.markError(URLnya1)

    KeywordUtil.markError(URLnya2)
}

