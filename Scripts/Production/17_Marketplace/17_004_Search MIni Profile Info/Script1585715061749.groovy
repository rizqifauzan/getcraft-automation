import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Catagory'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_videographer'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/01. Temp/search_box'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_SubCatagory'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_videoEditing'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/01. Temp/search_box'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/div_first Creator'), 5)

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/marketplace/div_first Creator'))

WebUI.getText(findTestObject('getcraft.io/marketplace/div_View More'))

WebUI.click(findTestObject('getcraft.io/marketplace/div_View More'))

WebUI.verifyTextPresent('Business Type', false)

WebUI.verifyTextPresent('About', false)

WebUI.verifyTextPresent('Language', false)

WebUI.verifyTextPresent('Industry expertise', false)

WebUI.verifyTextPresent('View less', false)

WebUI.click(findTestObject('getcraft.io/marketplace/div_View Less'))

WebUI.verifyTextPresent('View Profile', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/btn_addMoreCreator'), 3)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/cart_enable'), 3)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/button_share profile'), 3)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/close_mini profile'), 3)

WebUI.click(findTestObject('getcraft.io/marketplace/close_mini profile'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/marketplace/close_mini profile'), 3)

not_run: WebUI.getText(findTestObject('getcraft.io/marketplace/div_workspace type'))

not_run: WebUI.getText(findTestObject('getcraft.io/marketplace/div_Profile Desc'))

not_run: WebUI.getText(findTestObject('getcraft.io/marketplace/div_Language'))

not_run: WebUI.getText(findTestObject('getcraft.io/marketplace/div_Expertise'))

not_run: WebUI.getText(findTestObject('getcraft.io/marketplace/div_View Less'))

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

