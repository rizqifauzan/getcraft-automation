import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Sign up here'))

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_firsrname'), 'Julo')

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_lastname'), '9')

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_Email'), 'creator01@mailnesia.com')

WebUI.setEncryptedText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input Password'), 'K1i+QyEnEuNjXkAzQfX8hA==')

WebUI.clickOffset(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_checkbox'), 0, 0)

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Continue button'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/email used'), 5)

not_run: WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Sign up here'))

not_run: WebUI.click(findTestObject('getcraft.io/sign-up/close_popup'))

WebUI.refresh()

not_run: WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

not_run: WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Sign up here'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_firsrname'), 
    30)

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_firsrname'), 'Julo')

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_lastname'), '9')

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_Email'), 'CREATOR01@mailnesia.com')

WebUI.setEncryptedText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input Password'), 'K1i+QyEnEuNjXkAzQfX8hA==')

WebUI.clickOffset(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_checkbox'), 0, 0)

WebUI.clearText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_Email'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Continue button'))

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/email used'), 5)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

