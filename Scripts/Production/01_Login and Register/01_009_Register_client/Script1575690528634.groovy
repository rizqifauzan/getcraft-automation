import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Sign up here'))

//generate random integer
String Random

Random = ((Math.random() * 999) as int)

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_firsrname'), Client)

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_Email'), (Client + Random) + 
    '@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_lastname'), Random)

WebUI.setEncryptedText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input Password'), 'K1i+QyEnEuNjXkAzQfX8hA==')

WebUI.clickOffset(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_checkbox'), 0, 0)

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Continue button'))

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Select_Country'))

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Insert_poland'), 'poland')

not_run: WebUI.takeScreenshot()

WebUI.clickOffset(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/div_Poland'), 0, 15)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_number'), '732121377')

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Button_Request a code'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/verify_later'), 
    20)

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/verify_later'), FailureHandling.STOP_ON_FAILURE)

//Go in to new tab
not_run: WebUI.executeJavaScript('window.open();', [])

not_run: currentWindow = WebUI.getWindowIndex()

not_run: WebUI.switchToWindowIndex(currentWindow + 1)

not_run: WebUI.navigateToUrl('https://receive-smss.com/sms/48732121377/')

not_run: OTP = WebUI.getText(findTestObject('receive-sms/Get-OTP'))

//Comming back
not_run: WebUI.switchToWindowIndex(currentWindow)

not_run: WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_OTP'), OTP)

WebUI.delay(3)

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/select_clientbox'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/register/Page_Create New Workspace/div_continue'), 15)

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/div_continue'))

WebUI.delay(3)

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/div_continue'))

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/div_LetsStart'))

not_run: WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/div_next'))

not_run: WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/div_gotIt'))

WebUI.setText(findTestObject('getcraft.io/register/Page_Create New Workspace/input__profileName'), Client + Random)

WebUI.uploadFile(findTestObject('getcraft.io/register/Page_Create New Workspace/upload_profile'), GlobalVariable.imagePath2)

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/save_workspace'))

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/select_individual'))

width = WebUI.getPageWidth()

height = WebUI.getPageHeight()

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/select_city'))

WebUI.setText(findTestObject('getcraft.io/register/Page_Create New Workspace/input_cityname'), 'jaka')

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/select_jakarta'))

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/span_Continue'))

WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/Label_ManageProject'), 10)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Label_ManageProject'), 10)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

