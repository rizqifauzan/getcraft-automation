import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Sign up here'))

//generate random integer
String Random

Random = ((Math.random() * 999) as int)

'Input firsrname only'
WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_firsrname'), Client)

WebUI.clickOffset(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_checkbox'), 0, 0)

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Continue button'))

'Input firsrname and lastname'
WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_lastname'), Random)

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Continue button'))

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_Email'), (Client + Random) + 
    '@mailnesia.com')

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Continue button'))

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input Password'), 'get')

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Continue button'))

WebUI.delay(2)

'alert password'
WebUI.verifyTextPresent('Password must consist of 6 characters or more', false)

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input Password'), 'craft')

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Continue button'))

WebUI.delay(2)

'alert password'
WebUI.verifyTextPresent('Password must contain at least one letter and one number', false)

not_run: WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input Password'), '1234')

not_run: WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Continue button'))

not_run: WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Select_Country'))

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

