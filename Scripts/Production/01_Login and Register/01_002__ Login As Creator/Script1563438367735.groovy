import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.creator_email)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.creator_password)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.delay(2)

//WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))
WebUI.navigateToUrl(GlobalVariable.Prod_URL + 'personal-setting')

//WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))
//WebUI.click(findTestObject('getcraft.io/root/a_Account Setting'))
WebUI.verifyElementAttributeValue(findTestObject('getcraft.io/account-setting/input_FirstName'), 'value', 'Rahmat', 0)

