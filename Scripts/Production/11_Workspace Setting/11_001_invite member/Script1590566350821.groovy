import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'topi5@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.delay(2)

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('getcraft.io/kebab-menu/btn_manage_team'))

WebUI.setText(findTestObject('getcraft.io/setting/input_email'), 'topi2@mailnesia.com')

WebUI.click(findTestObject('getcraft.io/setting/send_email'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/setting/send_email'))

WebUI.setText(findTestObject('getcraft.io/setting/input_email'), 'topi2@mailnesia.com')

WebUI.click(findTestObject('getcraft.io/setting/send_email'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/setting/send_email'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/kebab-menu/Whoops alert'), 5)

WebUI.verifyElementPresent(findTestObject('getcraft.io/setting/verify_invited'), 10)

not_run: WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('getcraft.io/root/a_sign out'))

WebUI.navigateToUrl('https://mailnesia.com/')

WebUI.setText(findTestObject('Mailnesia/input_email'), 'topi2')

WebUI.delay(3)

WebUI.click(findTestObject('Mailnesia/Go'))

WebUI.click(findTestObject('Mailnesia/open mail'))

WebUI.verifyElementVisible(findTestObject('Mailnesia/Join team'))

WebUI.click(findTestObject('Mailnesia/Join team'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'topi2@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

//cek element allow notif
if (WebUI.verifyElementPresent(findTestObject('getcraft.io/account-setting/allow_notif'), 5, FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))
} else {
    WebUI.delay(2)
}

text = WebUI.getText(findTestObject('GETCRAFT/general/workspace_name'))

not_run: WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.delay(3)

WebUI.click(findTestObject('GETCRAFT/general/select_workspace'))

WebUI.delay(5)

text2 = WebUI.getText(findTestObject('GETCRAFT/general/workspace_name'))

WebUI.verifyNotEqual(text, text2)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

