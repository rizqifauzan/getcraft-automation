import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

/*String status2 = GlobalVariable.TC_Status2

if ((status2 == 'FAILED') || (status2 == 'ERROR')) {
    not_run: KeywordUtil.markErrorAndStop('---->|')
} else {*/
WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'topi5@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('getcraft.io/kebab-menu/btn_manage_team'))

WebUI.setText(findTestObject('getcraft.io/setting/input_email'), 'topi2@mailnesia.com')

WebUI.click(findTestObject('getcraft.io/setting/send_email'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/setting/send_email'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/kebab-menu/Whoops alert2'), 5)

WebUI.click(findTestObject('getcraft.io/setting/button_invoke'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/setting/span_Submit'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/setting/button_ReInvite'), 7)

WebUI.verifyTextPresent('Re - Invite', false)

not_run: WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('getcraft.io/root/a_sign out'))

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'topi2@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/icon_user-menu'), 15)

not_run: WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.click(findTestObject('getcraft.io/root/icon_expand more'))

WebUI.verifyElementNotPresent(findTestObject('GETCRAFT/general/select_workspace'), 10)

WebUI.delay(3)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

