import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.Cookie as Cookie
import org.openqa.selenium.WebDriver as WebDriver

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Admin_URL)

WebUI.maximizeWindow()

Cookie token = new Cookie('token', GlobalVariable.token)

Cookie user = new Cookie('userId', GlobalVariable.user)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().addCookie(token)

driver.manage().addCookie(user)

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('admin panel/admin/Nav Menu/nav_service-need-review'))

WebUI.delay(3)

WebUI.click(findTestObject('admin panel/admin/projects/View SuperUser'))

WebUI.switchToWindowIndex(1)

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/a_browse creator'), 15)

if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'))
} else {
    WebUI.delay(2)
}

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/a_browse creator'), 3)

WebUI.verifyElementPresent(findTestObject('admin panel/admin/Service_Need Review/SU_Approve'), 3)

WebUI.verifyElementPresent(findTestObject('admin panel/admin/Service_Need Review/SU_Reject'), 3)

WebUI.closeBrowser()

