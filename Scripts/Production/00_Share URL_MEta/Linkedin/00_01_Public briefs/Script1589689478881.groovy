import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.linkedin.com')

WebUI.maximizeWindow()

WebUI.click(findTestObject('MetaURL/Linkedin/Bt-Login'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('MetaURL/Linkedin/Insert_Email'), 'firstlater@gmail.com')

WebUI.setEncryptedText(findTestObject('MetaURL/Linkedin/Insert_Password'), 'P7VBu69HyMH7Odkf+euWtw==')

WebUI.click(findTestObject('MetaURL/Linkedin/Bt-SIgnIn'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl('https://www.linkedin.com/post-inspector/inspect/')

WebUI.setText(findTestObject('MetaURL/Linkedin/Insert_URL'), 'https://getcraft.com/briefs')

WebUI.click(findTestObject('MetaURL/Linkedin/Bt-Inspect'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('MetaURL/Linkedin/label-Info'), 10)

'og:title'
WebUI.verifyTextPresent('Creative Briefs & Jobs | GetCraft', false)

'og:description'
WebUI.verifyTextPresent('GetCraft is a premium creative network. It\'s the best place to connect with creative professionals and businesses looking for projects or work.', 
    false)

'og:image'
WebUI.verifyTextPresent('https://assets.getcraft.com/images/public-brief-getcraft.png', false)

'Warning error'
WebUI.verifyTextNotPresent('Missing Properties', false)

WebUI.navigateToUrl('https://www.linkedin.com')

WebUI.waitForElementPresent(findTestObject('MetaURL/Linkedin/icon_profile'), 7)

WebUI.click(findTestObject('MetaURL/Linkedin/icon_profile'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('MetaURL/Linkedin/sigout-linkedin'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('MetaURL/Linkedin/Bt-Login'), 7)

WebUI.closeBrowser()

