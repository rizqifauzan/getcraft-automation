import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://developers.facebook.com/tools/debug')

WebUI.maximizeWindow()

WebUI.click(findTestObject('MetaURL/Facebook/Bt_login'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('MetaURL/Facebook/input_email'), 'firstlater@gmail.com')

WebUI.setText(findTestObject('MetaURL/Facebook/input_password'), 'getcraft1234')

WebUI.click(findTestObject('MetaURL/Facebook/Submit_login'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('MetaURL/Facebook/input_URL'), 5)

WebUI.setText(findTestObject('MetaURL/Facebook/input_URL'), 'https://getcraft.com/arieltatum/instagram-sponsored-content-arieltatum-2')

WebUI.click(findTestObject('MetaURL/Facebook/Bt_Debug'), FailureHandling.STOP_ON_FAILURE)

'og:title'
WebUI.verifyTextPresent('Ariel Tatum - Instagram Sponsored Content Arieltatum | GetCraft', false)

'og:description'
WebUI.verifyTextPresent('Indonesia Celebrities & Influencers. Hire Ariel Tatum on GetCraft. Ariel Tatum adalah seorang Aktris tanah air yang mengawali karir sebagai bintang iklan dan ju...', 
    false)

'og:image'
WebUI.verifyTextPresent('https://gc-media-bucket.s3.amazonaws.com/creator/2020/raw/1587039837619279572.jpg', false)

WebUI.verifyTextNotPresent('Missing Properties', false)

WebUI.closeBrowser()

