import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.Cookie as Cookie
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Admin_URL)

WebUI.maximizeWindow()

Cookie token = new Cookie('token', GlobalVariable.token)

Cookie user = new Cookie('userId', GlobalVariable.user)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().addCookie(token)

driver.manage().addCookie(user)

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('admin panel/admin/Nav Menu/nav_service-need-review'))

WebUI.click(findTestObject('admin panel/admin/Service_Need Review/a_newest_service'))

WebUI.waitForElementPresent(findTestObject('admin panel/admin/Service_Need Review/radio_reject'), 10)

WebUI.click(findTestObject('admin panel/admin/Service_Need Review/radio_reject'))

WebUI.setText(findTestObject('admin panel/admin/Service_Need Review/reject_reason'), 'Service kamu JELEK')

WebUI.click(findTestObject('admin panel/admin/Service_Need Review/button_submit'))

WebUI.delay(9, FailureHandling.STOP_ON_FAILURE)

product = WebUI.getText(findTestObject('admin panel/admin/Service_Need Review/product_name'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('admin panel/admin/Nav Menu/nav_published-services'))

WebUI.delay(5)

WebUI.verifyTextNotPresent(product, false)

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/Searchbox_Homepage'), 10)

WebUI.setText(findTestObject('getcraft.io/root/Searchbox_Homepage'), product)

WebUI.sendKeys(findTestObject('getcraft.io/root/Searchbox_Homepage'), Keys.chord(Keys.ENTER))

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('No results found', false)

not_run: WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'))

not_run: WebUI.verifyTextNotPresent(product, false)

WebUI.closeBrowser()

