import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.RenderingHints.Key

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.Cookie as Cookie
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Admin_URL)

WebUI.maximizeWindow()

Cookie token = new Cookie('token', GlobalVariable.token)

Cookie user = new Cookie('userId', GlobalVariable.user)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().addCookie(token)

driver.manage().addCookie(user)

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('admin panel/admin/Nav Menu/nav_project'))

WebUI.delay(3)

WebUI.setText(findTestObject('admin panel/admin/projects/txt_projectName'), GlobalVariable.project_name)

WebUI.sendKeys(findTestObject('admin panel/admin/projects/txt_projectName'), Keys.chord(Keys.ENTER))
//WebUI.click(findTestObject('admin panel/admin/projects/btn_applyFilter'))

WebUI.delay(2)

WebUI.click(findTestObject('admin panel/admin/projects/button_eyes'))

WebUI.click(findTestObject('admin panel/admin/projects/button_complete project'))

WebUI.click(findTestObject('admin panel/admin/projects/button_complete project popup'))

WebUI.delay(3)

statusnya = WebUI.getText(findTestObject('admin panel/admin/projects/verify_status'))

WebUI.verifyMatch(statusnya, 'Payment Needed', false)

WebUI.closeBrowser()

