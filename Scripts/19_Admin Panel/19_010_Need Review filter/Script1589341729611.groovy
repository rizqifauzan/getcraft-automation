import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.Cookie as Cookie
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Admin_URL)

WebUI.maximizeWindow()

Cookie token = new Cookie('token', GlobalVariable.token)

Cookie user = new Cookie('userId', GlobalVariable.user)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().addCookie(token)

driver.manage().addCookie(user)

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('admin panel/admin/Nav Menu/nav_service-need-review'))

WebUI.setText(findTestObject('admin panel/admin/Service_Need Review/filter/input_projectName'), 'Fauzan')

WebUI.click(findTestObject('admin panel/admin/Service_Need Review/filter/btn_apply filter'))

WebUI.verifyElementText(findTestObject('admin panel/admin/Service_Need Review/filter/first_profileId'), 'fauzanapi')

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('admin panel/admin/Service_Need Review/filter/select_country'), 10)

WebUI.selectOptionByLabel(findTestObject('admin panel/admin/Service_Need Review/filter/select_country'), 'Singapore', false)

WebUI.click(findTestObject('admin panel/admin/Service_Need Review/filter/btn_apply filter'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('admin panel/admin/Service_Need Review/filter/first_country'), 10)

WebUI.verifyElementText(findTestObject('admin panel/admin/Service_Need Review/filter/first_country'), 'Singapore')

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('admin panel/admin/Service_Need Review/filter/select_category'), 0, FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByLabel(findTestObject('admin panel/admin/Service_Need Review/filter/select_category'), 'Photographers', 
    false)

WebUI.click(findTestObject('admin panel/admin/Service_Need Review/filter/btn_apply filter'))

WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('admin panel/admin/Service_Need Review/filter/first_serviceCategory'), 0)

WebUI.verifyElementText(findTestObject('admin panel/admin/Service_Need Review/filter/first_serviceCategory'), 'Photographers')

WebUI.closeBrowser()

