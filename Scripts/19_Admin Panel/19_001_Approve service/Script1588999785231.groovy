import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.Cookie as Cookie
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Admin_URL)

WebUI.maximizeWindow()

Cookie token = new Cookie('token', GlobalVariable.token)

Cookie user = new Cookie('userId', GlobalVariable.user)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().addCookie(token)

driver.manage().addCookie(user)

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('admin panel/admin/Nav Menu/nav_service-need-review'))

WebUI.click(findTestObject('admin panel/admin/Service_Need Review/a_newest_service'))

WebUI.waitForElementPresent(findTestObject('admin panel/admin/Service_Need Review/radio_approve'), 10)

WebUI.click(findTestObject('admin panel/admin/Service_Need Review/radio_approve'))

WebUI.click(findTestObject('admin panel/admin/Service_Need Review/button_submit'))

WebUI.delay(5)

product = WebUI.getText(findTestObject('admin panel/admin/Service_Need Review/product_name'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('admin panel/admin/Nav Menu/nav_published-services'))

WebUI.verifyTextPresent(product, false)

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.waitForElementPresent(findTestObject('getcraft.io/root/Searchbox_Homepage'), 10)

WebUI.setText(findTestObject('getcraft.io/root/Searchbox_Homepage'), product)

WebUI.sendKeys(findTestObject('getcraft.io/root/Searchbox_Homepage'), Keys.chord(Keys.ENTER))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 15)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent(product, false)

WebUI.closeBrowser()

