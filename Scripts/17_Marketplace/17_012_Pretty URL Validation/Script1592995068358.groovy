import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://beta.getcraft.com')

WebUI.maximizeWindow()

WebUI.delay(2)

String rootUrl = 'https://beta.getcraft.com/en/'

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Catagory'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_celebrities'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/search_box'))

'Get URL, Substring & Verify Category'
urlnya1 = WebUI.getUrl()
urlCategory='/celebrities-influencers'
if(urlnya1.contains(urlCategory)) {KeywordUtil.markPassed(urlnya1)}
urlResult = rootUrl+urlCategory



WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)
			
WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_SubCatagory'))
			
WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_InstagramPhoto'))
			
WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/search_box'))
			
'Get URL, Substring & Verify Service'
urlnya2 = WebUI.getUrl()
urlService='/instagram-photo'
if(urlnya2.contains(urlService)) {KeywordUtil.markPassed(urlnya2)}
urlResult = rootUrl+urlCategory+urlService



WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Expertise'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_gadgetGameTech'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/search_box'))

'Get URL, Substring & Verify Expertise'
urlnya3 = WebUI.getUrl()
urlExpertise='/gadget-games-technology'
if(urlnya3.contains(urlExpertise)) {KeywordUtil.markPassed(urlnya3)}
urlResult = rootUrl+urlCategory+urlService+urlExpertise



WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Location'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_Location Philippines'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/search_box'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

'Get URL, Substring ambil philipinnes & Verify Location'
urlnya4 = WebUI.getUrl()
urlLocation='philippines'
if(urlnya4.contains(urlLocation)) {KeywordUtil.markPassed(urlnya4)}
urlResult = rootUrl+urlLocation+urlCategory+urlService+urlExpertise


'Verify URL lengkapnya'
WebUI.verifyMatch(urlnya4, urlResult, false)



//WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)
//
//WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Business type'))
//
//WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_Individual'))
//
//WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/search_box'))
//
//WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)
//





	//WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_ServiceLanguage'))
	//
	//WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_English'))
	//
	//WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/search_box'))
	//
	//'Get URL, Substring & Verify Language'
	//urlnya4 = WebUI.getUrl()
	//int lastSlashIndex4 = urlnya4.lastIndexOf("/") // get a substring after last "/" to the end
	//String urlLanguage = urlnya4.substring(lastSlashIndex4 + 1)
	//urlResult = rootUrl+urlCategory+'/'+urlService+'/'+urlExpertise+'/'+urlLanguage



//WebUI.closeBrowser()

