import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://beta.getcraft.com')

WebUI.maximizeWindow()


WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'betacli@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'Pass4312')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByLabel(findTestObject('getcraft.io/project/select_project status'), '  Invitation Sent', false)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/Label_InvitationSent'), 5)

WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

WebUI.click(findTestObject('getcraft.io/project/Chat/Button_addCreator'))

urldariChat = WebUI.getUrl()

if(urldariChat.contains("/en")) {KeywordUtil.markPassed(urldariChat)}

WebUI.back()

WebUI.click(findTestObject('getcraft.io/project/order/tab_brief'))

String projectExisting = WebUI.getText(findTestObject('getcraft.io/project/verify_projectName'))

WebUI.click(findTestObject('getcraft.io/project/order/btn_addMoreCreators'))

urldariBrief =  WebUI.getUrl()

WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/div_firstProfileName'), 5)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/div_firstProfileName'), 5)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/div_firstProfileName'), 5)

namaCreator =  WebUI.getText(findTestObject('getcraft.io/marketplace/div_firstProfileName'))

WebUI.click(findTestObject('getcraft.io/marketplace/result_firstProfile'))

WebUI.click(findTestObject('getcraft.io/marketplace/btn_addToExisting Project'))

WebUI.setText(findTestObject('getcraft.io/marketplace/input_addSearch Project'), projectExisting)

WebUI.click(findTestObject('getcraft.io/marketplace/box_ProjectExisting'))

WebUI.click(findTestObject('getcraft.io/marketplace/add_existingProject'))

WebUI.delay(3)

WebUI.click(findTestObject('getcraft.io/marketplace/View Project'))

labelChat = WebUI.getText(findTestObject('getcraft.io/project/Chat/label_chatCreatorName'))

WebUI.verifyMatch(namaCreator,labelChat, false )

WebUI.closeBrowser()

