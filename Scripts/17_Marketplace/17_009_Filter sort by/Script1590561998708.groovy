import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 25)

Relevance = WebUI.getText(findTestObject('getcraft.io/marketplace/NewSearch/select service'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/MP-sort'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/MP-sort-Newest'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

Newest = WebUI.getText(findTestObject('getcraft.io/marketplace/NewSearch/select service'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyNotEqual(Relevance, Newest)

WebUI.click(findTestObject('getcraft.io/marketplace/MP-sort-Oldest'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

Oldest = WebUI.getText(findTestObject('getcraft.io/marketplace/NewSearch/select service'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.verifyNotEqual(Newest, Oldest)

WebUI.click(findTestObject('getcraft.io/marketplace/MP-sort-Highest'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

Highest = WebUI.getText(findTestObject('getcraft.io/marketplace/NewSearch/select service'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyNotEqual(Oldest, Highest)

WebUI.click(findTestObject('getcraft.io/marketplace/MP-sort-Lowest'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

Lowest = WebUI.getText(findTestObject('getcraft.io/marketplace/NewSearch/select service'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyNotEqual(Highest, Lowest)

not_run: WebUI.click(findTestObject('getcraft.io/marketplace/MP-sort-Relevance'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

not_run: Relevance2 = WebUI.getText(findTestObject('getcraft.io/marketplace/NewSearch/select service'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.verifyNotEqual(Relevance, Relevance2)

WebUI.closeBrowser()

