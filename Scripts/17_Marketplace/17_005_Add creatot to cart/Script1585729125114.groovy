import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'topi@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/root/a_browse vendor'))

WebUI.click(findTestObject('getcraft.io/marketplace/a_Videographers'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 15)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), 'Dimas Aryana')

WebUI.sendKeys(findTestObject('getcraft.io/marketplace/NewSearch/search_box'), Keys.chord(Keys.ENTER))

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 25)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'))

WebUI.click(findTestObject('getcraft.io/marketplace/cart_enable'))

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/marketplace/NewSearch/Tick ServiceCard'), 3)

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/cart_disable'), 3)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/preview'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/Bt_create brief'), 3)

WebUI.closeBrowser()

