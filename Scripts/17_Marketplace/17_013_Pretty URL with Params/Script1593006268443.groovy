import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://beta.getcraft.com')

WebUI.maximizeWindow()

WebUI.delay(2)

String rootUrl = 'https://beta.getcraft.com/en/'

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Catagory'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_marketingSpecialists'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/search_box'))

'Get URL, Substring & Verify Category'
urlnya1 = WebUI.getUrl()

int lastSlashIndex1 = urlnya1.lastIndexOf("/") 	// get a substring setelah tanda "/"
String urlCategory = urlnya1.substring(lastSlashIndex1 + 1)
WebUI.verifyMatch(urlCategory, 'marketing-specialists', false) 
urlResult = rootUrl+urlCategory



WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Business type'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_Company'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/search_box'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

'Get URL, Substring & Verify BusinessType'
urlnya2 = WebUI.getUrl()
int lastSlashIndex2 = urlnya2.lastIndexOf("?") 	// get a substring setelah tanda "?"
String urlBusinessType = urlnya2.substring(lastSlashIndex2 + 1)
WebUI.verifyMatch(urlBusinessType, 'businessType=Company%20Workspace', false)
urlResult = rootUrl+urlCategory+'?'+urlBusinessType



WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Price'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Input_PriceMin'))

WebUI.sendKeys(findTestObject('getcraft.io/marketplace/NewSearch/Input_PriceMin'), '500,000')  

WebUI.delay(5)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Input_PriceMax'))

WebUI.sendKeys(findTestObject('getcraft.io/marketplace/NewSearch/Input_PriceMax'), ' 3,000,000') 

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/search_box'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)


'Get URL, Substring & Verify Price'
urlnya3 = WebUI.getUrl()
int lastSlashIndex3 = urlnya3.lastIndexOf("&") 	// get a substring setelah tanda "&"
String urlPrice = urlnya3.substring(lastSlashIndex3 + 1)
WebUI.verifyMatch(urlPrice, 'priceUsd=500000%2C3000000', false)
urlResult = rootUrl+urlCategory+'?'+urlBusinessType+'&'+urlPrice



WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_ServiceLanguage'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_English'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/search_box'))

'Get URL, Substring & Verify Language'
urlnya4 = WebUI.getUrl()
int lastSlashIndex4 = urlnya4.lastIndexOf("&") 	// get a substring setelah tanda "&" yang ke 2
String urlLanguage = urlnya4.substring((lastSlashIndex4-1) + 2)
urlResult = rootUrl+urlCategory+'?'+urlBusinessType+'&'+urlPrice+'&'+urlLanguage


'Verify URL lengkapnya'
WebUI.verifyMatch(urlnya4, urlResult, false)


WebUI.closeBrowser()

