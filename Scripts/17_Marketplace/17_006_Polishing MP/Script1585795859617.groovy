import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://beta.getcraft.com/search')

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/Bt_Catagory'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/checkbox_celebrities'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/search_box'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/div_first Creator'), 5)

WebUI.click(findTestObject('getcraft.io/marketplace/div_first Creator'))

WebUI.delay(5)

WebUI.click(findTestObject('getcraft.io/marketplace/div_Creator Clicked'))

borderGrey = WebUI.getCSSValue(findTestObject('getcraft.io/marketplace/div_Creator Clicked'), 'border-color')

'Verify Card border is Grey'
WebUI.verifyMatch(borderGrey, 'rgb(230, 230, 230)', false)

WebUI.verifyElementClickable(findTestObject('getcraft.io/marketplace/NewSearch/div_First Service'))

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/div_First Service'))

'Verify Brief case button is Present'
WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/btn_Brief Case'), 5)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/btn_Brief Case'))

borderOrange = WebUI.getCSSValue(findTestObject('getcraft.io/marketplace/div_Creator Clicked'), 'border-color')

briefCaseOrange = WebUI.getCSSValue(findTestObject('getcraft.io/marketplace/NewSearch/btn_Brief Case'),'background-color')

'Verify Card border is Orange'
WebUI.verifyMatch(borderGrey, 'rgb(241, 94, 69)', false)

'Verify Brief Case Background color is Orange'
WebUI.verifyMatch(briefCaseOrange, 'rgb(241, 93, 70)', false)


