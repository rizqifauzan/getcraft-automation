import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.waitForElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 25)

WebUI.setText(findTestObject('getcraft.io/root/Searchbox_Homepage'), 'kompas')

WebUI.sendKeys(findTestObject('getcraft.io/root/Searchbox_Homepage'), Keys.chord(Keys.ENTER))

WebUI.verifyElementPresent(findTestObject('getcraft.io/marketplace/NewSearch/select service'), 10)

WebUI.click(findTestObject('getcraft.io/marketplace/NewSearch/select service'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/bt_currency'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('IDR', false)

WebUI.verifyTextPresent('MYR', false)

WebUI.verifyTextPresent('IDR', false)

WebUI.verifyTextPresent('SGD', false)

WebUI.verifyTextPresent('USD', false)

WebUI.click(findTestObject('getcraft.io/marketplace/currency-MYR'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.verifyTextContain'(findTestObject('getcraft.io/marketplace/service_price'), 'MYR')

WebUI.mouseOver(findTestObject('getcraft.io/product/card title'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/btn_see details'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('MYR', false)

WebUI.click(findTestObject('getcraft.io/marketplace/btn_close servicepopup'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/bt_currency'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/currency-PHP'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.verifyTextContain'(findTestObject('getcraft.io/marketplace/service_price'), 'PHP')

WebUI.mouseOver(findTestObject('getcraft.io/product/card title'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/btn_see details'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('PHP', false)

WebUI.click(findTestObject('getcraft.io/marketplace/btn_close servicepopup'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/bt_currency'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/currency-SGD'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.verifyTextContain'(findTestObject('getcraft.io/marketplace/service_price'), 'SGD')

WebUI.mouseOver(findTestObject('getcraft.io/product/card title'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/btn_see details'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('SGD', false)

WebUI.click(findTestObject('getcraft.io/marketplace/btn_close servicepopup'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/bt_currency'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/currency-USD'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.verifyTextContain'(findTestObject('getcraft.io/marketplace/service_price'), 'USD')

WebUI.mouseOver(findTestObject('getcraft.io/product/card title'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/btn_see details'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('USD', false)

WebUI.click(findTestObject('getcraft.io/marketplace/btn_close servicepopup'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/bt_currency'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/currency-IDR'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'IsStaging.verifyTextContain'(findTestObject('getcraft.io/marketplace/service_price'), 'IDR')

WebUI.mouseOver(findTestObject('getcraft.io/product/card title'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('getcraft.io/marketplace/btn_see details'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('IDR', false)

WebUI.click(findTestObject('getcraft.io/marketplace/btn_close servicepopup'), FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

