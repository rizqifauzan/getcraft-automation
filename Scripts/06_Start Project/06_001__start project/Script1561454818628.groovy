import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

//WebUI.maximizeWindow()
not_run: WebUI.setViewPortSize(1640, 982)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.ClientEmail)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.ClientPasswd)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 30)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

WebUI.delay(3)

WebUI.click(findTestObject('getcraft.io/top-menu/a_Projects'))

WebUI.click(findTestObject('getcraft.io/project/a_newest project'))

WebUI.delay(3)

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/Chat/Chat_1st'), 15)

WebUI.click(findTestObject('getcraft.io/project/Chat/Chat_1st'))

CustomKeywords.'IsStaging.verifyObjectFromList'('//*[@id = \'cardTitle\']', -1, 'New Quote Submitted')

WebUI.click(findTestObject('getcraft.io/project/order/tab_qoute'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/div_On review quote'), 15)

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/project/div_On review quote'))

not_run: WebUI.click(findTestObject('getcraft.io/project/order/span_Select'))

not_run: WebUI.waitForElementPresent(findTestObject('getcraft.io/project/order/button_start project'), 5)

not_run: WebUI.click(findTestObject('getcraft.io/project/order/button_start project'))

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('getcraft.io/project/order/span_Start Project'))

not_run: WebUI.waitForElementPresent(findTestObject('getcraft.io/project/payment/bt_Approve this quote'), 20)

not_run: WebUI.delay(2)

not_run: QuoteGen = WebUI.getText(findTestObject('getcraft.io/project/order/verify_project status'))

//if (QuoteGen == 'Quote Generated') {
//    KeywordUtil.markPassed(QuoteGen)
//} else {
//    KeywordUtil.markFailed(QuoteGen)
//}
not_run: if (QuoteGen == 'Quote created') {
    not_run: KeywordUtil.markPassed(QuoteGen)
} else {
    KeywordUtil.markFailed(QuoteGen)
}

not_run: GlobalVariable.QuoteGenerated = QuoteGen

WebUI.closeBrowser()

