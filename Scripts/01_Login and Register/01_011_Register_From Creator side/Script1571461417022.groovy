import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/Join_As Creator'))

WebUI.click(findTestObject('getcraft.io/root/button_Sign-Creator'))

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Sign up here'))

//generate random integer
String Random

Random = ((Math.random() * 999) as int)

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_firsrname'), Creator)

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_Email'), (Creator + Random) + 
    '@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_lastname'), Random)

WebUI.setEncryptedText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input Password'), 'K1i+QyEnEuNjXkAzQfX8hA==')

WebUI.clickOffset(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_checkbox'), 0, 0)

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Continue button'))

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Select_Country'))

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Insert_poland'), 'poland')

WebUI.clickOffset(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/div_Poland'), 0, 20)

not_run: WebUI.takeScreenshot()

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_number'), '732121377')

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Button_Request a code'))

//Go in to new tab
not_run: WebUI.executeJavaScript('window.open();', [])

not_run: currentWindow = WebUI.getWindowIndex()

not_run: WebUI.switchToWindowIndex(currentWindow + 1)

not_run: WebUI.navigateToUrl('https://receive-smss.com/sms/48732121377/')

not_run: OTP = WebUI.getText(findTestObject('receive-sms/Get-OTP'))

//Comming back
not_run: WebUI.switchToWindowIndex(currentWindow)

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_OTP'), OTP)

WebUI.verifyElementText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/try again'), 'Try again')

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/Input_OTP'), OTP)

WebUI.waitForElementPresent(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/verify_later'), 
    20)

WebUI.click(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/verify_later'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.setText(findTestObject('getcraft.io/register/Page_Create New Workspace/input__profileName'), Creator + Random)

WebUI.uploadFile(findTestObject('getcraft.io/register/Page_Create New Workspace/upload_profile'), GlobalVariable.imagePath2)

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/save_workspace'))

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/select_individual'))

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/select_city'))

WebUI.setText(findTestObject('getcraft.io/register/Page_Create New Workspace/input_cityname'), 'jaka')

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/select_jakarta'))

WebUI.setText(findTestObject('getcraft.io/register/Page_Create New Workspace/input_occupations'), 'Eng')

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/select_QA'))

WebUI.setText(findTestObject('getcraft.io/register/Page_Create New Workspace/input_about'), 'ini adlah contoh about di dalam service ini adlah contoh about di dalam serviceini adlah contoh about di dalam service')

WebUI.setText(findTestObject('getcraft.io/register/Page_Create New Workspace/input_industry'), 'auto')

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/select_Automotive'))

WebUI.click(findTestObject('getcraft.io/register/Page_Create New Workspace/span_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/product/Header_AddService'), 10)

not_run: WebUI.verifyElementPresent(findTestObject('getcraft.io/product/Header_AddService'), 10)

WebUI.click(findTestObject('getcraft.io/root/a_browse creator'))

WebUI.delay(2)

WebUI.acceptAlert()

WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))

WebUI.click(findTestObject('GETCRAFT/login/present_myproject-WISP1'))

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_number'), 
    15)

WebUI.click(findTestObject('getcraft.io/root/homepage'))

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_number'), 
    15)

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/product/VIew_My_Profile'))

WebUI.verifyElementNotPresent(findTestObject('getcraft.io/register/Page_GetCraft - Premium Creative Network/input_number'), 
    15)

if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later - wisp 2'))
} else {
    WebUI.delay(2)
}

WebUI.click(findTestObject('getcraft.io/product/bt_edit profile'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/product/bt_deactivated account'), 10)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

