import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.click(findTestObject('getcraft.io/sign-in/button_Google'))

WebUI.delay(2)

WebUI.switchToWindowTitle('Masuk - Akun Google')

WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_email'), GlobalVariable.email_google)

WebUI.sendKeys(findTestObject('getcraft.io/sign-in/google/input_email'), Keys.chord(Keys.ENTER))

//WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))
WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_password'), GlobalVariable.passw_google)

WebUI.sendKeys(findTestObject('getcraft.io/sign-in/google/input_password'), Keys.chord(Keys.ENTER))

//WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))
WebUI.delay(8)

//if (WebUI.verifyElementVisible(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.OPTIONAL)) {
//    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))
//} else {
//    WebUI.delay(2)
//}
//
//WebUI.verifyElementPresent(findTestObject('getcraft.io/root/icon_user-menu'), 120)

WebUI.closeBrowser()

