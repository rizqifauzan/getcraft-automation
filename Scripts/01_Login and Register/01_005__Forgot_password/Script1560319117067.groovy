import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//generate random password
WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/sign-in/a_forgot password'))

WebUI.setText(findTestObject('getcraft.io/sign-in/email_forgot_password'), GlobalVariable.email_forgot_passw)

WebUI.click(findTestObject('getcraft.io/sign-in/button_send_link'))

// open new  window 
WebUI.delay(5)

WebUI.executeJavaScript('window.open();', [])

currentWindow = WebUI.getWindowIndex()

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.navigateToUrl('https://mailnesia.com/')

WebUI.setText(findTestObject('Mailnesia/input_email'), 'pelakor')

WebUI.click(findTestObject('Mailnesia/Go'))

WebUI.click(findTestObject('Mailnesia/open mail'))

WebUI.verifyElementVisible(findTestObject('GETCRAFT/register/btn_recover_account'))

WebUI.click(findTestObject('GETCRAFT/register/btn_recover_account'))

WebUI.delay(2)

//generate random password
int Random

Random = ((Math.random() * 999) as int)

String New_Password_Value = GlobalVariable.new_password + Random

println(New_Password_Value)

WebUI.setText(findTestObject('getcraft.io/sign-in/setnew_password'), New_Password_Value)

WebUI.click(findTestObject('getcraft.io/sign-in/change_password'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.email_forgot_passw)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), New_Password_Value)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/root/icon_user-menu'), 20)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

