import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'creator98@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.creator_password)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/account-setting/allow_notif'), 15)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.delay(2)

not_run: WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

not_run: WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/a_Account Setting'))

WebUI.click(findTestObject('getcraft.io/account-setting/a_change email'))

WebUI.setText(findTestObject('getcraft.io/account-setting/change email/input_new email'), 'creator99@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/account-setting/change email/input_confirm new email'), 'creator99@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/account-setting/change email/input_password'), GlobalVariable.creator_password)

WebUI.click(findTestObject('getcraft.io/account-setting/change email/button_save changes'))

WebUI.verifyElementText(findTestObject('getcraft.io/account-setting/change email/verify_change email'), 'Request Submitted!')

WebUI.navigateToUrl('https://mailnesia.com/')

WebUI.setText(findTestObject('Mailnesia/input_email'), 'creator99')

WebUI.click(findTestObject('Mailnesia/Go'))

WebUI.click(findTestObject('Mailnesia/open mail'))

WebUI.click(findTestObject('Mailnesia/a_confirm changes'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'creator99@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.creator_password)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/a_newest project'), 20)

WebUI.mouseOver(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.delay(2)

not_run: WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

not_run: WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/a_Account Setting'))

WebUI.click(findTestObject('getcraft.io/account-setting/a_change email'))

WebUI.setText(findTestObject('getcraft.io/account-setting/change email/input_new email'), 'creator98@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/account-setting/change email/input_confirm new email'), 'creator98@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/account-setting/change email/input_password'), GlobalVariable.creator_password)

WebUI.click(findTestObject('getcraft.io/account-setting/change email/button_save changes'))

WebUI.verifyElementText(findTestObject('getcraft.io/account-setting/change email/verify_change email'), 'Request Submitted!')

WebUI.navigateToUrl('https://mailnesia.com/')

WebUI.setText(findTestObject('Mailnesia/input_email'), 'creator98')

WebUI.click(findTestObject('Mailnesia/Go'))

WebUI.click(findTestObject('Mailnesia/open mail'))

WebUI.click(findTestObject('Mailnesia/a_confirm changes'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'creator98@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.creator_password)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/top-menu/a_Projects'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/a_newest project'), 20)

WebUI.closeBrowser()

