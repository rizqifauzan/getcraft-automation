import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'firstlater@gmail.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

not_run: if (WebUI.verifyElementVisible(findTestObject('getcraft.io/account-setting/allow_notif'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'))
} else {
    WebUI.delay(2)
}

not_run: WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/a_Account Setting'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/account-setting/a_change email'), 6)

WebUI.click(findTestObject('getcraft.io/account-setting/a_change email'))

WebUI.setText(findTestObject('getcraft.io/account-setting/change email/input_new email'), 'julo5@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/account-setting/change email/input_confirm new email'), 'julo5@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/account-setting/change email/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/account-setting/change email/button_save changes'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/account-setting/change email/email has taken'), 3)

WebUI.closeBrowser()

