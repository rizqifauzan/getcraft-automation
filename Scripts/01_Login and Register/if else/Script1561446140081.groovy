import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.context.TestCaseContext as TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext as TestSuiteContext
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.BeforeTestCase as BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite as BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase as AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite as AfterTestSuite
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Admin_Staging_URL)

WebUI.click(findTestObject('admin.staging.getcraft.io/Login/button_login google'))

WebUI.switchToWindowIndex(1)

not_run: WebUI.switchToWindowTitle('Masuk - Akun Google')

title = WebUI.getWindowTitle()

WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_email'), 'QAtest@getcraft.com')

WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_password'), 'crafty1234')

WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))

WebUI.delay(2)

WebUI.switchToWindowTitle('Getcraft', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/nav_pending invoice'))

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/a_newest detail'))

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/radio_reject'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/radio_reject reason'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/button_submit_reject'))

WebUI.navigateToUrl(GlobalVariable.Prod_URL)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.email_invitation)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.passw_draft)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.click(findTestObject('getcraft.io/root/label_username'))

WebUI.click(findTestObject('getcraft.io/root/a_workspace setting'))

WebUI.verifyElementText(findTestObject('getcraft.io/setting/Making Payment/verify_is approved'), 'Payment Verification is Rejected')

WebUI.click(findTestObject('getcraft.io/setting/Making Payment/button_close'))

WebUI.closeBrowser()

