import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

String charset = ('a'..'z').join()

Integer length = 9

String randomString = RandomStringUtils.random(length, charset.toCharArray())

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Admin_Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('admin.staging.getcraft.io/Login/button_login google'))

WebUI.switchToWindowTitle('Masuk - Akun Google')

WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_email'), 'QAtest@getcraft.com')

WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/sign-in/google/input_password'), 0)

WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_password'), 'crafty1234')

WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))

WebUI.delay(2)

WebUI.switchToWindowTitle('Getcraft', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/nav_admin-created-users'))

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/admin created user/button_register new users'))

WebUI.setText(findTestObject('admin.staging.getcraft.io/admin/admin created user/input_firstName'), 'first')

WebUI.setText(findTestObject('admin.staging.getcraft.io/admin/admin created user/input_lastName'), randomString)

WebUI.setText(findTestObject('admin.staging.getcraft.io/admin/admin created user/input_email'), randomString + '@mailnesia.com')

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/admin created user/button_auto generate'))

password = WebUI.getAttribute(findTestObject('admin.staging.getcraft.io/admin/admin created user/input_password'), 'value')

WebUI.setText(findTestObject('admin.staging.getcraft.io/admin/admin created user/input_profile name'), 'profileName')

WebUI.setText(findTestObject('admin.staging.getcraft.io/admin/admin created user/input_profile ID'), 'profile-' + randomString)

WebUI.selectOptionByLabel(findTestObject('admin.staging.getcraft.io/admin/admin created user/select_country'), 'Indonesia', 
    false)

WebUI.selectOptionByLabel(findTestObject('admin.staging.getcraft.io/admin/admin created user/select_city'), 'Magelang, Central Java', 
    false)

WebUI.selectOptionByLabel(findTestObject('admin.staging.getcraft.io/admin/admin created user/select_defaultFeature'), 'Product', 
    false)

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/admin created user/button_Register user'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), randomString + '@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), password)

WebUI.verifyElementPresent(findTestObject('getcraft.io/product/a_Add a new service'), 10)

WebUI.closeBrowser()

