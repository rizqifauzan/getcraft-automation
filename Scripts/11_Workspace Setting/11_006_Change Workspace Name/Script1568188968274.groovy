import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'julo5@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'getcraft1234')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementPresent(findTestObject('getcraft.io/account-setting/allow_notif'), 30)

WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/Goto_profile'))

WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))

WebUI.delay(2)

WebUI.click(findTestObject('yopmail/Page_julo411 at Getcraft/svg_julo411_jss17 jss24'))

//generate random password
int Random

Random = ((Math.random() * 999) as int)

WebUI.setText(findTestObject('getcraft.io/creator-profile/profile-name'), Random)

WebUI.setText(findTestObject('getcraft.io/creator-profile/profile-id'), Random)

WebUI.click(findTestObject('getcraft.io/creator-profile/save'))

not_run: WebUI.click(findTestObject('getcraft.io/setting/Making Payment/button_edit'))

not_run: WebUI.click(findTestObject('getcraft.io/setting/Making Payment/button_submit'))

not_run: WebUI.verifyElementText(findTestObject('getcraft.io/setting/Making Payment/verify_is submited'), 'Payment Verification is Submitted')

not_run: WebUI.click(findTestObject('getcraft.io/setting/Making Payment/button_okey'))

not_run: WebUI.verifyElementText(findTestObject('getcraft.io/setting/Making Payment/verify_status'), 'The verification might take up to 2 working days. You will be notified once the verification is finished.')

not_run: WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

not_run: WebUI.click(findTestObject('getcraft.io/root/a_sign out'))

not_run: WebUI.delay(2)

not_run: WebUI.navigateToUrl(GlobalVariable.Admin_Staging_URL)

not_run: WebUI.click(findTestObject('admin.staging.getcraft.io/Login/button_login google'))

not_run: WebUI.switchToWindowIndex(1)

not_run: title = WebUI.getWindowTitle()

not_run: WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_email'), 'QAtest@getcraft.com')

not_run: WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))

not_run: WebUI.delay(2)

not_run: WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_password'), 'crafty1234')

not_run: WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))

not_run: WebUI.delay(2)

not_run: WebUI.switchToWindowTitle('Getcraft', FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.click(findTestObject('admin.staging.getcraft.io/admin/nav_pending invoice'))

not_run: WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/a_newest detail'))

not_run: WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/radio_reject'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/radio_reject reason'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/button_submit_reject'))

not_run: WebUI.navigateToUrl(GlobalVariable.Staging_URL)

not_run: WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

not_run: WebUI.delay(2)

not_run: WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.email_test)

not_run: WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.passw_draft)

not_run: WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

not_run: WebUI.click(findTestObject('getcraft.io/root/label_username'))

not_run: WebUI.click(findTestObject('getcraft.io/root/a_workspace setting'))

not_run: WebUI.verifyElementText(findTestObject('getcraft.io/setting/Making Payment/verify_is approved'), 'Payment Verification is Rejected')

not_run: WebUI.click(findTestObject('getcraft.io/setting/Making Payment/button_close'))

not_run: WebUI.closeBrowser()

