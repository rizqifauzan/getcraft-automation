import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.email_test)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.passw_draft)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/setting/tab_making payment'))

WebUI.click(findTestObject('getcraft.io/setting/Making Payment/button_edit'))

WebUI.click(findTestObject('getcraft.io/setting/Making Payment/button_submit'))

WebUI.verifyElementText(findTestObject('getcraft.io/setting/Making Payment/verify_is submited'), 'Payment Verification is Submitted')

WebUI.click(findTestObject('getcraft.io/setting/Making Payment/button_okey'))

WebUI.verifyElementText(findTestObject('getcraft.io/setting/Making Payment/verify_status'), 'The verification might take up to 2 working days. You will be notified once the verification is finished.')

WebUI.click(findTestObject('getcraft.io/account-setting/Homepage'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/root/a_sign out'))

WebUI.delay(2)

WebUI.navigateToUrl(GlobalVariable.Admin_Staging_URL)

WebUI.click(findTestObject('admin.staging.getcraft.io/Login/button_login google'))

WebUI.switchToWindowIndex(1)

title = WebUI.getWindowTitle()

WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_email'), 'QAtest@getcraft.com')

WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/google/input_password'), 'crafty1234')

WebUI.click(findTestObject('getcraft.io/sign-in/google/button_next'))

WebUI.delay(2)

WebUI.switchToWindowTitle('Getcraft', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/nav_pending invoice'))

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/a_newest detail'))

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/radio_reject'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/radio_reject reason'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('admin.staging.getcraft.io/admin/pending invoice/button_submit_reject'))

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), GlobalVariable.email_test)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), GlobalVariable.passw_draft)

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.click(findTestObject('getcraft.io/account-setting/allow_notif'))

WebUI.click(findTestObject('getcraft.io/root/icon_user-menu'))

WebUI.click(findTestObject('getcraft.io/setting/tab_making payment'))

WebUI.verifyElementText(findTestObject('getcraft.io/setting/Making Payment/verify_is approved'), 'Payment Verification is Rejected')

WebUI.click(findTestObject('getcraft.io/setting/Making Payment/button_Resbumit'))

WebUI.closeBrowser()

