import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

String status = GlobalVariable.TC_Status

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.Staging_URL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('getcraft.io/root/button_Sign In'))

WebUI.delay(2)

WebUI.setText(findTestObject('getcraft.io/sign-in/input_email'), 'betaclinov@mailnesia.com')

WebUI.setText(findTestObject('getcraft.io/sign-in/input_password'), 'Danesa123')

WebUI.click(findTestObject('getcraft.io/sign-in/button_Continue'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), 40)

WebUI.click(findTestObject('getcraft.io/sign-in/button_PushNotif Later'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/root/a_my project'))

WebUI.click(findTestObject('getcraft.io/project/payment/status_payment nedeed'))

WebUI.click(findTestObject('getcraft.io/project/order/tab_payment'))

WebUI.delay(2)

WebUI.click(findTestObject('getcraft.io/project/payment/button_Pay Now'))

WebUI.click(findTestObject('getcraft.io/project/payment/Button_Bank Transfer'))

WebUI.click(findTestObject('getcraft.io/project/payment/Button_back'))

WebUI.click(findTestObject('getcraft.io/project/payment/Button_Bank Transfer'))

WebUI.verifyElementPresent(findTestObject('getcraft.io/project/payment/button_i have paid'), 1)

WebUI.click(findTestObject('getcraft.io/project/payment/button_i have paid'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/project/payment/Button_No'), 1)

WebUI.click(findTestObject('getcraft.io/project/payment/Button_No'))

WebUI.click(findTestObject('getcraft.io/project/payment/button_i have paid'))

WebUI.waitForElementClickable(findTestObject('getcraft.io/project/payment/Button_yes i have paid'), 1)

WebUI.click(findTestObject('getcraft.io/project/payment/Button_yes i have paid'))

WebUI.click(findTestObject('getcraft.io/project/payment/Button_Ok'))

WebUI.verifyElementText(findTestObject('getcraft.io/project/payment/verify_progress'), 'Your Bank Transfer is in progress')

WebUI.click(findTestObject('getcraft.io/project/payment/Button_close'))

//Refresh the current web page'
WebUI.refresh()

WebUI.waitForElementPresent(findTestObject('getcraft.io/project/payment/verify_progress'), 1)

WebUI.verifyElementText(findTestObject('getcraft.io/project/payment/verify_progress'), 'Your Bank Transfer is in progress')

WebUI.closeBrowser()

