import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


class IsStaging {
	/**
	 * Refresh browser
	 */
	@Keyword
	def refreshBrowser() {
		KeywordUtil.logInfo("Refreshing")
		WebDriver webDriver = DriverFactory.getWebDriver()
		webDriver.navigate().refresh()
		KeywordUtil.markPassed("Refresh successfully")
	}


	/**
	 * Get all rows of HTML table
	 * @param table Katalon test object represent for HTML table
	 * @param outerTagName outer tag name of TR tag, usually is TBODY
	 * @return All rows inside HTML table
	 */
	@Keyword
	def List<WebElement> getHtmlTableRows(TestObject table, String outerTagName) {
		WebElement mailList = WebUiBuiltInKeywords.findWebElement(table)
		List<WebElement> selectedRows = mailList.findElements(By.xpath("./" + outerTagName + "/tr"))
		return selectedRows
	}



	/*
	 * @author Fauzan
	 * @created at April 6th, 2020
	 *
	 * This method use for edit HTML element 
	 * 
	 */
	@Keyword
	def updateElement(TestObject object) {

		WebUI.executeJavaScript("alert('This is an alert')", null)

		//		$('div.notranslate .public-DraftStyleDefault-block > span').html('<span data-text="true">Hai From JS</span>');
		//
		//		#rdw-wrapper-7283 > div > div > div.DraftEditor-editorContainer > div > div > div > div > span
		//		#rdw-wrapper-2809 > div > div > div.DraftEditor-editorContainer > div > div > div > div > span
		//		#rdw-wrapper-711 > div > div > div > div > div > div > div > span > span
	}

	/*
	 * @author Fauzan
	 * @created at November 5th, 2019
	 *
	 */
	@Keyword
	def clickElement(TestObject betaObject, TestObject stagingObject) {
		String url = WebUI.getUrl()
		if (url.contains("beta.")){
			WebElement element = WebUiBuiltInKeywords.findWebElement(betaObject);
			KeywordUtil.logInfo("Clicking element " + betaObject.toString())
			element.click()
		}else {
			WebElement element = WebUiBuiltInKeywords.findWebElement(stagingObject);
			KeywordUtil.logInfo("Clicking element " + stagingObject.toString())
			element.click()
		}
	}


	/*
	 * @author Fauzan
	 * @created at November 5th, 2019
	 * 
	 */
	@Keyword
	def setTextElement(TestObject betaObject, TestObject stagingObject, String text) {
		String url = WebUI.getUrl()
		if (url.contains("beta.")){
			//WebElement element = WebUiBuiltInKeywords.findWebElement(betaObject);
			KeywordUtil.logInfo("Set text on element " + betaObject.toString())
			WebUI.setText(betaObject, text)
		}else {
			//WebElement element = WebUiBuiltInKeywords.findWebElement(stagingObject);
			KeywordUtil.logInfo("Set text on element " + betaObject.toString())
			WebUI.setText(stagingObject, text)
		}
	}

	/*
	 * @author Fauzan
	 * @created at November 5th, 2019
	 *
	 */
	@Keyword
	def verifyTextContain(TestObject betaObject, String text){
		String elementText = WebUI.getText(betaObject)
		if(elementText.contains(text)){
			KeywordUtil.markPassed("As expected ")
		}else{
			KeywordUtil.markFailed(elementText + "not contain :" + text)
		}
	}

	/*
	 * @author Fauzan
	 * @created at November 20th, 2019
	 * -1 mean last object
	 */
	@Keyword
	def verifyAttributeOjectFromList(String xpath2, int i, String attribute, String text) {
		WebDriver webDriver = DriverFactory.getWebDriver()
		List<WebElement> element = webDriver.findElements(By.xpath(xpath2))

		int maxItem = element.size();
		//WebElement click2 = element[i]

		Iterator<WebElement> iter = element.iterator();

		// this will check whether list has some element or not
		int a = 0;
		while (iter.hasNext()) {
			// Iterate one by one
			WebElement item = iter.next();
			if(i<0 && !iter.hasNext()){
				// get the text

				String label = item.getAttribute(attribute)

				if(label.contains(text)){
					KeywordUtil.markPassed("As expected ")
				}else{
					KeywordUtil.markFailed("expected: " + text + ", actual :" + label)
				}
				// print the text
				//System.out.println("Row label is " + label2);
			}else if( a == i ){
				String label = item.getText();
				if(label == text){
					KeywordUtil.markPassed(label + " is apear in " + "xpath2")
				}else{

				}
			}
			// get the text
			//String label = item.getText();

			// print the text
			//System.out.println("Row label is " + label);
			a++;
		}
	}


	/*
	 * @author Fauzan
	 * @created at November 5th, 2019
	 *
	 */
	@Keyword
	def isStaging() {
		String url = WebUI.getUrl()
		if (url.contains("beta.")){
			return false
		}else {
			return true
		}
	}

	@Keyword
	def clickObjectFromList(String xpath2, int i) {
		WebDriver webDriver = DriverFactory.getWebDriver()
		List<WebElement> element = webDriver.findElements(By.xpath(xpath2))
		//WebElement click2 = element[i]

		Iterator<WebElement> iter = element.iterator();

		// this will check whether list has some element or not
		int a = 0;
		while (iter.hasNext()) {
			// Iterate one by one
			WebElement item = iter.next();
			if(i<0 && !iter.hasNext()){
				try {
					KeywordUtil.logInfo("try to click element - " + i)
					item.click()
					KeywordUtil.markPassed("Element " + i +" has been clicked")
				} catch (Exception e) {
					KeywordUtil.markFailed("Element cant be clicked")
					e.printStackTrace()
				}
			}else if( a == i ){
				try {
					item.click()
					KeywordUtil.markPassed("Element has been clicked")
				} catch (Exception e) {
					KeywordUtil.markFailed("Element cant be clicked")
					e.printStackTrace()
				}
			}
			a++;
		}
		while (iter.hasNext()) {
			// Iterate one by one
			WebElement item = iter.next();
			if( a == i ){
				item.click()
				KeywordUtil.markPassed("Element has been clicked")
			}
			// get the text
			String label = item.getText();

			// print the text
			System.out.println("Row label is " + label);
			a++;
		}
	}


	@Keyword
	def getTextObjectFromList(String xpath2, int i) {
		WebDriver webDriver = DriverFactory.getWebDriver()
		List<WebElement> element = webDriver.findElements(By.xpath(xpath2))
		//WebElement click2 = element[i]

		Iterator<WebElement> iter = element.iterator();

		String text;

		// this will check whether list has some element or not
		int a = 0;
		while (iter.hasNext()) {
			// Iterate one by one
			WebElement item = iter.next();
			if(i<0 && !iter.hasNext()){
				try {
					KeywordUtil.logInfo("try to click element - " + i)
					text = item.getText()
					KeywordUtil.markPassed("Element " + i +" has been clicked")
				} catch (Exception e) {
					KeywordUtil.markFailed("Element cant be clicked")
					e.printStackTrace()
				}
			}else if( a == i ){
				try {
					text = item.getText()
					KeywordUtil.markPassed("Element has been clicked")
				} catch (Exception e) {
					KeywordUtil.markFailed("Element cant be clicked")
					e.printStackTrace()
				}
			}
			a++;
		}
		while (iter.hasNext()) {
			// Iterate one by one
			WebElement item = iter.next();
			if( a == i ){
				text = item.getText()
				KeywordUtil.markPassed("Element has been clicked")
			}
			// get the text
			String label = item.getText();

			// print the text
			System.out.println("Row label is " + label);
			a++;
		}

		return text
	}


	// -1 mean last object
	@Keyword
	def verifyObjectFromList(String xpath2, int i, String text) {
		WebDriver webDriver = DriverFactory.getWebDriver()
		List<WebElement> element = webDriver.findElements(By.xpath(xpath2))
		//WebElement click2 = element[i]

		Iterator<WebElement> iter = element.iterator();

		// this will check whether list has some element or not
		int a = 0;
		while (iter.hasNext()) {

			// Iterate one by one
			WebElement item = iter.next();
			if(i<0 && !iter.hasNext()){
				// get the text
				String label = item.getText();

				if(label==text){
					KeywordUtil.markPassed("As expected ")
				}else{
					KeywordUtil.markFailed("expected: " + text + ", actual :" + label)
				}
				// print the text
				//System.out.println("Row label is " + label2);
			}else if( a == i ){
				String label = item.getText();
				if(label == text){
					KeywordUtil.markPassed(label + " is apear in " + "xpath2")
				}else{

				}
			}
			// get the text
			//String label = item.getText();

			// print the text
			//System.out.println("Row label is " + label);
			a++;
		}
		if(i==-1){

		}
	}

	@Keyword
	public static void run(TestObject objectto, String data) {
		try {
			WebDriver driver = DriverFactory.getWebDriver()
			JavascriptExecutor js = (JavascriptExecutor) driver;

			WebElement element = WebUiCommonHelper.findWebElement(objectto, 20);

			js.executeScript("arguments[0].setAttribute('style','border: 5px solid red;');",element)

			WebUI.verifyElementText(objectto, data)

			WebUI.takeScreenshot()

			js.executeScript("arguments[0].setAttribute('style','border: 0px;');",element)
		} catch (Exception e) {
			KeywordUtil.markFailed("Unable to verify element text " + data)
		}
	}
}