package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object creator_email
     
    /**
     * <p></p>
     */
    public static Object creator_password
     
    /**
     * <p></p>
     */
    public static Object email_draft
     
    /**
     * <p></p>
     */
    public static Object passw_draft
     
    /**
     * <p></p>
     */
    public static Object email_invitation
     
    /**
     * <p></p>
     */
    public static Object email_forgot_passw
     
    /**
     * <p></p>
     */
    public static Object new_password
     
    /**
     * <p></p>
     */
    public static Object pt_email
     
    /**
     * <p></p>
     */
    public static Object pt_password
     
    /**
     * <p></p>
     */
    public static Object email_google
     
    /**
     * <p>Profile rahmat : s</p>
     */
    public static Object passw_google
     
    /**
     * <p></p>
     */
    public static Object TC_Status
     
    /**
     * <p></p>
     */
    public static Object currentTestCaseId
     
    /**
     * <p></p>
     */
    public static Object TC_Status2
     
    /**
     * <p></p>
     */
    public static Object email_change_passw
     
    /**
     * <p>Profile Linux CI_Beta : ada
Profile Linux CI_Staging : ada
Profile Rizky_Beta : ada
Profile default : ada
Profile rahmat : ada</p>
     */
    public static Object Staging_URL
     
    /**
     * <p>Profile Linux CI_Beta : admin URL
Profile Linux CI_Staging : admin URL
Profile Rizky_Beta : admin URL
Profile default : admin URL
Profile production : admin URL
Profile rahmat : admin URL</p>
     */
    public static Object Admin_Staging_URL
     
    /**
     * <p>Profile production : &#47;home&#47;rachmat&#47;maxresdefault.jpg</p>
     */
    public static Object imagePath
     
    /**
     * <p>Profile rahmat : &#47;Users&#47;rachmat&#47;Downloads&#47;luffy2.jpg</p>
     */
    public static Object imagePath2
     
    /**
     * <p></p>
     */
    public static Object increment
     
    /**
     * <p></p>
     */
    public static Object imagePath3
     
    /**
     * <p></p>
     */
    public static Object email_test
     
    /**
     * <p></p>
     */
    public static Object serviceName
     
    /**
     * <p></p>
     */
    public static Object emailClient_check
     
    /**
     * <p></p>
     */
    public static Object emailCreator_check
     
    /**
     * <p>Profile production : ada</p>
     */
    public static Object Prod_URL
     
    /**
     * <p></p>
     */
    public static Object G_Timeout_Small
     
    /**
     * <p></p>
     */
    public static Object CreatorEmail
     
    /**
     * <p></p>
     */
    public static Object CreatorPassw
     
    /**
     * <p></p>
     */
    public static Object Keyword_Search
     
    /**
     * <p></p>
     */
    public static Object ClientEmail
     
    /**
     * <p></p>
     */
    public static Object ClientPasswd
     
    /**
     * <p></p>
     */
    public static Object project_name
     
    /**
     * <p></p>
     */
    public static Object QuoteGenerated
     
    /**
     * <p></p>
     */
    public static Object ProjectRun
     
    /**
     * <p></p>
     */
    public static Object token
     
    /**
     * <p></p>
     */
    public static Object user
     
    /**
     * <p></p>
     */
    public static Object Admin_URL
     
    /**
     * <p></p>
     */
    public static Object baseAPI
     
    /**
     * <p></p>
     */
    public static Object CeatorPasswd
     
    /**
     * <p>Profile rahmat : 4</p>
     */
    public static Object ImagePath4
     
    /**
     * <p></p>
     */
    public static Object CreatorNov
     
    /**
     * <p></p>
     */
    public static Object PasswdNov
     
    /**
     * <p></p>
     */
    public static Object chatUser
     
    /**
     * <p></p>
     */
    public static Object chatCreatorUser
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            creator_email = selectedVariables['creator_email']
            creator_password = selectedVariables['creator_password']
            email_draft = selectedVariables['email_draft']
            passw_draft = selectedVariables['passw_draft']
            email_invitation = selectedVariables['email_invitation']
            email_forgot_passw = selectedVariables['email_forgot_passw']
            new_password = selectedVariables['new_password']
            pt_email = selectedVariables['pt_email']
            pt_password = selectedVariables['pt_password']
            email_google = selectedVariables['email_google']
            passw_google = selectedVariables['passw_google']
            TC_Status = selectedVariables['TC_Status']
            currentTestCaseId = selectedVariables['currentTestCaseId']
            TC_Status2 = selectedVariables['TC_Status2']
            email_change_passw = selectedVariables['email_change_passw']
            Staging_URL = selectedVariables['Staging_URL']
            Admin_Staging_URL = selectedVariables['Admin_Staging_URL']
            imagePath = selectedVariables['imagePath']
            imagePath2 = selectedVariables['imagePath2']
            increment = selectedVariables['increment']
            imagePath3 = selectedVariables['imagePath3']
            email_test = selectedVariables['email_test']
            serviceName = selectedVariables['serviceName']
            emailClient_check = selectedVariables['emailClient_check']
            emailCreator_check = selectedVariables['emailCreator_check']
            Prod_URL = selectedVariables['Prod_URL']
            G_Timeout_Small = selectedVariables['G_Timeout_Small']
            CreatorEmail = selectedVariables['CreatorEmail']
            CreatorPassw = selectedVariables['CreatorPassw']
            Keyword_Search = selectedVariables['Keyword_Search']
            ClientEmail = selectedVariables['ClientEmail']
            ClientPasswd = selectedVariables['ClientPasswd']
            project_name = selectedVariables['project_name']
            QuoteGenerated = selectedVariables['QuoteGenerated']
            ProjectRun = selectedVariables['ProjectRun']
            token = selectedVariables['token']
            user = selectedVariables['user']
            Admin_URL = selectedVariables['Admin_URL']
            baseAPI = selectedVariables['baseAPI']
            CeatorPasswd = selectedVariables['CeatorPasswd']
            ImagePath4 = selectedVariables['ImagePath4']
            CreatorNov = selectedVariables['CreatorNov']
            PasswdNov = selectedVariables['PasswdNov']
            chatUser = selectedVariables['chatUser']
            chatCreatorUser = selectedVariables['chatCreatorUser']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
